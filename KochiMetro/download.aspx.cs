﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;

namespace KochiMetro
{
    public partial class download : System.Web.UI.Page
    {
        string app_data_folder = HttpContext.Current.Server.MapPath("/");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //app_data_folder = Server.MapPath("~\\appUploads\\");
                //string path = app_data_folder;
                string dir = Path.GetDirectoryName(Request["file"]);
                string name = Path.GetFileName(Request["file"]);
                string ext = Path.GetExtension(Request["file"]);
                string type = "";
                // set known types based on file extension
                if (ext != null)
                {
                    switch (ext.ToLower())
                    {
                        case ".htm":
                        case ".html":
                            type = "text/HTML";
                            break;
                        case ".rtf":
                            type = "Application/msword";
                            break;
                        case ".txt":
                        case ".sql":
                            type = "text/plain";
                            break;
                        case ".doc":
                            type = "application/ms-word";
                            break;
                        case ".xls":
                            type = "application/vnd.ms-excel";
                            break;
                        case ".gif":
                            type = "image/gif";
                            break;
                        case ".jpg":
                        case "jpeg":
                            type = "image/jpeg";
                            break;
                        case ".bmp":
                            type = "image/bmp";
                            break;
                        case ".wav":
                            type = "audio/wav";
                            break;
                        case ".ppt":
                            type = "application/mspowerpoint";
                            break;
                        case ".dwg":
                            type = "image/vnd.dwg";
                            break;
                        case ".zip":
                            type = "application/zip";
                            break;
                        case ".exe":
                            type = "application/exe";
                            break;
                        case ".apk":
                            type = "application/vnd.android.package-archive";
                            break;
                        default:
                            type = "application/octet-stream";
                            break;
                    }
                }

                Response.AppendHeader("content-disposition", "attachment; filename=" + name);

                if (type != "")
                {
                    Response.ContentType = type;
                    //Response.WriteFile(path);
                    Response.TransmitFile(dir + "\\" + name);
                    //Response.Redirect("/admin/BulkUpload.aspx");
                    Response.End();
                }



            }
            catch (Exception)
            { Response.End();  }
        }
    }
}