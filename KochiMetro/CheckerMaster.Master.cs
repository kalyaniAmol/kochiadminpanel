﻿using System;
using System.Web;
using System.Web.Security;
using System.Data;
using KochiMetro.classes;

namespace KochiMetro
{
    public partial class CheckerMaster : System.Web.UI.MasterPage
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Checker"))
            {
                if (!IsPostBack)
                {

                    lblName.Text = "Welcome, " + uDetails[1];
                    lblDate.Text = uDetails[3];
                    if (uDetails[2] == "Admin")
                    {
                        liExploreKochi.Visible = true;
                        liOffers.Visible = true;
                        liUsers.Visible = true;
                    }
                    else
                    {
                        DataTable dtPrivilege = new DataTable();
                        dtPrivilege = _ob.GetAdminPrivilegeByAdminId(uDetails[0]);
                        if (dtPrivilege.Rows.Count > 0)
                        {
                            foreach (DataRow row in dtPrivilege.Rows)
                            {
                                if (row["pId"].ToString() == "1" && row["isChecked"].ToString() == "True")
                                {
                                    liExploreKochi.Visible = true;
                                }
                                if (row["pId"].ToString() == "2" && row["isChecked"].ToString() == "True")
                                {
                                    liOffers.Visible = true;
                                }
                                if (row["pId"].ToString() == "3" && row["isChecked"].ToString() == "True")
                                {
                                    liUsers.Visible = true;
                                }
                            }

                        }
                        else
                        {
                            liExploreKochi.Visible = false;
                            liOffers.Visible = false;
                            liUsers.Visible = false;
                        }
                    }

                }
            }
            else
            {
                Response.Redirect("/CheckerLogin.aspx");
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/CheckerLogin.aspx");
        }
    }
}