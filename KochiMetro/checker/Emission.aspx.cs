﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.checker
{
    public partial class Emission : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        static string filename = "";
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin" || uDetails[2] == "Checker")
                {
                    try
                    {
                        lblEmissionId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetEmissionTestingById(lblEmissionId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            txtName.Text = dt.Rows[0]["emissionName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();

                            btnBack.Visible = true;
                            btnAdd.Text = "Edit & Approve";
                            Session.Remove("id");
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                else
                {
                    Response.Redirect("/CheckerLogin.aspx");
                }
            }


        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string res;
                if (btnAdd.Text == "Edit & Approve")
                {
                    res = _ob.EditApproveEmissionTesting(lblEmissionId.Text, txtName.Text, txtAddress.Text, txtArea.Text,
                       txtContactNo.Text, CKEditorDesc.Text, uDetails[0],uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        ClearTextBoxes();
                        Session["status"] = "Success";
                        Response.Redirect("/checker/EntryDetails.aspx");
                    }
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/checker/EntryDetails.aspx");
        }
    }
}