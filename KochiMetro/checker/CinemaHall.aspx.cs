﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.checker
{
    public partial class CinemaHall : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        static string filename = "";
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin" || uDetails[2] == "Checker")
                {
                    try
                    {
                        lblCinemaId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetCinemaHallById(lblCinemaId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                            DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                            if (dtFilter.Rows.Count > 0)
                            {
                                ddType.DataSource = dtFilter;
                                ddType.DataBind();
                            }
                            txtName.Text = dt.Rows[0]["theatreName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            ddType.SelectedValue = dt.Rows[0]["noOfScreens"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                            lblImage.Text = dt.Rows[0]["displayImage"].ToString();

                            btnBack.Visible = true;
                            btnAdd.Text = "Edit & Approve";
                        }
                        Session.Remove("id");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/CheckerLogin.aspx");
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddType.SelectedValue != "--Select--")
                {
                    string res;
                    if (btnAdd.Text == "Edit & Approve")
                    {
                        if (uploadImg.HasFile)
                        {
                            string ranId = _ob.getRandomID();
                            filename = uploadImg.FileName.Replace(' ', '_');
                            string Ext = System.IO.Path.GetExtension(this.uploadImg.PostedFile.FileName);
                            if (Ext.ToLower() == ".bmp" || Ext.ToLower() == ".gif" || Ext.ToLower() == ".jpg" || Ext.ToLower() == ".jpeg" || Ext.ToLower() == ".png")
                            {
                                string[] splitname = filename.Split('.');
                                filename = splitname[0].ToString() + "_" + ranId + Ext;
                                uploadImg.SaveAs(Server.MapPath("~\\images\\cinema\\" + filename));

                            }

                            lblImage.Text = filename;
                        }

                        res = _ob.EditApproveCinemaHall(lblCinemaId.Text, txtName.Text, txtAddress.Text, txtArea.Text, lblImage.Text,
                           ddType.SelectedValue, txtContactNo.Text, CKEditorDesc.Text, uDetails[0],uDetails[0]);
                        if (res.Contains("Success"))
                        {
                            ClearTextBoxes();
                            Session["status"] = "Success";
                            Response.Redirect("/checker/EntryDetails.aspx");
                        }
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please Select number of screens !! ");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            ddType.DataBind();
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/checker/EntryDetails.aspx");
        }

        protected void ddType_DataBinding(object sender, EventArgs e)
        {
            ddType.Items.Clear();
        }

        protected void ddType_DataBound(object sender, EventArgs e)
        {
            ddType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", ""));
        }
    }
}