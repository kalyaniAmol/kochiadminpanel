﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.checker
{
    public partial class Hotel : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin" || uDetails[2] == "Checker")
                {
                    try
                    {
                        lblHotelId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetHotelById(lblHotelId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                            DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                            if (dtFilter.Rows.Count > 0)
                            {
                                ddType.DataSource = dtFilter;
                                ddType.DataBind();
                            }
                            txtName.Text = dt.Rows[0]["hotelName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            ddType.SelectedValue = dt.Rows[0]["starRating"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();

                            btnBack.Visible = true;
                            btnAdd.Text = "Edit & Approve";
                        }
                        Session.Remove("id");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/CheckerLogin.aspx");
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddType.SelectedValue != "--Select--")
                {
                    string res;
                    if (btnAdd.Text == "Edit & Approve")
                    {
                        res = _ob.EditApproveHotel(lblHotelId.Text, txtName.Text, txtAddress.Text, txtArea.Text,
                            ddType.SelectedValue, txtContactNo.Text, CKEditorDesc.Text, uDetails[0],uDetails[0]);
                        if (res.Contains("Success"))
                        {
                            ClearTextBoxes();
                            Session["status"] = "Success";
                            Response.Redirect("/checker/EntryDetails.aspx");
                        }
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please select star rating !! ");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";

            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/checker/EntryDetails.aspx");
        }

        protected void ddType_DataBinding(object sender, EventArgs e)
        {
            ddType.Items.Clear();
        }

        protected void ddType_DataBound(object sender, EventArgs e)
        {
            ddType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", ""));
        }
    }
}