﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace KochiMetro.classes
{
    public class AdminClass
    {
        private readonly SqlConnection _cn = new SqlConnection(ConfigurationManager.ConnectionStrings["kochiMetroConnectionString"].ConnectionString);

        public void Openconn()
        {
            try { _cn.Open(); }
            catch { _cn.Close(); _cn.Open(); }
        }

        public string base64Encode(string sData)
        {
            try
            {
                byte[] encData_byte = new byte[sData.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        }

        public string base64Decode(string sData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(sData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char); return result;
        }

        public DataTable GetAdminLoginDetails(string mobileNo, string loginPassword)
        {
            DataTable dt = new DataTable();
            try
            {
                string pass = base64Encode(loginPassword);
                SqlCommand cmd = new SqlCommand("getAdminLoginDetails", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("loginPassword", pass);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        // Explore Kochi

        //Hospital Details

        public int GetBatch(string exploreCatId)
        {
            int i = 0;
            SqlCommand cmd = null;
            if (exploreCatId == "1")
            {
                cmd = new SqlCommand("GetHospitalBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "2")
            {
                cmd = new SqlCommand("GetGovtOfficeBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "3")
            {
                cmd = new SqlCommand("GetEmissionBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "4")
            {
                cmd = new SqlCommand("GetParkingBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "5")
            {
                cmd = new SqlCommand("GetPoliceStationBatch", _cn) { CommandType = CommandType.StoredProcedure };
            } 
            if (exploreCatId == "6")
            {
                cmd = new SqlCommand("GetTourismBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "7")
            {
                cmd = new SqlCommand("GetCinemaBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "8")
            {
                cmd = new SqlCommand("GetHotelBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }
            if (exploreCatId == "9")
            {
                cmd = new SqlCommand("GetFuelStationBatch", _cn) { CommandType = CommandType.StoredProcedure };
            }

            Openconn();
            i = Convert.ToInt32(cmd.ExecuteScalar());
            _cn.Close();
            return i;
        }

        public int inserBulkHospital(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkHospital", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkGovtOffices(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkGovtOffices", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkParking(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkParking", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkPoliceStation(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkPoliceStation", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkEmissionTest(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkEmissionTest", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkTourism(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkTourism", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkCinemas(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkCinemas", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkRestaurant(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkRestaurant", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public int inserBulkFuelPoint(string strXML)
        {
            SqlCommand cmd = new SqlCommand("inserBulkFuelPoint", _cn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.AddWithValue("strXML", strXML);
            Openconn();
            int recordsInserted = cmd.ExecuteNonQuery();
            _cn.Close();
            return recordsInserted;
        }

        public string AddHospital(string exploreCatId, string hospitalName, string address, string area, string specialityId, string speciality, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addHospital", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("hospitalName", hospitalName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("specialityId", specialityId);
                cmd.Parameters.AddWithValue("speciality", speciality);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetHospitalById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetHospitalById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateHospital(string hospitalId, string hospitalName, string address, string area, string specialityId, string speciality, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateHospital", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("hospitalId", hospitalId);
                cmd.Parameters.AddWithValue("hospitalName", hospitalName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("specialityId", specialityId);
                cmd.Parameters.AddWithValue("speciality", speciality);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveHospital(string hospitalId, string hospitalName, string address, string area, string specialityId, string speciality, string contactNo, string otherDescription, string updatedBy, string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveHospital", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("hospitalId", hospitalId);
                cmd.Parameters.AddWithValue("hospitalName", hospitalName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("specialityId", specialityId);
                cmd.Parameters.AddWithValue("speciality", speciality);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        // Emission Testing

        public string AddEmissionTesting(string exploreCatId, string emissionName, string address, string area, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addEmissionTesting", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("emissionName", emissionName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetEmissionTestingById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetEmissionTestingById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateEmissionTesting(string emissionId, string emissionName, string address, string area, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateEmissionTesting", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("emissionId", emissionId);
                cmd.Parameters.AddWithValue("emissionName", emissionName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveEmissionTesting(string emissionId, string emissionName, string address, string area, string contactNo, string otherDescription, string updatedBy,string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveEmissionTesting", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("emissionId", emissionId);
                cmd.Parameters.AddWithValue("emissionName", emissionName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //Fuel Point

        public string AddFuelPoint(string exploreCatId, string fuelPointName, string address, string displayImage, string area, string fuelComapny, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addFuelPoint", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("fuelPointName", fuelPointName);
                cmd.Parameters.AddWithValue("address", address);
                cmd.Parameters.AddWithValue("displayImage", displayImage);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("fuelComapny", fuelComapny);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetFuelPointById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetFuelPointById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateFuelPoint(string fuelPointId, string fuelPointName, string address, string area, string fuelComapny, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateFuelPoint", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("fuelPointId", fuelPointId);
                cmd.Parameters.AddWithValue("fuelPointName", fuelPointName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("fuelComapny", fuelComapny);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveFuelPoint(string fuelPointId, string fuelPointName, string address, string area, string fuelComapny, string contactNo, string otherDescription, string updatedBy,string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveFuelPoint", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("fuelPointId", fuelPointId);
                cmd.Parameters.AddWithValue("fuelPointName", fuelPointName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("fuelComapny", fuelComapny);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //Parking 

        public string AddParkingStation(string exploreCatId, string parkingName, string address, string area, string pTypeId, string pType, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addParkingStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("parkingName", parkingName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("pTypeId", pTypeId);
                cmd.Parameters.AddWithValue("pType", pType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetParkingStationById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetParkingStationById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateParkingStation(string parkingId, string parkingName, string address, string area, string pTypeId, string pType, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateParkingStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("parkingId", parkingId);
                cmd.Parameters.AddWithValue("parkingName", parkingName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("pTypeId", pTypeId);
                cmd.Parameters.AddWithValue("pType", pType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveParkingStation(string parkingId, string parkingName, string address, string area, string pTypeId, string pType, string contactNo, string otherDescription, string updatedBy,string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveParkingStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("parkingId", parkingId);
                cmd.Parameters.AddWithValue("parkingName", parkingName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("pTypeId", pTypeId);
                cmd.Parameters.AddWithValue("pType", pType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //Police Station

        public string AddPoliceStation(string exploreCatId, string policeStationName, string address, string area, string policeType, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addPoliceStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("policeStationName", policeStationName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("policeType", policeType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetPoliceStationById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetPoliceStationById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdatePoliceStation(string policeId, string policeStationName, string address, string area, string policeType, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdatePoliceStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("policeId", policeId);
                cmd.Parameters.AddWithValue("policeStationName", policeStationName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("policeType", policeType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApprovePoliceStation(string policeId, string policeStationName, string address, string area, string policeType, string contactNo, string otherDescription, string updatedBy,string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApprovePoliceStation", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("policeId", policeId);
                cmd.Parameters.AddWithValue("policeStationName", policeStationName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("policeType", policeType);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //Cinema hall
        public string AddCinemaHall(string exploreCatId, string theatreName, string address, string area, string displayImage, string noOfScreens, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addCinemaHall", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("theatreName", theatreName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("displayImage", displayImage);
                cmd.Parameters.AddWithValue("noOfScreens", noOfScreens);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetCinemaHallById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetCinemaHallById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateCinemaHall(string cinemaId, string theatreName, string address, string area, string displayImage, string noOfScreens, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateCinemaHall", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("cinemaId", cinemaId);
                cmd.Parameters.AddWithValue("theatreName", theatreName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("displayImage", displayImage);
                cmd.Parameters.AddWithValue("noOfScreens", noOfScreens);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveCinemaHall(string cinemaId, string theatreName, string address, string area, string displayImage, string noOfScreens, string contactNo, string otherDescription, string updatedBy, string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveCinemaHall", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("cinemaId", cinemaId);
                cmd.Parameters.AddWithValue("theatreName", theatreName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("displayImage", displayImage);
                cmd.Parameters.AddWithValue("noOfScreens", noOfScreens);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //Hotel
        public string AddHotel(string exploreCatId, string hotelName, string address, string area, string starRating, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addHotel", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("hotelName", hotelName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("starRating", starRating);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetHotelById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetHotelById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateHotel(string hotelId, string hotelName, string address, string area, string starRating, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateHotel", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("hotelId", hotelId);
                cmd.Parameters.AddWithValue("hotelName", hotelName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("starRating", starRating);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveHotel(string hotelId, string hotelName, string address, string area, string starRating, string contactNo, string otherDescription, string updatedBy,string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveHotel", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("hotelId", hotelId);
                cmd.Parameters.AddWithValue("hotelName", hotelName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("starRating", starRating);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId",checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }
        //Govt Offices

        public string AddGovtOffice(string exploreCatId, string gcatId, string gcatName, string address, string area, string serviceId, string services, string contactNo, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddGovtOffice", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("gcatId", gcatId);
                cmd.Parameters.AddWithValue("gcatName", gcatName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("serviceId", serviceId);
                cmd.Parameters.AddWithValue("services", services);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetGovtOfficeById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetGovtOfficeById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateGovtOffice(string govtOfficeId, string gcatId, string gcatName, string address, string area, string serviceId, string services, string contactNo, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateGovtOffice", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("govtOfficeId", govtOfficeId);
                cmd.Parameters.AddWithValue("gcatId", gcatId);
                cmd.Parameters.AddWithValue("gcatName", gcatName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("serviceId", serviceId);
                cmd.Parameters.AddWithValue("services", services);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string EditApproveGovtOffice(string govtOfficeId, string gcatId, string gcatName, string address, string area, string serviceId, string services, string contactNo, string otherDescription, string updatedBy, string checkerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("EditApproveGovtOffice", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("govtOfficeId", govtOfficeId);
                cmd.Parameters.AddWithValue("gcatId", gcatId);
                cmd.Parameters.AddWithValue("gcatName", gcatName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("serviceId", serviceId);
                cmd.Parameters.AddWithValue("services", services);
                cmd.Parameters.AddWithValue("contactNo", contactNo);

                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);
                cmd.Parameters.AddWithValue("checkerId", checkerId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        //Tourism
        public string AddTourism(string exploreCatId, string touristSpotName, string address, string area, string contactNo, string coverImage, string activitiesId, string activities, string distanceFrom, string otherDescription, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddTourism", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("touristSpotName", touristSpotName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("contactNo", contactNo);
                cmd.Parameters.AddWithValue("coverImage", coverImage);
                cmd.Parameters.AddWithValue("activitiesId", activitiesId);
                cmd.Parameters.AddWithValue("activities", activities);
                cmd.Parameters.AddWithValue("distanceFrom", distanceFrom);
                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetTourismById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetTourismById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string UpdateTourism(string tourismId, string touristSpotName, string address, string area, string contactNo, string coverImage, string activitiesId, string activities, string distanceFrom, string otherDescription, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateTourism", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("tourismId", tourismId);
                cmd.Parameters.AddWithValue("touristSpotName", touristSpotName);
                cmd.Parameters.AddWithValue("address", address);

                cmd.Parameters.AddWithValue("area", area);
                cmd.Parameters.AddWithValue("contactNo", contactNo);
                cmd.Parameters.AddWithValue("coverImage", coverImage);
                cmd.Parameters.AddWithValue("activitiesId", activitiesId);
                cmd.Parameters.AddWithValue("activities", activities);
                cmd.Parameters.AddWithValue("distanceFrom", distanceFrom);
                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        public DataTable GetTopSixEntryDetails()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getTopSixEntryDetails", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetTopSixEntryDetailsByMaker(string adminId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetTopSixEntryDetailsByMaker", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("adminId", adminId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string AddFilterType(string exploreCatId, string filterType, string imageName)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddFilterType", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("filterType", filterType);
                cmd.Parameters.AddWithValue("imageName", imageName);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetFilterTypeById(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetFilterTypeById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId ", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetGovtCategory()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetGovtCategory", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetFilterTypeById1(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetFilterTypeById1", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId ", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntries()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntries", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesByMakerId(string insertedBy)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesByMakerId", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetAllEntriesForChecker(string checkerId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesForChecker", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesForSearch(string status, string exploreCatId, string team, string insertedBy)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesForSearch", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("status", status);
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("team", team);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception )
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesForSearchByChecker(string status, string exploreCatId, string team, string checkerId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesForSearchByChecker", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("status", status);
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("team", team);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetAllEntriesById(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesByIdAdmin", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesByIdWebservice(string exploreCatId, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesByIdForExport(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesByIdForExport", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllEntriesByIdBetDatesForExport(string exploreCatId,DateTime fromDate,DateTime toDate)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEntriesByIdBetDatesForExport", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("fromDate", fromDate);
                cmd.Parameters.AddWithValue("toDate", toDate);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetEntryByIdBatch(string exploreCatId, string batch)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetEntryByIdBatch", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("batch", batch);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllGovtEntriesById(string exploreCatId, string govtCatId, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllGovtEntriesById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("govtCatId", govtCatId);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAllServiceGovtEntriesById(string exploreCatId, string serviceId, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllServiceGovtEntriesById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("serviceId", serviceId);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetGovtCategoryServices(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetGovtCategoryServices", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable ContextualSearchResult(string keyword, string exploreCatId, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("ContextualSearchResult", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("keyword", keyword);
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable getFilterSearch(string keyword, string exploreCatId, string distanceFrom, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getFilterSearch", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("keyword", keyword);
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                cmd.Parameters.AddWithValue("distanceFrom", distanceFrom);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetAllActiveEntriesById(string exploreCatId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllActiveEntriesById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetEntryByEntryId(string exploreCatId, string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetEntryByEntryId", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("exploreCatId", exploreCatId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }



        public DataTable SearchEntryByNameArea(string searchText)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchEntryByNameArea", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable SearchEntryByNameAreaByMaker(string searchText,string insertedBy)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchEntryByNameArea", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable SearchEntryByNameAreaForChecker(string searchText,string checkerId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchEntryByNameAreaForChecker", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GlobalSearchResult(string searchText, string Page, string RecsPerPage)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GlobalSearchResult", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                cmd.Parameters.AddWithValue("Page", Convert.ToInt32(Page));
                cmd.Parameters.AddWithValue("RecsPerPage", Convert.ToInt32(RecsPerPage));
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string DeleteEntry(string exploreCat, string id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("deleteEntry", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCat", exploreCat);
                cmd.Parameters.AddWithValue("id", id);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        public string ApproveRejectEntry(string exploreCat, string id, string checkerId, string status)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("ApproveRejectEntry", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("exploreCat", exploreCat);
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                cmd.Parameters.AddWithValue("status", status);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string ApproveRejectOffer(string id, string checkerId,string supercheckerId, string cstatus, string scstatus)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("ApproveRejectOffer", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("checkerId", checkerId);
                cmd.Parameters.AddWithValue("supercheckerId", supercheckerId);
                cmd.Parameters.AddWithValue("cstatus", cstatus);
                cmd.Parameters.AddWithValue("scstatus", scstatus);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        //App User Section

        public DataTable GetAllUsers()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllUsers", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetUserById(string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetUserById", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("userId", userId);
                Openconn();
                da.Fill(dt);
                _cn.Close();
            }
            catch (Exception)
            {
                //ignored
            }
            return dt;
        }

        public DataTable SearchUserByNameMobile(string searchText)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchUserByNameMobile", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }



        public DataTable GetAllUserByType(string signUpType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllUserByType", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("signUpType", signUpType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public string DeleteUser(string userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("DeleteUser", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string ChangeUserStatus(string userId, string isActive)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("ChangeUserStatus", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("isActive", isActive);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        //Offers & Promotions

        public DataTable GetOffersPromotionById(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetOffersPromotionById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id ", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string AddOfferPromotion(string offerName, string bannerImage, string address, string otherDescription,DateTime validTill, string insertedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddOfferPromotion", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("offerName", offerName);
                cmd.Parameters.AddWithValue("bannerImage", bannerImage);

                cmd.Parameters.AddWithValue("address", address);
                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("validTill", validTill);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string UpdateOfferPromotion(string id,string offerName, string bannerImage, string address, string otherDescription,DateTime validTill, string updatedBy)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateOfferPromotion", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("offerName", offerName);
                cmd.Parameters.AddWithValue("bannerImage", bannerImage);
                cmd.Parameters.AddWithValue("address", address);
                cmd.Parameters.AddWithValue("otherDescription", otherDescription);
                cmd.Parameters.AddWithValue("validTill", validTill);
                cmd.Parameters.AddWithValue("updatedBy", updatedBy);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }



        public DataTable SearchOfferByName(string searchText)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchOfferByName", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable SearchOfferByNameMakerId(string searchText,string insertedBy)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SearchOfferByNameMakerId", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("searchText", searchText);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetOfferPromotionByType(string type)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetOfferPromotionByType", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("type", type);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public string DeleteOfferPromotion(string id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("DeleteOfferPromotion", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("id", id);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string AddAdmin(string name, string emailId, string mobileNo, string loginPassword, string adminType, string team)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddAdmin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("name", name);
                cmd.Parameters.AddWithValue("emailId", emailId);
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);

                cmd.Parameters.AddWithValue("loginPassword", loginPassword);
                cmd.Parameters.AddWithValue("adminType", adminType);
                cmd.Parameters.AddWithValue("team", team);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee)
            {
                if (ee.Message.ToLower().Contains("cannot insert duplicate key row in object"))
                {
                    return "Mobile number already exists !!";
                }
                else
                {
                    return ee.Message;
                }
            }
        }

        public string UpdateAdmin(string name, string emailId, string mobileNo, string loginPassword, string adminType, string team, string adminId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateAdmin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("name", name);
                cmd.Parameters.AddWithValue("emailId", emailId);
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);

                cmd.Parameters.AddWithValue("loginPassword", loginPassword);
                cmd.Parameters.AddWithValue("adminType", adminType);
                cmd.Parameters.AddWithValue("team", team);
                cmd.Parameters.AddWithValue("adminId", adminId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee)
            {
                if (ee.Message.ToLower().Contains("cannot insert duplicate key row in object"))
                {
                    return "Mobile number already exists !!";
                }
                else
                {
                    return ee.Message;
                }
            }
        }

        public DataTable GetAdminsByType(string adminType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminsByType", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("adminType", adminType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAdminsByTypeTeam(string adminType, string team)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminsByTypeTeam", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("adminType", adminType);
                cmd.Parameters.AddWithValue("team", team);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetActiveAdminsByType(string adminType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetActiveAdminsByType", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("adminType", adminType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception )
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAdminsByTypePrivilege(string privilegeType, string adminType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminsByTypePrivilege", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("privilegeType", privilegeType);
                cmd.Parameters.AddWithValue("adminType", adminType);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAdminsByTypePrivilegeTeam(string privilegeType, string adminType, string team)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminsByTypePrivilegeTeam", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("privilegeType", privilegeType);
                cmd.Parameters.AddWithValue("adminType", adminType);
                cmd.Parameters.AddWithValue("team", team);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetAdminPrivilegeByAdminId(string adminId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminPrivilegeByAdminId", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("adminId", adminId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public string AddAdminPrivilege(string pId, string adminId, string isChecked)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AddAdminPrivilege", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("pId", pId);
                cmd.Parameters.AddWithValue("adminId", adminId);
                cmd.Parameters.AddWithValue("isChecked", isChecked);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string ChangeAdminStatus(string adminId, string isActive)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("ChangeAdminStatus", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("adminId", adminId);
                cmd.Parameters.AddWithValue("isActive", isActive);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string DeleteAdmin(string adminId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("DeleteAdmin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("adminId", adminId);
                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetAdminById(string adminId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAdminById", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("adminId ", adminId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string getRandomID()
        {
            Random random = new Random();
            string[] array = new string[60] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "Y", "Z", "_", ".", "@", "#", "$", "%", "-", "*" };
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < 8; i++)
            {
                int abc = random.Next(0, 59);
                sb.Append(array[abc]);
            }
            string randomID = sb.ToString();
            return randomID;
        }

    }
}