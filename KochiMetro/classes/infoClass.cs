﻿using System;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace KochiMetro.classes
{
    public class InfoClass
    {
        private readonly SqlConnection _cn = new SqlConnection(ConfigurationManager.ConnectionStrings["kochiMetroConnectionString"].ConnectionString);

        public void Openconn()
        {
            try { _cn.Open(); }
            catch { _cn.Close(); _cn.Open(); }
        }

        public DataTable SignUp(string mobileNo, string udob, string referralCode, string referredUserCode, string veriPin, string signUpType, string imeiNo, string deviceType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("signUp1", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("udob", Convert.ToDateTime(udob, CultureInfo.GetCultureInfo("hi-IN")));
                cmd.Parameters.AddWithValue("referralCode", referralCode);
                cmd.Parameters.AddWithValue("referredUserCode", referredUserCode);
                cmd.Parameters.AddWithValue("signUpType", signUpType);
                cmd.Parameters.AddWithValue("veriPin", veriPin);
                cmd.Parameters.AddWithValue("imeiNo", imeiNo);
                cmd.Parameters.AddWithValue("deviceType", deviceType);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();
            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable ManualSignUp(string pname, string emailId, string mobileNo, string udob, string referralCode, string referredUserCode, string veriPin, string signUpType, string imeiNo, string deviceType)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("manualSignUp", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("pname", pname);
                cmd.Parameters.AddWithValue("emailId", emailId);
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("udob", Convert.ToDateTime(udob, CultureInfo.GetCultureInfo("hi-IN")));
                cmd.Parameters.AddWithValue("referralCode", referralCode);
                cmd.Parameters.AddWithValue("referredUserCode", referredUserCode);

                cmd.Parameters.AddWithValue("signUpType", signUpType);


                cmd.Parameters.AddWithValue("veriPin", veriPin);
                cmd.Parameters.AddWithValue("imeiNo", imeiNo);
                cmd.Parameters.AddWithValue("deviceType", deviceType);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();
            }
            catch (Exception)
            {
                // ignored
            }

            return dt;
        }

        public string UpdateProfile(string pname, string emailId, string mobileNo, string udob, string userId)
        {
            SqlCommand cmd = new SqlCommand("updateProfile", _cn) { CommandType = CommandType.StoredProcedure };

            cmd.Parameters.AddWithValue("pname", pname);
            cmd.Parameters.AddWithValue("emailId", emailId);
            cmd.Parameters.AddWithValue("mobileNo", mobileNo);
            cmd.Parameters.AddWithValue("udob", Convert.ToDateTime(udob, CultureInfo.GetCultureInfo("hi-IN")));
            cmd.Parameters.AddWithValue("userId", userId);

            Openconn();
            string i = Convert.ToString(cmd.ExecuteScalar());
            _cn.Close();
            return i;



        }


        public string GetMyVerificationPin(string userId)
        {
            SqlCommand cmd = new SqlCommand("getMyVerificationPin", _cn) { CommandType = CommandType.StoredProcedure };

            cmd.Parameters.AddWithValue("userId", userId);

            Openconn();
            string i = Convert.ToString(cmd.ExecuteScalar());
            _cn.Close();
            return i;
        }

        public string ResendVerficationPin(string userId, string veriPin)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("resendVerficationPin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("veriPin", veriPin);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        public string VerifyOtp(string userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("verifyOTP", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string SetMpin(string userId, string mPin)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("setMpin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("mPin", mPin);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string ForgetMpin(string mobileNo, string veriPin)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("forgetMpin", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                cmd.Parameters.AddWithValue("veriPin", veriPin);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }


        public DataTable GetUserDetailsBymobileNo(string mobileNo)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getUserDetailsBymobileNo", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("mobileNo", mobileNo);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string GetMyMPin(string mobileNo)
        {
            SqlCommand cmd = new SqlCommand("getMyMPin", _cn) { CommandType = CommandType.StoredProcedure };

            cmd.Parameters.AddWithValue("mobileNo", mobileNo);

            Openconn();
            string i = Convert.ToString(cmd.ExecuteScalar());
            _cn.Close();
            return i;
        }

        public string GetMyMPinByuserId(string userId)
        {
            SqlCommand cmd = new SqlCommand("getMyMPinByuserId", _cn) { CommandType = CommandType.StoredProcedure };

            cmd.Parameters.AddWithValue("userId", userId);

            Openconn();
            string i = Convert.ToString(cmd.ExecuteScalar());
            _cn.Close();
            return i;
        }


        public string AddSosContact(string userId, string contactName, string contactNumber)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("addSOsContact", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("contactName", contactName);
                cmd.Parameters.AddWithValue("contactNumber", contactNumber);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetSosDetails(string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getSosDetails", _cn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.AddWithValue("userId", userId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public string EditSosContact(string userId, string sosId, string contactName, string contactNumber)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("editSOSContact", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("sosId", sosId);
                cmd.Parameters.AddWithValue("contactName", contactName);
                cmd.Parameters.AddWithValue("contactNumber", contactNumber);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public string DeleteSosContact(string sosId, string userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("deleteSOsContact", _cn) { CommandType = CommandType.StoredProcedure };

                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("sosId", sosId);

                Openconn();
                string i = Convert.ToString(cmd.ExecuteScalar());
                _cn.Close();
                return i;
            }
            catch (Exception ee) { return ee.Message; }
        }

        public DataTable GetServiceSubCategories()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getServiceSubCategories", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetExploreKochiCategories()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("getExploreKochiCategories", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetOffersPromotions()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetOffersPromotions", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetIssuanceCentre()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetIssuanceCentre", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


        public DataTable GetOffersPromotionsByMakerId(string insertedBy)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetOffersPromotionsByMakerId", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("insertedBy", insertedBy);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }

        public DataTable GetPrivilegeTypes()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("GetPrivilegeTypes", _cn) { CommandType = CommandType.StoredProcedure };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                Openconn();
                da.Fill(dt);
                _cn.Close();

            }
            catch (Exception)
            {
                // ignored
            }
            return dt;
        }


    }
}