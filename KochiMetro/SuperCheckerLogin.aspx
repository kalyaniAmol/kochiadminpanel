﻿<%@ Page Title="Super Checker Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SuperCheckerLogin.aspx.cs" Inherits="KochiMetro.SuperCheckerLogin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style>
        div.footer p.copyright
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="DivToHide">
        <uc1:messages ID="messages2" runat="server" />
    </div>
      <!--login form here-->
    <div class="login-form" style="top: 145px;">
        <h3 class="login-title">
            Sign In</h3>
        <ul class="form">
            <li>
                <label>
                    Mobile No.:</label>
                <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="11" CssClass="text" onkeydown="return (!(event.keyCode>=65) && event.keyCode!=32);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                    ControlToValidate="txtMobileNo" Text="Mobile required" ForeColor="Red"></asp:RequiredFieldValidator>
                <%--   <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtMobileNo"
                    ValidChars="0123456789" FilterMode="ValidChars">
                </asp:FilteredTextBoxExtender>--%>
            </li>
            <li>
                <label>
                    Password:</label>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="RequiredFieldValidator"
                    ControlToValidate="txtPassword" Text="Password required" ForeColor="Red"></asp:RequiredFieldValidator>
            </li>
            <li class="submit-button">
                <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" class="submit-btn" />
            </li>
            <li style="text-align: center;"><a class="forgot-password" href="#">Forgot Password
            </a>
                <br />
            </li>
        </ul>
    </div>
</asp:Content>
