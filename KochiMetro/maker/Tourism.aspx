﻿<%@ Page Title="Tourism" Language="C#" MasterPageFile="~/MakerMaster.Master" AutoEventWireup="true" CodeBehind="Tourism.aspx.cs" Inherits="KochiMetro.maker.Tourism" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style>
        select
        {
            float: left;
            width: 40%;
            height: 25px;
            font-family: calibri;
            font-size: 14px;
            color: #555555;
            padding: 0px 10px;
            border: none;
            border: 1px solid #ccc;
            background: #fafafa;
        }
        .vsboxim
        {
            float: left;
        }
        .vsboximg
        {
            width: 100px;
            height: 120px;
            float: left;
            text-align: center;
            margin: 10px;
            box-shadow: 0px 1px 2px 0px #ccc;
            -moz-box-shadow: 0px 1px 2px 0px #ccc;
            -webkit-box-shadow: 0px 1px 2px 0px #ccc;
        }
        a.vsboximga
        {
            border-radius: 2px;
            padding: 4px 10px;
            background: #cb0202;
            color: #fff;
            text-decoration: none;
            font-weight: bold;
            font-size: 11px;
        }
        a.vsboximga:hover
        {
            background: #fd0000;
        }
        img.vsboximg1
        {
            width: 100%;
            height: 90px;
            margin-bottom: 5px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        function Validate_Checkbox() {
            var chks = $("#<%= chkActivity.ClientID %> input:checkbox");

            var hasChecked = false;
            for (var i = 0; i < chks.length; i++) {
                if (chks[i].checked) {
                    hasChecked = true;
                    break;
                }
            }
            if (hasChecked == false) {
                alert("Please select at least one checkbox..!");

                return false;
            }

        }     
    </script>
    <style>
        table#ContentPlaceHolder1_chkActivity
        {
            margin-bottom: 10px;
        }
        table#ContentPlaceHolder1_chkActivity label
        {
            width: 80%;
            font-size: 12px;
            float: right;
            margin: 0;
            padding: 0;
        }
        table#ContentPlaceHolder1_chkActivity td
        {
            border: 0;
        }
        table#ContentPlaceHolder1_chkActivity input[type="checkbox"]
        {
            width: 20px;
            float: left;
            margin: 0;
        }
    </style>
    <script type="text/javascript">
        function UploadFile(fileUpload) {
            if (fileUpload.value != '') {
                alert('hi');
                document.getElementById("<%=Button1.ClientID %>").click();
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <asp:Label ID="lblExploreKochiId" runat="server" Text="" Visible="False"></asp:Label>
            <asp:Label ID="lblTourismId" runat="server" Text="" Visible="False"></asp:Label>
            <asp:Label ID="lblImage" runat="server" Text="" Visible="False"></asp:Label>
            <h2 class="page-title">
                Tourism</h2>
            <div class="content-box">
                <ul class="dashboard-form">
                    <asp:Panel ID="Panel2" runat="server">
                        <li>
                            <label>
                                Name:</label>
                            <asp:TextBox ID="txtName" runat="server" CssClass="text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtName" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </li>
                        <li>
                            <label>
                                Address:</label>
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtAddress" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </li>
                        <li>
                            <label>
                                Area:</label>
                            <asp:TextBox ID="txtArea" runat="server" CssClass="text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtArea" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </li>
                        <li>
                            <label>
                                Phone No.:</label>
                            <asp:TextBox ID="txtContactNo" runat="server" CssClass="text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtContactNo" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                                ValidChars="0123456789" FilterMode="ValidChars">
                            </asp:FilteredTextBoxExtender>
                        </li>
                        <li>
                            <label>
                                Upload Cover Image:</label>
                            <asp:FileUpload ID="uploadCover" runat="server" />
                            <asp:Button ID="Button1" Text="Upload" runat="server" OnClick="Upload" Style="display: none" CausesValidation="false" />
                            <%-- <asp:LinkButton ID="btnUpload" runat="server" CausesValidation="false" OnClick="btnUpload_Click">Upload</asp:LinkButton>--%>
                            <p class="upload-txt">
                                Image should be 1680x1050 in size (.jpeg, .png format only)</p>
                        </li>
                        <li>
                            <asp:Panel ID="Panel1" runat="server" Visible="false">
                                <label>
                                    &nbsp;</label><div class="vsboxim">
                                        <div class="vsboximg">
                                            <img id="ImgCover" runat="server" class="vsboximg1" />
                                            <asp:LinkButton ID="btnDelete" OnClick="btnDelete_Click" runat="server" CausesValidation="false"
                                                CssClass="vsboximga">Delete</asp:LinkButton>
                                        </div>
                                    </div>
                            </asp:Panel>
                        </li>
                        <li>
                            <label>
                                Activities:</label>
                            <asp:CheckBoxList ID="chkActivity" runat="server" DataTextField="filterType" DataValueField="id"
                                RepeatColumns="4" RepeatDirection="Horizontal">
                            </asp:CheckBoxList>
                            <%--   <asp:DropDownList ID="ddType" runat="server" DataTextField="filterType"
                                    DataValueField="id" OnDataBinding="ddType_DataBinding" OnDataBound="ddType_DataBound">
                                </asp:DropDownList>--%>
                            <asp:LinkButton ID="btnAddActivity" runat="server" OnClick="btnAddActivity_Click"
                                CssClass="add-btn" CausesValidation="false"> + </asp:LinkButton>
                        </li>
                        <li>
                            <label>
                                Distance from Ernakulam South Railway Station</label>
                            <asp:DropDownList ID="ddType" runat="server" >
                                 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                <asp:ListItem Value="Within 25 km">Within 25 km</asp:ListItem>
                                <asp:ListItem Value="25-50 km">25-50 km</asp:ListItem>
                                <asp:ListItem Value="50-100 km">50-100 km</asp:ListItem>
                                <asp:ListItem Value="100 km +">100 km +</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li>
                            <label>
                                Other Description:</label>
                            <div style="float: left; width: 66%; height: 93%;">
                                <CKEditor:CKEditorControl ID="CKEditorDesc" BasePath="/ckeditor/" runat="server">
                                </CKEditor:CKEditorControl></div>
                        </li>
                        <li>
                            <div class="add-button">
                                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                                    Visible="False" />
                                <asp:Button ID="btnAdd" runat="server" Text="Save Details" CssClass="add-btn" OnClick="btnAdd_Click" />
                            </div>
                        </li>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server" Visible="false">
                        <li>
                            <label>
                                Activity:</label>
                            <asp:TextBox ID="txtActivity" runat="server" CssClass="text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="txtActivity" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </li>
                        <li>
                            <label>
                                Upload Activity Image:</label>
                            <asp:FileUpload ID="uploadActivityImage" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="RequiredFieldValidator"
                                ControlToValidate="uploadActivityImage" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            <p class="upload-txt">
                                Image should be 1680x1050 in size (.jpeg, .png format only)</p>
                        </li>
                        <li>
                            <div class="add-button">
                                <asp:Button ID="btnSaveActivity" runat="server" Text="Save" CssClass="add-btn" OnClick="btnSaveActivity_Click"
                                    CausesValidation="false" />
                                <asp:Button ID="btnPanelBack" runat="server" Text="Back" CssClass="add-btn back"
                                    OnClick="btnPanelBack_Click" CausesValidation="false" />
                            </div>
                        </li>
                    </asp:Panel>
                </ul>
            </div>
        </ContentTemplate>
         <Triggers>
                <asp:PostBackTrigger ControlID="Button1" />
            </Triggers>
    </asp:UpdatePanel>
</asp:Content>
