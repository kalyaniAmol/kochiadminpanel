﻿using System;
using System.Web;


namespace KochiMetro.maker
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Maker"))
            {
                if (!IsPostBack)
                {
                }

            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
        }
    }
}