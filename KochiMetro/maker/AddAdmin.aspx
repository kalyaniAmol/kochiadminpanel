﻿<%@ Page Title="Add Admin" Language="C#" MasterPageFile="~/MakerMaster.Master" AutoEventWireup="true" CodeBehind="AddAdmin.aspx.cs" Inherits="KochiMetro.maker.AddAdmin" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.7.1005, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style>
        select
        {
            float: left;
            width: 40%;
            height: 30px;
            font-family: calibri;
            font-size: 14px;
            color: #555555;
            padding: 0px 10px;
            border: none;
            border: 1px solid #ccc;
            background: #fafafa;
        }div.content-box ul.dashboard-form li div.add-button input.add-btn{font-size:11px;padding:6px 10px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #666; z-index: 999;
                    -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75); -o-filter: alpha(opacity=75);
                    filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75; padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/ajax-loader2.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lbladminId" runat="server" Text="" Visible="False"></asp:Label>
          <div class="c-vsssc" style="margin-left:20%;">  <h2 class="page-title" style="margin-top:20px;margin-left:0;">
                Create User</h2>
            <div class="content-box">
                <div id="DivToHide">
                    <uc1:messages ID="messages1" runat="server" />
                </div>
                <ul class="dashboard-form">
                    <li>
                        <label>
                            Name:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtName" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Email ID:</label>
                        <asp:TextBox ID="txtemailId" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtemailId" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid ID"
                            ControlToValidate="txtemailId" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </li>
                    <li>
                        <label>
                            Mobile No. :</label>
                        <asp:TextBox ID="txtContactNo" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtContactNo" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                            ValidChars="0123456789" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                    </li>
                    <li>
                        <label>
                            Team:</label>
                        <asp:DropDownList ID="ddTeam" runat="server">
                            <asp:ListItem Value="--Select Team--">--Select Team--</asp:ListItem>
                            <asp:ListItem Value="KMRL">KMRL</asp:ListItem>
                            <asp:ListItem Value="Checker">Axis</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="ddType" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Admin Type:</label>
                        <asp:DropDownList ID="ddType" runat="server">
                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                            <asp:ListItem Value="Maker">Maker</asp:ListItem>
                            <asp:ListItem Value="Checker">Checker</asp:ListItem>
                            <asp:ListItem Value="Super Checker">Super Checker</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="ddType" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <div class="add-button" style="float: none;    width: 42%;
    text-align: right;">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                                Visible="False" />
                            <asp:Button ID="btnAdd" runat="server" Text="Create User" CssClass="add-btn" OnClick="btnAdd_Click" />
                        </div>
                    </li>
                </ul>
            </div>  </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
