﻿<%@ Page Title="Bulk Upload" Language="C#" MasterPageFile="~/MakerMaster.Master" AutoEventWireup="true" CodeBehind="BulkUpload.aspx.cs" Inherits="KochiMetro.maker.BulkUpload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style>
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding:5px 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
        }
        a.large-btn
        {
            margin-right: 10px;
            font-size: 12px;
        }
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 32px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        
        #search a.button:hover
        {
            background-color: #000;
        }
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding:5px 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
            margin-left: 10px;
        }
        input.add-btn
        {
            width: auto;
            cursor: pointer;
            background: #c0cf2f;
            border-radius: 3px;
            height: auto;
            padding: 8px;
            color: #444;
            font-family: 'Lato' , sans-serif;
            border: none;
            font-size: 12px;
            margin-top: 10px;
        }
        input#ContentPlaceHolder1_btnBack
        {
            background: #ccc;
        }
        a.large-btn
        {
            font-size: 11px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <%-- <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:Label ID="lblExploreKochiId" runat="server" Text="" Visible="False"></asp:Label>
    <asp:Label ID="lblBatchId" runat="server" Text="" Visible="False"></asp:Label>
    <h2 class="page-title">
        Manage Bulk Data</h2>
    <div id="DivToHide">
        <uc1:messages ID="messages1" runat="server" />
    </div>
    <div class="content-box">
        <asp:Panel ID="Panel1" runat="server">
            <div class="search-select">
                <div class="select-box">
                    <asp:DropDownList ID="ddCategory" runat="server" DataTextField="category" DataValueField="exploreCatId"
                        OnSelectedIndexChanged="ddCategory_SelectedIndexChanged" OnDataBinding="ddCategory_DataBinding"
                        OnDataBound="ddCategory_DataBound" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <asp:LinkButton ID="btnUploadData" CssClass="large-btn" runat="server" OnClick="btnUploadData_Click">Upload Bulk Data</asp:LinkButton>
                <asp:LinkButton ID="btnDownloadBulkData" CssClass="large-btn" runat="server" OnClick="btnDownloadBulkData_Click">Download Bulk Data</asp:LinkButton>
                <asp:LinkButton ID="btnDownloadSample" CssClass="large-btn" runat="server" OnClick="btnDownloadSample_Click">Download Sample Data File</asp:LinkButton>
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <ul class="dashboard-form">
                <li>
                    <label style="padding: 18px 0px;">
                        Upload Excel:</label>
                    <asp:FileUpload ID="uploadExcel" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                        ControlToValidate="uploadExcel" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:Button ID="btnAdd" runat="server" Text="Upload" CssClass="add-btn" OnClick="btnAdd_Click" />
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                        CausesValidation="false" />
                </li>
            </ul>
            <asp:GridView ID="GridView1" Visible="false" runat="server" CssClass="explore-listing vstabless">
            </asp:GridView>
            <asp:GridView ID="gridData" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                PageSize="15" CssClass="explore-listing vstabless" DataKeyNames="id" EmptyDataText="No entries to show !!"
                OnPageIndexChanging="gridData_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="Sr.No." ItemStyle-Height="20">
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1 %>
                        </ItemTemplate>
                        <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="entryName" HeaderText="Name Of Entry" />
                    <asp:TemplateField HeaderText="Date Of Entry" SortExpression="insertDate1">
                        <ItemTemplate>
                            <%#Eval("insertDate1")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="address" HeaderText="Address" />
                    <asp:BoundField DataField="area" HeaderText="Area" />
                    <asp:BoundField DataField="contactNo" HeaderText="Contact No" />
                    <asp:BoundField DataField="name" HeaderText="Maker" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server">
            <ul class="dashboard-form">
                <li>
                    <label style="padding: 10px 0px; font-size: 18px;">
                        Select Date:</label><br />
                    <br />
                    <br />
                </li>
                <li>
                    <label style="padding: 10px 0px;">
                        From Date</label></li>
                <li>
                    <asp:TextBox ID="txtFrom" runat="server" MaxLength="10" CssClass="text"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFrom"
                        FilterMode="ValidChars" ValidChars="0123456789./-">
                    </asp:FilteredTextBoxExtender>
                    <br />
                    <br />
                </li>
                <li>
                    <label style="padding: 10px 0px;">
                        To Date
                    </label>
                </li>
                <li>
                    <asp:TextBox ID="txtTo" runat="server" MaxLength="10" CssClass="text"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTo"
                        FilterMode="ValidChars" ValidChars="0123456789./-">
                    </asp:FilteredTextBoxExtender>
                    <br />
                    <br />
                </li>
                <li>
                    <asp:CheckBox ID="chkAllData" runat="server" />
                    Get All Data</li>
                <li>
                    <div class="add-button" style="float: none; width: 500px; text-align: right;">
                        <asp:Button ID="btnGetBulk" runat="server" Text="Get Data" CssClass="add-btn" OnClick="btnGetBulk_Click" />
                        <asp:Button ID="btnBackBulk" runat="server" Text="Back" CssClass="add-btn back" CausesValidation="false"
                            OnClick="btnBackBulk_Click" />
                    </div>
                </li>
            </ul>
        </asp:Panel>
    </div>
    <div style="display: none;">
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true" CssClass="explore-listing vstabless">
            <Columns>
                <%--<asp:BoundField DataField="category" HeaderText="Category" />
                <asp:BoundField DataField="entryName" HeaderText="Name Of Entry" />
                <asp:BoundField DataField="address" HeaderText="Address" />
                <asp:BoundField DataField="area" HeaderText="Area" />
                <asp:BoundField DataField="contactNo" HeaderText="Contact No" />
                <asp:BoundField DataField="speciality" HeaderText="Speciality" />
                <asp:BoundField DataField="fuelComapny" HeaderText="Fuel Company" />
                <asp:BoundField DataField="pType" HeaderText="Parking Type" />
                <asp:BoundField DataField="policeType" HeaderText="Police Type" />
                <asp:BoundField DataField="noOfscreens" HeaderText="Screens" />
                <asp:BoundField DataField="starRating" HeaderText="Rating" />
                <asp:BoundField DataField="activities" HeaderText="Activities" />
                <asp:BoundField DataField="distanceFrom" HeaderText="Distance" />
                <asp:BoundField DataField="name" HeaderText="Maker" />
                <asp:BoundField DataField="checkerName" HeaderText="Checker" />
                <asp:BoundField DataField="status" HeaderText="status" />
                <asp:TemplateField HeaderText="Date Of Entry" SortExpression="insertDate1">
                    <ItemTemplate>
                        <%#Eval("insertDate1")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="otherDescription" HeaderText="Description" />--%>
            </Columns>
        </asp:GridView>
    </div>
    <%-- </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAdd" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
