﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.maker
{
    public partial class GovernmentOffices : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(','); static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Maker"))
                {
                    prevPage = Request.UrlReferrer.ToString();
                    DataTable dtCat = _ob.GetGovtCategory();
                    if (dtCat.Rows.Count > 0)
                    {
                        ddCategory.DataSource = dtCat;
                        ddCategory.DataBind();
                    }

                    try
                    {
                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        if (dtFilter.Rows.Count > 0)
                        {
                            chkServices.DataSource = dtFilter;
                            chkServices.DataBind();
                        }
                        Session.Remove("exploreId");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    try
                    {
                        lblGovtId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetGovtOfficeById(lblGovtId.Text);
                        lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        if (dtFilter.Rows.Count > 0)
                        {
                            chkServices.DataSource = dtFilter;
                            chkServices.DataBind();
                        }

                        ddCategory.SelectedValue = dt.Rows[0]["gcatId"].ToString();
                        txtAddress.Text = dt.Rows[0]["address"].ToString();
                        txtArea.Text = dt.Rows[0]["area"].ToString();
                        txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                        CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                        string[] splittedserviceId = dt.Rows[0]["serviceId"].ToString().Split(',');

                        for (int j = 0; j < chkServices.Items.Count; j++)
                        {
                            for (int i = 0; i < splittedserviceId.Length; i++)
                            {
                                if (splittedserviceId[i].ToString() == chkServices.Items[j].Value)
                                {
                                    chkServices.Items[j].Selected = true;
                                }
                            }
                        }

                        btnBack.Visible = true;
                        btnAdd.Text = "Update Details";
                        Session.Remove("id");
                    }
                    catch (Exception)
                    {
                        // ignored

                    }
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddCategory.SelectedValue != "--Select--")
                {
                    string res;
                    string services = "";
                    string serviceId = "";

                    for (int i = 0; i < chkServices.Items.Count; i++)
                    {
                        if (chkServices.Items[i].Selected)
                        {
                            services = services + "," + chkServices.Items[i].Text;
                            serviceId = serviceId + "," + chkServices.Items[i].Value;
                        }
                    }
                    services = services.TrimStart(',');
                    serviceId = serviceId.TrimStart(',');

                    if (btnAdd.Text == "Save Details")
                    {
                        res = _ob.AddGovtOffice(lblExploreKochiId.Text, ddCategory.SelectedValue, ddCategory.SelectedItem.Text, txtAddress.Text, txtArea.Text, serviceId,
                      services, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                        if (res.Contains("Success"))
                        {
                            messages1.SetMessage(1, "Details saved successfully!! ");
                            messages1.Visible = true;
                            ClearTextBoxes();
                        }

                    }
                    if (btnAdd.Text == "Update Details")
                    {
                        res = _ob.UpdateGovtOffice(lblGovtId.Text, ddCategory.SelectedValue, ddCategory.SelectedItem.Text, txtAddress.Text, txtArea.Text, serviceId,
                      services, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                        if (res.Contains("Success"))
                        {
                            ClearTextBoxes();
                            Session["status"] = "Success";
                            Response.Redirect(prevPage);
                        }
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please Select category first !! ");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtAddress.Text = "";
            txtArea.Text = "";
            ddCategory.DataBind();
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void ddCategory_DataBinding(object sender, EventArgs e)
        {
            ddCategory.Items.Clear();
        }

        protected void ddCategory_DataBound(object sender, EventArgs e)
        {
            ddCategory.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", ""));
        }
    }
}