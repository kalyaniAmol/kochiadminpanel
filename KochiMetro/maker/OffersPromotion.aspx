﻿<%@ Page Title="Offers & Promotions" Language="C#" MasterPageFile="~/MakerMaster.Master"
    AutoEventWireup="true" CodeBehind="OffersPromotion.aspx.cs" Inherits="KochiMetro.maker.OffersPromotion" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function openpopup(id) {
            // alert(id);
            //Calculate Page width and height 
            var pageWidth = window.innerWidth;
            var pageHeight = window.innerHeight;
            if (typeof pageWidth != "number") {
                if (document.compatMode === "CSS1Compat") {
                    pageWidth = document.documentElement.clientWidth;
                    pageHeight = document.documentElement.clientHeight;
                } else {
                    pageWidth = document.body.clientWidth;
                    pageHeight = document.body.clientHeight;
                }
            }
            //Make the background div tag visible...
            var divbg = document.getElementById('bg');
            divbg.style.visibility = "visible";

            var divobj = document.getElementById(id);
            divobj.style.visibility = "visible";
            var computedStyle;
            if (navigator.appName === "Microsoft Internet Explorer")
                computedStyle = divobj.currentStyle;
            else computedStyle = document.defaultView.getComputedStyle(divobj, null);
            //Get Div width and height from StyleSheet 
            var divWidth = computedStyle.width.replace('px', '');
            var divHeight = computedStyle.height.replace('px', '');
            var divLeft = (pageWidth - divWidth) / 2;
            var divTop = (pageHeight - divHeight) / 2;
            //Set Left and top coordinates for the div tag 
            divobj.style.left = divLeft + "px";
            divobj.style.top = divTop + "px";
            //Put a Close button for closing the popped up Div tag 
            if (divobj.innerHTML.indexOf("closepopup('" + id + "')") < 0)
                divobj.innerHTML = "<a href=\"#\" onclick=\"closepopup('" + id + "')\"><span class=\"close_button\"><img src='/asset/close.png'></span></a>" + divobj.innerHTML;
        }
        function closepopup(id) {
            var divbg = document.getElementById('bg');
            divbg.style.visibility = "hidden";
            var divobj = document.getElementById(id);
            divobj.style.visibility = "hidden";
        }
    </script>
    <style>
        .vsmaint
        {
            width: 100%;
        }
        .vsmaint tr td label
        {
            font-weight: bold;
        }
        .vsmaint tr td p
        {
            margin: 0;
        }
        .vsmaint tr td
        {
            border: 0px;
            vertical-align: top;
            border-bottom: 1px solid #eee;
        }
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding: 5px 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 223px;
        }
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 32px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        #search a.button:hover
        {
            background-color: #000;
        }
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding: 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 200px;
            margin-left: 0px;
        }
    </style>
    <style type="text/css">
        .popup
        {
            height: 500px;
            width: 80%;
            position: fixed;
            visibility: hidden;
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            text-align: justify;
            padding: 0px;
            z-index: 9999;
            top: 30px;
        }
        .popup_bg
        {
            position: fixed;
            visibility: hidden;
            height: 100%;
            width: 100%;
            left: 0;
            top: 0;
            -webkit-filter: alpha(opacity=80);
            -moz-filter: alpha(opacity=80);
            -o-filter: alpha(opacity=80);
            filter: alpha(opacity=80); /* for IE */
            -ms-opacity: 0.8;
            opacity: 0.8; /* CSS3 standard */
            background-color: #9fddea;
            z-index: 91;
        }
        .close_button
        {
            position: absolute;
            right: 76px;
            top: -26px;
            font-family: Verdana, Geneva, sans-serif;
            font-size: small;
            font-weight: bold;
            float: right;
            color: #666;
            display: block;
            text-decoration: none;
        }
        body
        {
            margin: 0;
            overflow-x: hidden;
        }
        ol.vscheckist input
        {
            margin: 5px;
        }
        ol.vscheckist
        {
            width: 100%;
            float: left;
            list-style: none;
        }
        ol.vscheckist li
        {
            width: 32%;
            padding: 10px;
            float: left;
        }
        ol.vscheckist label
        {
            margin: 5px;
        }
        div.select-box select
        {
            padding: 7px 10px;
        }
    </style>
    <style type="text/css">
        .tooltip1
        {
            border-bottom: 1px dotted #000000;
            color: #000000;
            outline: none;
            cursor: help;
            text-decoration: none;
            position: relative;
        }
        .tooltip1 span
        {
            margin-left: -999em;
            position: absolute;
            float: right;
        }
        .tooltip1:hover span
        {
            border-radius: 5px 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1);
            -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
            -: mmoz-box-shadow:5px5pxrgba(0, 0, 0, 0.1);
            font-family: Calibri,Tahoma,Geneva,sans-serif;
            position: absolute;
            right: 1em;
            margin-left: 0;
            top: 2em;
            z-index: 99;
            width: 450px;
        }
        .tooltip1:hover img
        {
            border: 0;
            margin: -10px 0 0 -55px;
            float: right;
            position: absolute;
        }
        .tooltip1:hover em
        {
            font-family: Candara, Tahoma, Geneva, sans-serif;
            font-size: 1.2em;
            font-weight: bold;
            display: block;
            padding: 0.2em 0 0.6em 0;
        }
        .classic
        {
            padding: 0.8em 1em;
        }
        .custom
        {
            padding: 0.5em 0.8em 0.8em 2em;
        }
        * html a:hover
        {
            background: transparent;
        }
        .classic
        {
            background: #FFFFAA;
            border: 1px solid #FFAD33;
        }
        .critical
        {
            background: #FFCCAA;
            border: 1px solid #FF3334;
        }
        .help
        {
            background: #9FDAEE;
            border: 1px solid #2BB0D7;
        }
        .info
        {
            background: #9FDAEE;
            border: 1px solid #2BB0D7;
        }
        table.explore-listing img
        {
            -moz-transition: all 0.3s;
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
        }
        table.explore-listing img:hover
        {
            -moz-transform: scale(1.6);
            -webkit-transform: scale(1.6);
            transform: scale(1.6);
        }
        .warning
        {
            background: #FFFFAA;
            border: 1px solid #FFAD33;
        }
        .tooltip
        {
            position: relative;
            display: inline-block;
        }
        .tooltip .tooltiptext
        {
            visibility: hidden;
            width: 600px;
            background-color: #eee;
            color: #000;
            text-align: justify;
            border: 2px solid #2d3e4e;
            padding: 5px 0;
            color: #000; /* Position the tooltip */
            position: fixed;
            z-index: 1;
            top: 40%;
            right: 7.5%;
            z-index: 999999999;
        }
        .tooltiptext a
        {
            color: #000;
        }
        .tooltip:hover .tooltiptext
        {
            visibility: visible;
        }
        table.explore-listing
        {
            margin-top: 0;
        }
        a.large-btn
        {
            font-size: 11px;
            margin-left: 5px;
            padding: 8px 10px;
        }
        #btnDiv
        {
            position: relative;
            width: 220px;
            float: right;
        }
        #btnDiv1
        {
            position: absolute;
            z-index: 2;
            height: 30px;
            width: 220px;
        }
        #btnDiv2
        {
            position: absolute;
            z-index: 1;
        }
        #btnDiv a
        {
            opacity: 0.4;
        }
        div.container .main
        {
            width: 92%;
            height: 100%;
        }
        div.select-box
        {
            float: left;
            width: 25%;
            height: 30px;
        }
        div.wrapper .inner
        {
            float: left;
            width: 100%;
            min-height: 900px;
            padding: 0px 2% 11% 2%;
        }
    </style>
    <script>

        function showImg() {

            $("table.explore-listing tr th a").append("<img src='/images/sortvs.png'>");
        }

    </script>
    <script type="text/javascript">
        // Select/Deselect checkboxes based on header checkbox
        function SelectheaderCheckboxes(headerchk) {
            debugger
            var gvcheck = document.getElementById('ContentPlaceHolder1_gridData');
            var div = document.getElementById('btnDiv');
            var i;
            //Condition to check header checkbox selected or not if that is true checked all checkboxes
            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = true;
                    document.getElementById("hiddenFlag").value = gvcheck.rows.length;

                    //alert(document.getElementById("hiddenFlag").value);
                }
            }
            //if condition fails uncheck all checkboxes in gridview
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = false;
                    document.getElementById("hiddenFlag").value = 0;

                    //alert(document.getElementById("hiddenFlag").value);
                }
            }
        }
        //function to check header checkbox based on child checkboxes condition
        function Selectchildcheckboxes(header) {
            var ck = header;
            var count = 0;
            var gvcheck = document.getElementById('ContentPlaceHolder1_gridData');
            var headerchk = document.getElementById(header);
            var rowcount = gvcheck.rows.length;
            var div = document.getElementById('btnDiv');

            //By using this for loop we will count how many checkboxes has checked
            for (i = 1; i < gvcheck.rows.length; i++) {

                var inputs = gvcheck.rows[i].getElementsByTagName('input');
                if (inputs[0].checked) {
                    count++;

                }
            }

            //Condition to check all the checkboxes selected or not
            if (count == rowcount - 1) {
                headerchk.checked = true;
                document.getElementById("hiddenFlag").value = rowcount - 1;

            }
            else {
                headerchk.checked = false; document.getElementById("hiddenFlag").value = 0;

            }
        }
    </script>
    <script src="/asset/js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").on("click", "input[type='checkbox']", function () {
                if ($(this).is(":checked")) {
                    $("#btnDiv1").hide();
                    $('#btnDiv a').css("opacity", "1"); //$('#btnDiv a').removeAttr('disabled');
                    //$('#btnDiv a').attr('href', _href);
                } else {
                    $("#btnDiv1").show();
                    $('#btnDiv a').css("opacity", "0.4"); //$('#btnDiv a').attr('disabled', 'disabled');
                    //$('#btnDiv a').removeAttr('href');
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <input id="hiddenFlag" type="hidden" />
            <asp:Label ID="lblExploreKochiId" runat="server" Text="" Visible="False"></asp:Label>
            <h2 class="page-title">
                Offers & Promotions</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <div class="search-select">
                    <div id="search">
                        <form id="tfnewsearch" method="get" action="#">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="search" placeholder="Search by name"></asp:TextBox>
                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click"></asp:LinkButton>
                        </form>
                    </div>
                    <%-- <a class="large-btn" href="/admin/.aspx">Add Data</a>--%>
                </div>
                <div class="search-select" style="margin: 10px 2.7%;">
                    <a class="large-btn" href="/admin/AddOffersPromotion.aspx">Add Data</a>
                    <div id="btnDiv">
                        <div id="btnDiv1">
                        </div>
                        <div id="btnDiv2">
                            <asp:LinkButton ID="btnDeleteEntry" runat="server" CssClass="large-btn" CausesValidation="false"
                                OnClick="btnDeleteEntry_Click">Delete</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <asp:GridView ID="gridData" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EmptyDataText="No offer or promotions to show !!" AllowSorting="True" OnSorting="gridData_Sorting"
                    PageSize="15" CssClass="explore-listing vstabless" OnRowCreated="gridData_RowCreated"
                    OnRowCommand="gridData_RowCommand" DataKeyNames="id" OnPageIndexChanging="gridData_PageIndexChanging"
                    OnRowDataBound="gridData_RowDataBound">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkheader" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkchild" runat="server" onclick="javascript:Selectchildcheckboxes(this)" />
                            </ItemTemplate>
                            <HeaderStyle VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sr.No." ItemStyle-Height="20">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" />
                        <asp:BoundField DataField="statusByChecker" HeaderText="statusByChecker" ReadOnly="True" />
                        <asp:BoundField DataField="statusBySuperChecker" HeaderText="statusBySuperChecker"
                            ReadOnly="True" />
                        <asp:TemplateField HeaderText="Banner">
                            <ItemTemplate>
                                <asp:Image ID="Image3" runat="server" Height="50px" ImageUrl='<%#"~\\images\\offerImages\\"+Eval("bannerImage") %>'
                                    Width="50px" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name Of Offer">
                            <ItemTemplate>
                                <div style="width: 100px;">
                                    <%# Eval("offerName")%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Of Entry" SortExpression="insertDate1">
                            <ItemTemplate>
                                <%#Eval("insertDate1")%>
                                <itemstyle height="20px" horizontalalign="Center" verticalalign="Middle" width="40px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Validity" SortExpression="validTill1">
                            <ItemTemplate>
                                <%#Eval("validTill1")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <div style="width: 200px;">
                                    <%# Eval("address")%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeny" runat="server" CausesValidation="false" CommandName="change"
                                    Text="Edit" CommandArgument='<%# Eval("id") %>' />
                                &nbsp; &Iota; &nbsp;
                                <asp:LinkButton ID="btnReturn" runat="server" CausesValidation="false" CommandName="remove"
                                    Text="Delete" CommandArgument='<%# Eval("id") %>' OnClientClick="return confirm('Are you sure you want to delete this Record?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="name" HeaderText="Maker" />
                        <asp:BoundField DataField="checkerName" HeaderText="Checker" />
                        <asp:BoundField DataField="superCheckerName" HeaderText="S. Checker" />
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Final Status">
                            <ItemTemplate>
                                <asp:Image ID="Image2" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Info">
                            <ItemTemplate>
                                <div class="tooltip">
                                    <img src="../Images/info.png" alt="Help" height="48" width="48" />
                                    <span class="tooltiptext">
                                        <div style="width: 99%; height: 350px; overflow: auto; padding: 5px">
                                            <asp:Label ID="remarks" runat="server" Text='<%# Eval("otherdescription") %>'></asp:Label>
                                            <%--  <%# Eval("otherdescription") %>--%>
                                        </div>
                                    </span>
                                </div>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <p class="note">
                    <strong>Note:</strong> If approved data edited or deleted by maker, checker again
                    needs to follow approval cycle.</p>
                <ul class="legend">
                    <li>
                        <p>
                            <strong>Legend:</strong></p>
                    </li>
                    <li>
                        <p>
                            <img src="/images/pending.png" />Pending</p>
                    </li>
                    <li>
                        <p>
                            <img src="/images/approved.png" />Approved</p>
                    </li>
                    <li>
                        <p>
                            <img src="/images/rejected.png" />Rejected</p>
                    </li>
                    <li>
                        <p>
                            <img src="/images/edit_approve.png" />Edit and Approve</p>
                    </li>
                    <li>
                        <p>
                            <img src="/images/sort.png" />Reorder</p>
                    </li>
                </ul>
            </div>
            <div id="popup1" class="popup">
                <asp:Panel ID="Panel4" runat="server">
                    <div class="content-box" style="min-height: 330px;">
                        <ul class="dashboard-form">
                            <li>
                                <div class="profpic">
                                    <img id="imgOffer" runat="server" src="/images/dummy-article-img.jpg">
                                </div>
                                <%-- <h4 class="name">
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </h4>--%>
                                <%--  <a href="#" class="grid-btn">
                                <img src="/images/_grid.png"></a>--%>
                            </li>
                            <li>
                                <label>
                                    Name :</label>
                                <p class="view-text">
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Type :</label>
                                <p class="view-text">
                                    <asp:Label ID="lblType" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Date of Entry:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Details:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblDetails" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                        </ul>
                    </div>
                </asp:Panel>
            </div>
            <div id="bg" class="popup_bg">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
