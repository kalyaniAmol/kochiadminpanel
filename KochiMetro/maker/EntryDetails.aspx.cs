﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro.maker
{
    public partial class EntryDetails : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin" || uDetails[2] == "Maker")
                {
                    try
                    {
                        if (Session["status"].ToString() == "Success")
                        {
                            messages1.SetMessage(1, "Details updated successfully !!");
                            messages1.Visible = true;
                        }

                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        var dtCat = _obInfo.GetExploreKochiCategories();
                        if (dtCat.Rows.Count > 0)
                        {
                            ddCategory.DataSource = dtCat;
                            ddCategory.DataBind();
                        }

                        DataTable dtGridData = _ob.GetAllEntriesByMakerId(uDetails[0]);
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); //ViewState["dtbl"] = dtGridData;

                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/MakerLogin.aspx");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchEntryByNameAreaByMaker(txtSearch.Text, uDetails[0]);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); //ViewState["dtbl"] = dtGridData;
            }
            else
            {
                DataTable dtGridData = _ob.GetAllEntriesByMakerId(uDetails[0]);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); //ViewState["dtbl"] = dtGridData;
            }
        }

        protected void ddCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
            gridData.DataSource = dtGridData;
            gridData.DataBind(); //ViewState["dtbl"] = dtGridData;

        }

        protected void ddCategory_DataBound(object sender, EventArgs e)
        {
            ddCategory.Items.Insert(0, new ListItem("--Select Category--", "--Select Category--"));
        }

        protected void ddCategory_DataBinding(object sender, EventArgs e)
        {
            ddCategory.Items.Clear();
        }

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridData.PageIndex = e.NewPageIndex;
            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchEntryByNameArea(txtSearch.Text);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); //ViewState["dtbl"] = dtGridData;
            }
            else
            {
                DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
                gridData.DataSource = dtGridData;
                gridData.DataBind();
            }
        }

        protected void gridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;

            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;

            }
        }

        protected void gridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //int row = 0;
                if (e.CommandName == "details")
                {
                    DataTable dt;
                    string[] splittedArgs = e.CommandArgument.ToString().Split(';');
                    dt = _ob.GetEntryByEntryId(splittedArgs[1], splittedArgs[0]);
                    if (dt.Rows.Count > 0)
                    {
                        lblName.Text = dt.Rows[0]["entryName"].ToString();
                        lblAddress.Text = dt.Rows[0]["address"].ToString();
                        lblArea.Text = dt.Rows[0]["area"].ToString();
                        lblContact.Text = dt.Rows[0]["contactNo"].ToString();
                        lblOtherDescription.Text = dt.Rows[0]["otherDescription"].ToString();
                        int catId = Convert.ToInt32(splittedArgs[1]);

                        switch (catId)
                        {
                            case 1:
                                lblType.Text = "Speciality";
                                lblSpeciality.Text = dt.Rows[0]["speciality"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = false;
                                break;
                            case 2:
                                lblType.Text = "Services";
                                lblSpeciality.Text = dt.Rows[0]["services"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = false;
                                break;
                            case 3:
                                break;
                            case 4:
                                lblType.Text = "Parking type";
                                lblSpeciality.Text = dt.Rows[0]["pType"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = false;
                                break;
                            case 5:
                                lblType.Text = "Police Station";
                                lblSpeciality.Text = dt.Rows[0]["policeType"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = false;
                                break;
                            case 6:
                                lblType.Text = "Activities";
                                lblSpeciality.Text = dt.Rows[0]["activities"].ToString();
                                trDistance.Visible = true;
                                lblDistance.Text = dt.Rows[0]["distanceFrom"].ToString();
                                liImg.Visible = true;
                                imgName.Src = "~\\images\\tourism\\" + dt.Rows[0]["displayImage"].ToString();
                                break;
                            case 7:
                                lblType.Text = "No. of screens";
                                lblSpeciality.Text = dt.Rows[0]["noOfScreens"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = true;
                                imgName.Src = "~\\images\\cinema\\" + dt.Rows[0]["displayImage"].ToString();
                                break;
                            case 8:
                                lblType.Text = "Star Rating";
                                lblSpeciality.Text = dt.Rows[0]["starRating"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = false;
                                break;
                            case 9:
                                lblType.Text = "Fuel Company";
                                lblSpeciality.Text = dt.Rows[0]["fuelcomapny"].ToString();
                                trDistance.Visible = false;
                                liImg.Visible = true;
                                imgName.Src = "~\\images\\fuel\\" + dt.Rows[0]["displayImage"].ToString();

                                break;

                        }

                        string script = "<script type=\"text/javascript\">openpopup('popup1')</script>";
                        ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
                    }

                }
                if (e.CommandName == "change")
                {
                    string[] splittedArgs = e.CommandArgument.ToString().Split(';');
                    Session["id"] = splittedArgs[0];
                    Response.Redirect("/maker/" + splittedArgs[3]);
                    // Server.Transfer(splittedArgs[3]);
                    string script = "<script type=\"text/javascript\">myfunction()</script>";
                    ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
                }
                if (e.CommandName == "remove")
                {
                    messages1.Visible = false;
                    string[] splittedArgs = e.CommandArgument.ToString().Split(';');
                    string i = _ob.DeleteEntry(splittedArgs[1], splittedArgs[0]);
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Entry deleted successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); //ViewState["dtbl"] = dtGridData;
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (e.Row.Cells[4].Text == "Pending")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\pending.png";
                }
                if (e.Row.Cells[4].Text == "Approved")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\approved.png";
                } if (e.Row.Cells[4].Text == "Rejected")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\rejected.png";
                } if (e.Row.Cells[4].Text == "Edit and Approve")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\edit_approve.png";
                }
            }
        }

        protected void gridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataTable dataTable = ViewState["dtbl"] as DataTable;
            //string sortingDirection;
            //if (Direction == SortDirection.Ascending)
            //{
            //    Direction = SortDirection.Descending;
            //    sortingDirection = "Desc";
            //    string script = "<script type=\"text/javascript\">showImg()</script>";
            //    ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
            //}
            //else
            //{
            //    Direction = SortDirection.Ascending;
            //    sortingDirection = "Asc";
            //    string script = "<script type=\"text/javascript\">showImg()</script>";
            //    ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
            //}

            //DataView sortedView = new DataView(dataTable);
            //sortedView.Sort = e.SortExpression + " " + sortingDirection;
            //gridData.DataSource = sortedView;
            //gridData.DataBind();
        }

        public SortDirection Direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
            gridData.DataSource = dtGridData;
            gridData.DataBind();
        }

        protected void ddTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
            gridData.DataSource = dtGridData;
            gridData.DataBind();
        }

        protected void btnDeleteEntry_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gridData.Rows.Count; i++)
                {
                    CheckBox cb = (CheckBox)gridData.Rows[i].Cells[1].FindControl("chkchild");

                    if (cb != null)
                    {
                        if (cb.Checked)
                        {
                            string res = _ob.DeleteEntry(gridData.Rows[i].Cells[4].Text, gridData.Rows[i].Cells[3].Text);
                        }
                    }

                }
                messages1.SetMessage(1, "Deleted Successfully !!");
                messages1.Visible = true;
                DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
                gridData.DataSource = dtGridData;
                gridData.DataBind();

            }
            catch (Exception)
            {

            }
        }

        protected void ddStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtGridData = _ob.GetAllEntriesForSearch(ddStatus.SelectedValue, ddCategory.SelectedValue, ddTeam.SelectedValue, uDetails[0]);
            gridData.DataSource = dtGridData;
            gridData.DataBind();
        }

    }
}