﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Data;
using System.Web.UI;

namespace KochiMetro.maker
{
    public partial class ExploreKochiCetegories : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Maker"))
            {
                if (!IsPostBack)
                {
                    var dtCat = _obInfo.GetExploreKochiCategories();
                    if (dtCat == null) throw new ArgumentNullException();
                    rptExploreKochi.DataSource = dtCat;
                    rptExploreKochi.DataBind();

                    DataTable dtGridData = _ob.GetTopSixEntryDetailsByMaker(uDetails[0]);
                    gridData.DataSource = dtGridData;
                    gridData.DataBind();

                    try
                    {
                        if (Session["status"].ToString() == "Success")
                        {
                            messages1.SetMessage(1, "Deatils updated successfully !!");
                            messages1.Visible = true;
                        }

                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            {
                Response.Redirect("/MakerLogin.aspx");
            }
        }

        protected void ImageShow_Click(object sender, EventArgs e)
        {
            LinkButton btndetails = sender as LinkButton;
            if (btndetails != null)
            {
                string id = btndetails.CommandArgument;
                string[] splittedArgs = id.Split(';');
                Session["exploreId"] = splittedArgs[0];
                Response.Redirect(splittedArgs[1]);
            }
        }

        protected void gridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        protected void gridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "change")
                {
                    string[] splittedArgs = e.CommandArgument.ToString().Split(';');
                    Session["id"] = splittedArgs[0];
                    Response.Redirect("/maker/" + splittedArgs[3]);
                }
                if (e.CommandName == "remove")
                {
                    messages1.Visible = false;
                    string[] splittedArgs = e.CommandArgument.ToString().Split(';');
                    string i = _ob.DeleteEntry(splittedArgs[1], splittedArgs[0]);
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Entry deleted successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _ob.GetTopSixEntryDetailsByMaker(uDetails[0]);
                        gridData.DataSource = dtGridData;
                        gridData.DataBind();
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}