﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;
using System.IO;
using System.Globalization;

namespace KochiMetro.maker
{
    public partial class AddOffersPromotion : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            uploadCover.Attributes["onchange"] = "UploadFile(this)";
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin" || uDetails[2] == "Maker")
                {
                    try
                    {
                        if (Session["id"].ToString().Length > 0)
                        {
                            lblID.Text = Session["id"].ToString();
                            DataTable dt = _ob.GetOffersPromotionById(lblID.Text);
                            if (dt.Rows.Count > 0)
                            {
                                //ddType.SelectedValue = dt.Rows[0]["offerName"].ToString();
                                txtName.Text = dt.Rows[0]["offerName"].ToString();
                                txtAddress.Text = dt.Rows[0]["address"].ToString();
                                txtValidity.Text = dt.Rows[0]["validTill1"].ToString();
                                CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                                lblImage.Text = dt.Rows[0]["bannerImage"].ToString();
                                ImgCover.Src = "~\\images\\offerImages\\" + lblImage.Text;

                                btnBack.Visible = true;
                                btnAdd.Text = "Update Details";
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/MakerLogin.aspx");
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string res;
                string imgName;

                if (btnAdd.Text == "Save Details")
                {
                    res = _ob.AddOfferPromotion(txtName.Text, lblImage.Text, txtAddress.Text, CKEditorDesc.Text, Convert.ToDateTime(txtValidity.Text, CultureInfo.GetCultureInfo("hi-IN")), uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        messages1.SetMessage(1, "Details saved successfully!! ");
                        messages1.Visible = true;
                        ClearTextBoxes();
                    }

                }
                if (btnAdd.Text == "Update Details")
                {
                    imgName = lblImage.Text;


                    res = _ob.UpdateOfferPromotion(lblID.Text, txtName.Text, lblImage.Text, txtAddress.Text, CKEditorDesc.Text, Convert.ToDateTime(txtValidity.Text, CultureInfo.GetCultureInfo("hi-IN")), uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        ClearTextBoxes();
                        Session["status"] = "Success";
                        Response.Redirect("/maker/OffersPromotion.aspx");
                    }
                }

            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }


        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtValidity.Text = "";
            CKEditorDesc.Text = "";
            Panel1.Visible = false;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("/maker/OffersPromotion.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Server.MapPath("~\\images\\offerImages\\" + lblImage.Text);
                FileInfo file = new FileInfo(path);
                if (file.Exists)//check file exsit or not
                {
                    file.Delete();
                }
                lblImage.Text = "";
                Panel1.Visible = false;
            }
            catch (Exception)
            {
            }

        }

        protected void Upload(object sender, EventArgs e)
        {
            try
            {
                if (uploadCover.HasFile)
                {
                    string fname = "";
                    string ext = System.IO.Path.GetExtension(uploadCover.FileName);
                    fname = uploadCover.FileName;
                    string extToUpper = ext.ToUpper();
                    if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG" || extToUpper == ".GIF" || extToUpper == ".BMP")
                    {
                        try
                        {
                            System.Drawing.Image UploadedImage = System.Drawing.Image.FromStream(uploadCover.PostedFile.InputStream);
                            if (UploadedImage.PhysicalDimension.Width <= 1680 && UploadedImage.PhysicalDimension.Height <= 1050)
                            {
                                Random random = new Random();
                                string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                for (int ij = 0; ij < 6; ij++)
                                {
                                    int abc = random.Next(0, 53);
                                    sb.Append(array[abc]);
                                }
                                string[] splitname = fname.Split('.');
                                string tourismImage = "IMG" + splitname[0].ToString() + sb.ToString() + ext;
                                uploadCover.SaveAs(Server.MapPath("~\\images\\offerImages\\" + tourismImage));
                                lblImage.Text = tourismImage;
                                ImgCover.Src = "~\\images\\offerImages\\" + lblImage.Text;
                                Panel1.Visible = true;
                            }
                            else
                            {
                                messages1.SetMessage(2, "Image size should be 1680 By 1050 px");
                                messages1.Visible = true;
                            }

                        }
                        catch (Exception)
                        {
                            //ignored
                        }

                    }
                    else
                    {
                        //string script = "<script type=\"text/javascript\">alert('Allowed file extensions are .PNG .BMP .JPG .JPEG .GIF')</script>";
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    }
                }
                else
                {
                    messages1.SetMessage(0, "Please upload image first !");
                    messages1.Visible = true;
                }
            }
            catch (Exception)
            {
                //ignored
            }
        }
    }
}