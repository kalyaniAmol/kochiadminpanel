﻿<%@ Page Title="Explore Kochi" Language="C#" MasterPageFile="~/MakerMaster.Master"
    AutoEventWireup="true" CodeBehind="ExploreKochiCetegories.aspx.cs" Inherits="KochiMetro.maker.ExploreKochiCetegories" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding: 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
        }
        
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 40px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        
        #search a.button:hover
        {
            background-color: #000;
        }
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding: 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
            margin-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2 class="page-title">
                Add Data in Categories</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <div class="categories-list">
                    <ul class="list-items">
                        <asp:Repeater ID="rptExploreKochi" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:LinkButton ID="ImageButton1" OnClick="ImageShow_Click" runat="server" CausesValidation="False"
                                        CommandArgument='<%#Eval("exploreCatId") +";"+ Eval("linkPath")%>'> <img src="<%#"/images/"+Eval("imageName") %>" >
                    <p>
                           <%# DataBinder.Eval(Container.DataItem, "category") %></p> </asp:LinkButton>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li></li>
                    </ul>
                </div>
                <h3 class="inner-title">
                    Recently Added</h3>
                <asp:GridView ID="gridData" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="explore-listing" OnRowCreated="gridData_RowCreated" OnRowCommand="gridData_RowCommand"
                    DataKeyNames="id">
                    <Columns>
                        <asp:TemplateField HeaderText="Sr.No." ItemStyle-Height="20">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
                        <asp:BoundField DataField="exploreCatId" HeaderText="exploreCatId" ReadOnly="True"
                            SortExpression="exploreCatId" />
                        <asp:BoundField DataField="category" HeaderText="Category Name" />
                        <asp:BoundField DataField="entryName" HeaderText="Name Of Entry" />
                        <asp:BoundField DataField="insertDate1" HeaderText="Date Of Entry" />
                        <asp:BoundField DataField="area" HeaderText="Area" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeny" runat="server" CausesValidation="false" CommandName="change"
                                    Text="Edit" CommandArgument='<%# Eval("cmdArg") %>' />
                                &nbsp; &Iota; &nbsp;
                                <asp:LinkButton ID="btnReturn" runat="server" CausesValidation="false" CommandName="remove"
                                    Text="Delete" CommandArgument='<%# Eval("cmdArg") %>' OnClientClick="return confirm('Are you sure you want to delete this Record?');" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
