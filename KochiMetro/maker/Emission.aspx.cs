﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.maker
{
    public partial class Emission : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(','); static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
            {
                if (!IsPostBack)
                {
                    prevPage = Request.UrlReferrer.ToString();
                    try
                    {
                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        if (Session["id"].ToString().Length <= 0) return;
                        lblEmissionId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetEmissionTestingById(lblEmissionId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            txtName.Text = dt.Rows[0]["emissionName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();

                            btnBack.Visible = true;
                            btnAdd.Text = "Update Details";
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
            }
            else
            {
                Response.Redirect("/MakerLogin.aspx");
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string res;
                if (btnAdd.Text == "Save Details")
                {
                    res = _ob.AddEmissionTesting(lblExploreKochiId.Text, txtName.Text, txtAddress.Text, txtArea.Text,
                        txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        messages1.SetMessage(1, "Emission details saved successfully!! ");
                        messages1.Visible = true;
                        ClearTextBoxes();
                    }
                }
                if (btnAdd.Text == "Update Details")
                {
                    res = _ob.UpdateEmissionTesting(lblEmissionId.Text, txtName.Text, txtAddress.Text, txtArea.Text,
                       txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        ClearTextBoxes();
                        Session["status"] = "Success";
                        Response.Redirect(prevPage);
                    }
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}