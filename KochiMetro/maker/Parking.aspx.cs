﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.maker
{
    public partial class Parking : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(','); static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Maker"))
                {
                    prevPage = Request.UrlReferrer.ToString();
                    try
                    {
                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        if (dtFilter.Rows.Count > 0)
                        {
                            chkParkingType.DataSource = dtFilter;
                            chkParkingType.DataBind();
                        }

                        Session.Remove("exploreId");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    try
                    {
                        if (Session["id"].ToString().Length > 0)
                        {
                            lblParkingId.Text = Session["id"].ToString();
                            DataTable dt = _ob.GetParkingStationById(lblParkingId.Text);
                            if (dt.Rows.Count > 0)
                            {
                                lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                                DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                                if (dtFilter.Rows.Count > 0)
                                {
                                    chkParkingType.DataSource = dtFilter;
                                    chkParkingType.DataBind();
                                }

                                txtName.Text = dt.Rows[0]["parkingName"].ToString();
                                txtAddress.Text = dt.Rows[0]["address"].ToString();
                                txtArea.Text = dt.Rows[0]["area"].ToString();
                                chkParkingType.SelectedValue = dt.Rows[0]["pType"].ToString();
                                txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                                CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                                string[] splittedParkingId = dt.Rows[0]["pTypeId"].ToString().Split(',');

                                for (int j = 0; j < chkParkingType.Items.Count; j++)
                                {
                                    for (int i = 0; i < splittedParkingId.Length; i++)
                                    {
                                        if (splittedParkingId[i].ToString() == chkParkingType.Items[j].Value)
                                        {
                                            chkParkingType.Items[j].Selected = true;
                                        }
                                    }
                                }
                                btnBack.Visible = true;
                                btnAdd.Text = "Update Details";
                                Session.Remove("id");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/MakerLogin.aspx");
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string parkingType = "";
                string parkingId = "";
                string res;

                for (int i = 0; i < chkParkingType.Items.Count; i++)
                {
                    if (chkParkingType.Items[i].Selected)
                    {

                        parkingType = parkingType + "," + chkParkingType.Items[i].Text;
                        parkingId = parkingId + "," + chkParkingType.Items[i].Value;
                    }
                }
                parkingType = parkingType.TrimStart(',');
                parkingId = parkingId.TrimStart(',');

                if (btnAdd.Text == "Save Details")
                {
                    res = _ob.AddParkingStation(lblExploreKochiId.Text, txtName.Text, txtAddress.Text, txtArea.Text, parkingId, parkingType, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        messages1.SetMessage(1, "Parking details saved successfully!! ");
                        messages1.Visible = true;
                        ClearTextBoxes();
                    }
                }
                if (btnAdd.Text == "Update Details")
                {
                    res = _ob.UpdateParkingStation(lblParkingId.Text, txtName.Text, txtAddress.Text, txtArea.Text, parkingId, parkingType, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        ClearTextBoxes();
                        Session["status"] = "Success";
                        Response.Redirect(prevPage);
                    }
                }

            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            chkParkingType.ClearSelection();
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}