﻿<%@ Page Title="Set Privilege" Language="C#" MasterPageFile="~/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="SetPrivilege.aspx.cs" Inherits="KochiMetro.admin.SetPrivilege" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        fieldset
        {
            border: 3px solid #ccc;
            width: 270px;
            padding: 20px 10px;
            float: left;
            margin-right: 20px;
            height: 320px;
            overflow-y: hidden;
        }
        a.large-btn
        {
            padding: 10px 20px;
        }
        legend
        {
            padding: 8px;
            font-weight: bold;
            font-size: 16px;
            margin-left: 15px;
        }
        .vstableo1
        {
            height: 300px;
            overflow-y: scroll;
        }
        .vstableo tr td
        {
            border: 0;
            padding: 3px;
            vertical-align: top;
            line-height: 24px;
        }
    </style>
    <style>
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding: 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
        }
        
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 40px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        
        #search a.button:hover
        {
            background-color: #000;
        }
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding: 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
            margin-left: 10px;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        function Validate_Checkbox() {
            var chks = $("#<%= CheckBoxList1.ClientID %> input:checkbox");

            var hasChecked = false;
            for (var i = 0; i < chks.length; i++) {
                if (chks[i].checked) {
                    hasChecked = true;
                    break;
                }
            }
            if (hasChecked == false) {
                alert("Please select at least one admin..!");

                return false;
            }

        }     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <h2 class="page-title" style="padding-left: 20%; margin-top: 20px;">
        Set Privillages<asp:LinkButton ID="btnEdit" runat="server" OnClick="btnEdit_Click" CssClass="large-btn">Edit</asp:LinkButton>
        <%--<a class="large-btn" href="#">Edit</a>--%>
    </h2>
    <div id="DivToHide">
        <uc1:messages ID="messages1" runat="server" />
    </div>
    <div class="content-box" style="padding-left: 20%;">
        <div class="search-select" style="margin: 30px 0;">
            <div class="select-box" style="margin-left: -10px;">
                <asp:DropDownList ID="ddType" runat="server" OnSelectedIndexChanged="ddType_SelectedIndexChanged"
                    AutoPostBack="true">
                    <asp:ListItem Value="--Set privileges for--">--Set privileges for--</asp:ListItem>
                    <asp:ListItem Value="Maker">Maker</asp:ListItem>
                    <asp:ListItem Value="Checker">Checker</asp:ListItem>
                    <asp:ListItem Value="Super Checker">Super Checker</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="select-box" style="margin-left: 75px;">
                <asp:DropDownList ID="ddTeam" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddTeam_SelectedIndexChanged">
                    <asp:ListItem Value="--Select Team--">--Select Team--</asp:ListItem>
                    <asp:ListItem Value="KMRL">KMRL</asp:ListItem>
                    <asp:ListItem Value="Checker">Axis</asp:ListItem>
                </asp:DropDownList>
            </div>
            
        </div>
        <asp:Panel ID="Panel1" runat="server">
            <div style="width: 100%; float: left;" class="vs11">
                <form>
                <fieldset>
                    <legend>Modules</legend>
                    <table class="vstableo">
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkExploreKochi" runat="server" />
                            </td>
                            <td>
                                <b>Explore Kochi</b><br />
                                - View Details & Status<br />
                                - Add & Manage Data
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkOffers" runat="server" />
                            </td>
                            <td>
                                <b>Offer & Promotions</b><br />
                                - View Details & Status<br />
                                - Add & Manage Offers<br />
                                - Add & Manage Promo
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkUser" runat="server" />
                            </td>
                            <td>
                                <b>Portal User</b> - Add Portal Users<br />
                                - Manage Makers<br />
                                - Manage Checkers<br />
                                - Manage Supercheckers<br />
                                - Set Privileges
                            </td>
                        </tr>
                    </table>
                </fieldset>
                </form>
                <form>
                <fieldset style="width: 5%; border: 0;">
                    &nbsp;</fieldset>
                </form>
                <fieldset>
                    <legend>Users</legend>
                    <div class="vstableo1">
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataTextField="name" DataValueField="adminId"
                            RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="OrderedList">
                        </asp:CheckBoxList>
                        <asp:Label ID="lblUsers" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </fieldset>
            </div>
            <div style="width: 100%; float: left;" class="vs11">
                <p style="padding-left: 27%;">
                    <br />
                    <br />
                    <asp:LinkButton ID="btnSet" runat="server" CssClass="large-btn" OnClientClick="javascript:Validate_Checkbox();"
                        Style="float: none;" OnClick="btnSet_Click">Set</asp:LinkButton>
                    <br />
                </p>
            </div>
        </asp:Panel>
    </div>
    <%-- </ContentTemplate>
          <Triggers>
                <asp:PostBackTrigger ControlID="btnSet" />
            </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
