﻿<%@ Page Title="User Management" Language="C#" MasterPageFile="~/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="UserMgmt.aspx.cs" Inherits="KochiMetro.admin.UserMgmt" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function openpopup(id) {
            // alert(id);
            //Calculate Page width and height 
            var pageWidth = window.innerWidth;
            var pageHeight = window.innerHeight;
            if (typeof pageWidth != "number") {
                if (document.compatMode === "CSS1Compat") {
                    pageWidth = document.documentElement.clientWidth;
                    pageHeight = document.documentElement.clientHeight;
                } else {
                    pageWidth = document.body.clientWidth;
                    pageHeight = document.body.clientHeight;
                }
            }
            //Make the background div tag visible...
            var divbg = document.getElementById('bg');
            divbg.style.visibility = "visible";

            var divobj = document.getElementById(id);
            divobj.style.visibility = "visible";
            var computedStyle;
            if (navigator.appName === "Microsoft Internet Explorer")
                computedStyle = divobj.currentStyle;
            else computedStyle = document.defaultView.getComputedStyle(divobj, null);
            //Get Div width and height from StyleSheet 
            var divWidth = computedStyle.width.replace('px', '');
            var divHeight = computedStyle.height.replace('px', '');
            var divLeft = (pageWidth - divWidth) / 2;
            var divTop = (pageHeight - divHeight) / 2;
            //Set Left and top coordinates for the div tag 
            divobj.style.left = divLeft + "px";
            divobj.style.top = divTop + "px";
            //Put a Close button for closing the popped up Div tag 
            if (divobj.innerHTML.indexOf("closepopup('" + id + "')") < 0)
                divobj.innerHTML = "<a href=\"#\" onclick=\"closepopup('" + id + "')\"><span class=\"close_button\">X</span></a>" + divobj.innerHTML;
        }
        function closepopup(id) {
            var divbg = document.getElementById('bg');
            divbg.style.visibility = "hidden";
            var divobj = document.getElementById(id);
            divobj.style.visibility = "hidden";
        }
    </script>
<style> #search input { background: none repeat scroll 0 0 #fff; border: 1px solid #ccc; color: #7F7F7F; float: left; font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif; height: 20px; margin: 0; padding:5px 9px; -webkit-transition: background 0.3s ease-in-out 0; -moz-transition: background 0.3s ease-in-out 0; -ms-transition: background 0.3s ease-in-out 0; -o-transition: background 0.3s ease-in-out 0; transition: background 0.3s ease-in-out 0; width: 270px; } #search a.button { background: url("/images/search.png") no-repeat scroll center center #7eac10; cursor: pointer; float: left; height: 32px; text-indent: -99999em; -webkit-transition: background 0.3s ease-in-out 0; -moz-transition: background 0.3s ease-in-out 0; -ms-transition: background 0.3s ease-in-out 0; -o-transition: background 0.3s ease-in-out 0; transition: background 0.3s ease-in-out 0; width: 40px; border: 0 solid #fff; } #search a.button:hover { background-color: #000; } div.select-box select { background: none repeat scroll 0 0 #fff; border: 1px solid #ccc; color: #7F7F7F; float: left; font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif; margin: 0; padding:5px 10px; -webkit-transition: background 0.3s ease-in-out 0; -moz-transition: background 0.3s ease-in-out 0; -ms-transition: background 0.3s ease-in-out 0; -o-transition: background 0.3s ease-in-out 0; transition: background 0.3s ease-in-out 0; width: 270px; margin-left: 10px; } </style> <style type="text/css"> .popup { background-color: #fafafa; height: 400px; width: 600px; border: 5px solid #2ac4ea; position: absolute; visibility: hidden; font-family: Verdana, Geneva, sans-serif; font-size: small; text-align: justify; padding: 15px; overflow: auto; z-index: 2; } .popup_bg { position: absolute; visibility: hidden; height: 100%; width: 100%; left: 0; top: 0; -webkit-filter: alpha(opacity=80); -moz-filter: alpha(opacity=80); -o-filter: alpha(opacity=80); filter: alpha(opacity=80); /* for IE */ -ms-opacity: 0.8; opacity: 0.8; /* CSS3 standard */ background-color: #9fddea; z-index: 1; } .close_button { font-family: Verdana, Geneva, sans-serif; font-size: small; font-weight: bold; float: right; color: #666; display: block; text-decoration: none; border: 2px solid #666; padding: 0 3px 0 3px; } body { margin: 0; } .dbtnvs { cursor: pointer; background-color: #4CAF50; border: none; color: white; padding: 6px 7px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight: bold; width: 80px; } .actbtnvs { cursor: pointer; background-color: #f44336; border: none; color: white; padding: 6px 8px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight: bold; width:80px; } ol.vscheckist input { margin: 5px; } ol.vscheckist { width: 100%; float: left; list-style: none; } ol.vscheckist li { width: 32%; padding: 10px; float: left; } ol.vscheckist label { margin: 5px; } </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblUserId" runat="server" Text="" Visible="False"></asp:Label>
       <h2 class="page-title" style="margin-left:2.8%;">
                App User Management</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <div class="select-box" style="margin-left:27px">
                        <asp:DropDownList ID="ddUserType" runat="server" OnSelectedIndexChanged="ddUserType_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                            <asp:ListItem Value="Kochi 1 card user">Kochi 1 card user</asp:ListItem>
                            <asp:ListItem Value="App only User">App only User</asp:ListItem>
                        </asp:DropDownList>
                    </div>   <div class="search-select" style="width:30%;float:right;margin: 0;margin-right:4px;">
                    <div id="search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                            <form id="tfnewsearch" method="get" action="#">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="search" placeholder="Search by name of entry or mobile no"></asp:TextBox>
                            <asp:LinkButton ID="btnSearch" runat="server" CssClass="button" OnClick="btnSearch_Click"
                                CausesValidation="False"></asp:LinkButton>
                            </form>
                        </asp:Panel>
                    </div>
                 
                    <%--  <a class="large-btn" href="#">Add Data</a>--%>
                </div>
                <asp:GridView ID="gridData" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EmptyDataText="No users to display !!" PageSize="15" CssClass="explore-listing"
                    OnRowDataBound="gridData_RowDataBound" AllowSorting="True" OnRowCreated="gridData_RowCreated"
                    OnRowCommand="gridData_RowCommand" DataKeyNames="userId" OnPageIndexChanging="gridData_PageIndexChanging"
                    OnSorting="gridData_Sorting">
                    <Columns>
                        <asp:TemplateField HeaderText="S.No" ItemStyle-Height="20">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="userId" HeaderText="userId" ReadOnly="True" />
                        <asp:BoundField DataField="isActive" HeaderText="status" ReadOnly="True" />
                        <asp:TemplateField HeaderText="Name" SortExpression="pname">
                            <ItemTemplate>
                                <%#Eval("pname")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="mobileNo" HeaderText="Mobile No"   ItemStyle-Width="100px"/>
                        <asp:TemplateField HeaderText="Sign Up Date" SortExpression="registrationDate1">
                            <ItemTemplate>
                                <%#Eval("registrationDate1")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="emailID" HeaderText="Email Address"  ItemStyle-Width="210px"/>
                        <asp:BoundField DataField="uAddress" HeaderText="Address"  ItemStyle-Width="180px"/>
                        <asp:BoundField DataField="virtualCardNo" HeaderText="Card No." />
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <%--  <asp:LinkButton ID="btnViewDetails" runat="server" CausesValidation="false" CommandName="details"
                                    Text="View Details" CommandArgument='<%# Eval("userId") %>' />
                                &nbsp; &Iota; &nbsp;--%>
                                <asp:LinkButton ID="btnRemove" runat="server" CausesValidation="false" CommandName="remove"
                                    Text="Delete" CommandArgument='<%# Eval("userId") %>' OnClientClick="return confirm('Are you sure you want to delete this Record?');" />
                            </ItemTemplate> <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:Button ID="btnActive" runat="server" CausesValidation="false" CommandName="active"
                                    CssClass="actbtnvs" Text="Deactive" CommandArgument='<%# Eval("userId") %>' />
                                <asp:Button ID="btnDeactive" runat="server" CausesValidation="false" CommandName="deactive"
                                    CssClass="dbtnvs" Text="Active" CommandArgument='<%# Eval("userId") %>' OnClientClick="return confirm('Are you sure you want to deactivate this Record?');" />
                            </ItemTemplate>
            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <ul class="legend">
                     <li style="width:24% !important;text-align:justify;">
              
                        <p>
                          <strong>Legend:</strong>     <img src="/images/sort.png">Sort List Alphabetically</p>
                    </li>
                </ul>
            </div>
            <div id="popup1" class="popup">
                <asp:Panel ID="Panel4" runat="server">
                    <div class="content-box" style="min-height: 330px;">
                        <ul class="dashboard-form">
                            <li>
                                <div class="profpic">
                                    <img src="/images/dummy-article-img.jpg">
                                </div>
                                <h4 class="name">
                                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                </h4>
                                <%-- <a href="#" class="grid-btn">
                                <img src="/images/_grid.png"></a> --%></li>
                            <li>
                                <label>
                                    Email Address:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Date of Birth:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblDob" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Mobile Number:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                            <li>
                                <label>
                                    Sign Up Date:</label>
                                <p class="view-text">
                                    <asp:Label ID="lblSignUpDate" runat="server" Text=""></asp:Label>
                                </p>
                            </li>
                        </ul>
                    </div>
                </asp:Panel>
            </div>
            <div id="bg" class="popup_bg">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
