﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="KochiMetro.admin.UserDetails" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.7.1005, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/styles.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/css/style.css">
    <link href="/css/popup.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function okay() {
            window.parent.document.getElementById('btnOkay').click();
        }
        function cancel() {
            window.parent.document.getElementById('btnCancel').click();
        }

        function callfun() {
            parent.CallAlert();
            return false;
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                User Details
            </div>
            <div class="TitlebarRight" onclick="cancel();">
                <b><a href="#" onclick="cancel();">X</a></b>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container inner">
                    <div class="content-box" style="background: #d4d4d4; background: rgba(212, 212, 212, 0.35)">
                        <ul class="dashboard-form">
                            <li>
                                <div class="profpic">
                                    <img src="/images/dummy-article-img.jpg" alt="">
                                </div>
                                <h4 class="name">
                                    Johnathan Doe</h4>
                                <a href="#" class="grid-btn">
                                 <img src="/images/_grid.png"></a> </li>
                            <li>
                                <label>
                                    Email Address:</label>
                                <p class="view-text">
                                    johnathandoe@example.com</p>
                            </li>
                            <li>
                                <label>
                                    Date of Birth:</label>
                                <p class="view-text">
                                    20/03/1991</p>
                            </li>
                            <li>
                                <label>
                                    Mobile Number:</label>
                                <p class="view-text">
                                    9878986543</p>
                            </li>
                            <li>
                                <label>
                                    Sign Up Date:</label>
                                <p class="view-text">
                                    18/09/1996</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
