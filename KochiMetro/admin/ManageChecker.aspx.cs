﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro.admin
{
    public partial class ManageChecker : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    try
                    {
                        if (Session["status"].ToString() == "Success")
                        {
                            messages1.SetMessage(1, "Deatils updated successfully !!");
                            messages1.Visible = true;
                        }

                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        var dtType = _obInfo.GetPrivilegeTypes();
                        if (dtType == null) return;
                        ddType.DataSource = dtType;
                        ddType.DataBind();


                        DataTable dtGridData = _ob.GetAdminsByType("Checker");
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); ViewState["dtbl"] = dtGridData;

                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

        }

        protected void ddType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddType.SelectedItem.Text != "-Select-")
            {
                if (ddTeam.SelectedItem.Text == "--Select Team--")
                {
                    DataTable dtGridData = _ob.GetAdminsByTypePrivilege(ddType.SelectedValue, "Checker");
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
                else
                {
                    DataTable dtGridData = _ob.GetAdminsByTypePrivilegeTeam(ddType.SelectedValue, "Checker", ddTeam.SelectedValue);
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
            }
            else
            {
                if (ddTeam.SelectedItem.Text == "--Select Team--")
                {
                    DataTable dtGridData = _ob.GetAdminsByType("Checker");
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
                else
                {
                    DataTable dtGridData = _ob.GetAdminsByTypeTeam("Checker", ddTeam.SelectedValue);
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
            }
        }

        protected void ddType_DataBound(object sender, EventArgs e)
        {
            ddType.Items.Insert(0, new ListItem("-Select-", ""));
        }

        protected void ddType_DataBinding(object sender, EventArgs e)
        {
            ddType.Items.Clear();
        }

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridData.PageIndex = e.NewPageIndex;
            gridData.DataBind();
        }

        protected void gridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        protected void gridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "change")
                {
                    Session["id"] = e.CommandArgument.ToString();
                    Response.Redirect("/admin/AddAdmin.aspx");
                }
                if (e.CommandName == "remove")
                {
                    messages1.Visible = false;
                    string i = _ob.DeleteAdmin(e.CommandArgument.ToString());
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Admin deleted successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _ob.GetAdminsByType("Checker");
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
                if (e.CommandName == "active")
                {
                    messages1.Visible = false;
                    string i = _ob.ChangeAdminStatus(e.CommandArgument.ToString(), "0");
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Admin deactivated successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _ob.GetAdminsByType("Checker");
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
                if (e.CommandName == "deactive")
                {
                    messages1.Visible = false;
                    string i = _ob.ChangeAdminStatus(e.CommandArgument.ToString(), "1");
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Admin activated successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _ob.GetAdminsByType("Checker");
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
                if (e.CommandName == "view")
                {
                    Session["id"] = e.CommandArgument.ToString() + ";" + "view" + ";" + "Checker";
                    Response.Redirect("/admin/SetPrivilege.aspx");
                }
                if (e.CommandName == "pedit")
                {
                    Session["id"] = e.CommandArgument.ToString() + ";" + "edit" + ";" + "Checker";
                    Response.Redirect("/admin/SetPrivilege.aspx");
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["dtbl"] as DataTable;
            string sortingDirection;
            if (Direction == SortDirection.Ascending)
            {
                Direction = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                Direction = SortDirection.Ascending;
                sortingDirection = "Asc";
            }

            DataView sortedView = new DataView(dataTable);
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            gridData.DataSource = sortedView;
            gridData.DataBind();
        }

        public SortDirection Direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Text == "True")
                {
                    ((Button)e.Row.Cells[8].FindControl("btnActive")).Visible = true;
                    ((Button)e.Row.Cells[8].FindControl("btnDeactive")).Visible = false;
                }
                if (e.Row.Cells[2].Text == "False")
                {
                    ((Button)e.Row.Cells[8].FindControl("btnActive")).Visible = false;
                    ((Button)e.Row.Cells[8].FindControl("btnDeactive")).Visible = true;
                }
            }
        }

        protected void ddTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddTeam.SelectedItem.Text != "--Select Team--")
            {
                if (ddType.SelectedItem.Text == "-Select Privilege-")
                {
                    DataTable dtGridData = _ob.GetAdminsByTypeTeam("Checker", ddTeam.SelectedValue);
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
                else
                {
                    DataTable dtGridData = _ob.GetAdminsByTypePrivilegeTeam(ddType.SelectedValue, "Checker", ddTeam.SelectedValue);
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
            }
            else
            {
                if (ddType.SelectedItem.Text == "-Select Privilege-")
                {
                    DataTable dtGridData = _ob.GetAdminsByType("Checker");
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
                else
                {
                    DataTable dtGridData = _ob.GetAdminsByTypePrivilege(ddType.SelectedValue, "Checker");
                    gridData.DataSource = dtGridData;
                    gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                }
            }
        }
    }
}