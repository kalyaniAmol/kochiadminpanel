﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace KochiMetro.admin
{
    public partial class AddAdmin : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        // static variable
        static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    try
                    {
                        if (Session["id"].ToString().Length > 0)
                        {
                            prevPage = Request.UrlReferrer.ToString();
                            lbladminId.Text = Session["id"].ToString();
                            DataTable dt = _ob.GetAdminById(lbladminId.Text);
                            if (dt.Rows.Count > 0)
                            {
                                lblUser.Text = "Update User Details";
                                txtName.Text = dt.Rows[0]["name"].ToString();
                                txtemailId.Text = dt.Rows[0]["emailId"].ToString();
                                txtContactNo.Text = dt.Rows[0]["mobileNo"].ToString();
                                ddType.SelectedValue = dt.Rows[0]["adminType"].ToString();
                                ddTeam.SelectedValue = dt.Rows[0]["team"].ToString();

                                btnBack.Visible = true;
                                btnAdd.Text = "Update Details";
                            }
                            Session.Remove("id");
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string res;
                if (ddType.SelectedValue != "--Select--")
                {
                    if (ddTeam.SelectedValue != "--Select Team--")
                    {
                        if (btnAdd.Text == "Create User")
                        {
                            // to generate random alphanumeric linkID for sending in link to set password
                            Random random = new Random();
                            string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            for (int i = 0; i < 6; i++)
                            {
                                int abc = random.Next(0, 53);
                                sb.Append(array[abc]);
                            }

                            res = _ob.AddAdmin(txtName.Text, txtemailId.Text, txtContactNo.Text, _ob.base64Encode(sb.ToString()), ddType.SelectedValue, ddTeam.SelectedValue);
                            if (res.Contains("Success"))
                            {
                                sendMail(sb.ToString());
                                messages1.SetMessage(1, "Details saved successfully!! ");
                                messages1.Visible = true;
                                ClearTextBoxes();
                            }
                            else
                            {
                                messages1.SetMessage(0, res);
                                messages1.Visible = true;
                            }
                        }
                        if (btnAdd.Text == "Update Details")
                        {
                            Random random = new Random();
                            string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            for (int i = 0; i < 6; i++)
                            {
                                int abc = random.Next(0, 53);
                                sb.Append(array[abc]);
                            }

                            res = _ob.UpdateAdmin(txtName.Text, txtemailId.Text, txtContactNo.Text, _ob.base64Encode(sb.ToString()), ddType.SelectedValue, ddTeam.SelectedValue, lbladminId.Text);
                            if (res.Contains("Success"))
                            {
                                sendMail(sb.ToString());
                                messages1.Visible = true;
                                ClearTextBoxes();
                                Session["status"] = "Success";
                                Response.Redirect(prevPage);
                            }
                            else
                            {
                                messages1.SetMessage(0, res);
                                messages1.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        messages1.SetMessage(2, "Please select team !!");
                        messages1.Visible = true;
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please select admin type !!");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {

                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtContactNo.Text = "";
            txtemailId.Text = "";
            //ddType.DataBind(); 
            ddType.SelectedIndex = 0;
            //ddTeam.DataBind();
            ddTeam.SelectedIndex = 0;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        public void sendMail(string password)
        {
            try
            {
                string MailBody = "";
                MailBody = "Dear " + txtName.Text + ",<br><br>Your login details to KMRL : <br><br><table border='1' width='50%'><tr><td><b>Username</b></td><td>" + txtContactNo.Text + "</td></tr><tr><td><b>Password</b></td><td>" + password + "</td></tr></table><br><br>";

                string emailID = ConfigurationManager.AppSettings["EmailID"].ToString();
                string emailPass = ConfigurationManager.AppSettings["EmailPass"].ToString();
                string emailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
                string emailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();

                MailMessage ms = new MailMessage();
                ms.From = new MailAddress(emailID);
                ms.To.Add(txtemailId.Text);
                ms.Subject = "Registration Details at KMRL";

                ms.Body = MailBody;

                SmtpClient Sc = new SmtpClient(emailHost);
                ms.IsBodyHtml = true;
                Sc.Port = Convert.ToInt32(emailPort); ;
                Sc.UseDefaultCredentials = false;

                Sc.Credentials = new NetworkCredential(emailID, emailPass);

                Sc.EnableSsl = true;

                Sc.Send(ms);



            }
            catch (Exception)
            {
                //ignored
            }
        }

        public void sendUpdateMail(string password)
        {
            try
            {
                string MailBody = "";
                MailBody = "Dear " + txtName.Text + ",<br><br>Your login details to KMRL : <br><br><table border='1' width='50%'><tr><td><b>Username</b></td><td>" + txtContactNo.Text + "</td></tr><tr><td><b>Password</b></td><td>" + password + "</td></tr></table><br><br>";

                string emailID = ConfigurationManager.AppSettings["EmailID"].ToString();
                string emailPass = ConfigurationManager.AppSettings["EmailPass"].ToString();
                string emailHost = ConfigurationManager.AppSettings["EmailHost"].ToString();
                string emailPort = ConfigurationManager.AppSettings["EmailPort"].ToString();

                MailMessage ms = new MailMessage();
                ms.From = new MailAddress(emailID);
                ms.To.Add(txtemailId.Text);
                ms.Subject = "Details updated at KMRL";

                ms.Body = MailBody;

                SmtpClient Sc = new SmtpClient(emailHost);
                ms.IsBodyHtml = true;
                Sc.Port = Convert.ToInt32(emailPort); ;
                Sc.UseDefaultCredentials = false;

                Sc.Credentials = new NetworkCredential(emailID, emailPass);

                Sc.EnableSsl = true;

                Sc.Send(ms);



            }
            catch (Exception )
            {

            }
        }

    }
}