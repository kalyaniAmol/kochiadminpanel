﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace KochiMetro.admin
{
    public partial class Tourism : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(','); 
        static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            uploadCover.Attributes["onchange"] = "UploadFile(this)";

            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    prevPage = Request.UrlReferrer.ToString();

                    try
                    {
                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        Panel1.Visible = false;
                        if (dtFilter.Rows.Count > 0)
                        {
                            chkActivity.DataSource = dtFilter;
                            chkActivity.DataBind();
                        }
                        Session.Remove("exploreId");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    try
                    {
                        lblTourismId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetTourismById(lblTourismId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            txtName.Text = dt.Rows[0]["touristSpotName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            lblImage.Text = dt.Rows[0]["coverImage"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                            lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                            ddType.SelectedValue = dt.Rows[0]["distanceFrom"].ToString();
                            ImgCover.Src = "~\\images\\tourism\\" + lblImage.Text;
                            Panel1.Visible = true;
                            DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                            if (dtFilter.Rows.Count > 0)
                            {
                                chkActivity.DataSource = dtFilter;
                                chkActivity.DataBind();
                            }

                            string[] splittedActivityId = dt.Rows[0]["activitiesId"].ToString().Split(',');

                            for (int j = 0; j < chkActivity.Items.Count; j++)
                            {
                                for (int i = 0; i < splittedActivityId.Length; i++)
                                {
                                    if (splittedActivityId[i].ToString() == chkActivity.Items[j].Value)
                                    {
                                        chkActivity.Items[j].Selected = true;
                                    }
                                }
                            }

                            btnBack.Visible = true;
                            btnAdd.Text = "Update Details";
                            Session.Remove("id");
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddType.SelectedValue != "-Select-")
                {
                    string activities = "";
                    string activitiesId = "";
                    string res;

                    for (int i = 0; i < chkActivity.Items.Count; i++)
                    {
                        if (chkActivity.Items[i].Selected)
                        {

                            activities = activities + "," + chkActivity.Items[i].Text;
                            activitiesId = activitiesId + "," + chkActivity.Items[i].Value;
                        }
                    }
                    activities = activities.TrimStart(',');
                    activitiesId = activitiesId.TrimStart(',');

                    if (btnAdd.Text == "Save Details")
                    {
                        if (lblImage.Text.Length > 0)
                        {
                            res = _ob.AddTourism(lblExploreKochiId.Text, txtName.Text, txtAddress.Text, txtArea.Text, txtContactNo.Text, lblImage.Text, activitiesId, activities, ddType.SelectedValue, CKEditorDesc.Text, uDetails[0]);
                            if (res.Contains("Success"))
                            {
                                messages1.SetMessage(1, "Tourism details saved successfully!! ");
                                messages1.Visible = true;
                                ClearTextBoxes();
                            }
                        }
                        else
                        {
                            messages1.SetMessage(2, "Please select image  !!");
                        }
                    }
                    if (btnAdd.Text == "Update Details")
                    {
                        res = _ob.UpdateTourism(lblTourismId.Text, txtName.Text, txtAddress.Text, txtArea.Text, txtContactNo.Text, lblImage.Text, activitiesId, activities, ddType.SelectedValue, CKEditorDesc.Text, uDetails[0]);

                        if (res.Contains("Success"))
                        {
                            ClearTextBoxes();
                            Session["status"] = "Success";
                            Response.Redirect(prevPage);
                        }
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please Select distance !! ");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            ddType.DataBind();
            chkActivity.ClearSelection();
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Server.MapPath("~\\images\\tourism\\" + lblImage.Text);
                FileInfo file = new FileInfo(path);
                if (file.Exists)//check file exsit or not
                {
                    file.Delete();
                }
                lblImage.Text = "";
                Panel1.Visible = false;
            }
            catch (Exception )
            {
            }

        }

        protected void Upload(object sender, EventArgs e)
        {
            try
            {
                if (uploadCover.HasFile)
                {
                    string fname = "";
                    string ext = System.IO.Path.GetExtension(uploadCover.FileName);
                    fname = uploadCover.FileName;
                    string extToUpper = ext.ToUpper();
                    if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG" || extToUpper == ".GIF" || extToUpper == ".BMP")
                    {
                        try
                        {
                            System.Drawing.Image UploadedImage = System.Drawing.Image.FromStream(uploadCover.PostedFile.InputStream);
                            if (UploadedImage.PhysicalDimension.Width <= 1680 && UploadedImage.PhysicalDimension.Height <= 1050)
                            {
                                Random random = new Random();
                                string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                for (int ij = 0; ij < 6; ij++)
                                {
                                    int abc = random.Next(0, 53);
                                    sb.Append(array[abc]);
                                }
                                string[] splitname = fname.Split('.');
                                string tourismImage = "IMG" + splitname[0].ToString() + sb.ToString() + ext;
                                uploadCover.SaveAs(Server.MapPath("~\\images\\tourism\\" + tourismImage));
                                lblImage.Text = tourismImage;
                                ImgCover.Src = "~\\images\\tourism\\" + lblImage.Text;
                                Panel1.Visible = true;
                            }
                            else
                            {
                                messages1.SetMessage(2, "Image size should be 1680 By 1050 px");
                                messages1.Visible = true;
                            }

                        }
                        catch (Exception)
                        {
                            //ignored
                        }

                    }
                    else
                    {
                        string script = "<script type=\"text/javascript\">alert('Allowed file extensions are .PNG .BMP .JPG .JPEG .GIF')</script>";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                    }
                }
                else
                {
                    messages1.SetMessage(0, "Please upload image first !");
                    messages1.Visible = true;
                }
            }
            catch (Exception)
            {
                //ignored
            }
        }

        protected void btnAddActivity_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
        }

        protected void btnSaveActivity_Click(object sender, EventArgs e)
        {
            if (uploadActivityImage.HasFile)
            {
                string fname = "";
                string ext = System.IO.Path.GetExtension(uploadActivityImage.FileName);
                fname = uploadActivityImage.FileName;
                string extToUpper = ext.ToUpper();
                if (extToUpper == ".JPG" || extToUpper == ".JPEG" || extToUpper == ".PNG" || extToUpper == ".GIF" || extToUpper == ".BMP")
                {
                    try
                    {
                        System.Drawing.Image UploadedImage = System.Drawing.Image.FromStream(uploadActivityImage.PostedFile.InputStream);
                        if (UploadedImage.PhysicalDimension.Width <= 1680 && UploadedImage.PhysicalDimension.Height <= 1050)
                        {
                            Random random = new Random();
                            string[] array = new string[54] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            for (int ij = 0; ij < 6; ij++)
                            {
                                int abc = random.Next(0, 53);
                                sb.Append(array[abc]);
                            }
                            string[] splitname = fname.Split('.');
                            string tourismImage = "IMG" + splitname[0].ToString() + sb.ToString() + ext;
                            uploadActivityImage.SaveAs(Server.MapPath("~\\images\\filter\\" + tourismImage));

                            string res = _ob.AddFilterType(lblExploreKochiId.Text, txtActivity.Text, tourismImage);
                            if (res == "Success")
                            {
                                Panel2.Visible = true;
                                Panel3.Visible = false;
                                if (lblTourismId.Text.Length > 0)
                                {
                                }
                                else
                                {

                                }
                            }
                        }
                        else
                        {
                            messages1.SetMessage(2, "Image size should be 1680 By 1050 px");
                            messages1.Visible = true;
                        }

                    }
                    catch (Exception)                    {

                    }
                }
                else
                {
                    string script = "<script type=\"text/javascript\">alert('Allowed file extensions are .PNG .BMP .JPG .JPEG .GIF')</script>";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
                }
            }
            else
            {
                messages1.SetMessage(0, "Please upload image first !");
                messages1.Visible = true;
            }
        }

        protected void btnPanelBack_Click(object sender, EventArgs e)
        {

        }
    }
}