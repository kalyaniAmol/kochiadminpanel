﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro.admin
{
    public partial class OffersPromotion : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    try
                    {
                        if (Session["status"].ToString() == "Success")
                        {
                            messages1.SetMessage(1, "Deatils updated successfully !!");
                            messages1.Visible = true;
                        }

                    }
                    catch (Exception)
                    {

                    }

                    try
                    {
                        DataTable dtGridData = _obInfo.GetOffersPromotions();
                        gridData.DataSource = dtGridData;
                        gridData.DataBind(); ViewState["dtbl"] = dtGridData;

                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchOfferByName(txtSearch.Text);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
            else
            {
                DataTable dtGridData = _obInfo.GetOffersPromotions();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
        }

        //protected void ddType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddType.SelectedItem.Text != "-Select-")
        //    {
        //        DataTable dtGridData = _ob.GetOfferPromotionByType(ddType.SelectedValue);
        //        gridData.DataSource = dtGridData;
        //        gridData.DataBind(); ViewState["dtbl"] = dtGridData;
        //    }
        //    else
        //    {
        //        DataTable dtGridData = _obInfo.GetOffersPromotions();
        //        gridData.DataSource = dtGridData;
        //        gridData.DataBind(); ViewState["dtbl"] = dtGridData;
        //    }
        //}

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridData.PageIndex = e.NewPageIndex;
            gridData.DataBind();
            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchOfferByName(txtSearch.Text);
                gridData.DataSource = dtGridData;
                gridData.DataBind();
            }
            else
            {
                DataTable dtGridData = _obInfo.GetOffersPromotions();
                gridData.DataSource = dtGridData;
                gridData.DataBind();
            }
        }

        protected void gridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[2].Visible = false;
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
            }
        }

        protected void gridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //int row = 0;
                if (e.CommandName == "details")
                {
                    DataTable dt;
                    dt = _ob.GetOffersPromotionById(e.CommandArgument.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        imgOffer.Src = "" + dt.Rows[0]["bannerImage"].ToString();
                        lblName.Text = dt.Rows[0]["offerName"].ToString();
                        lblType.Text = dt.Rows[0]["type"].ToString();
                        lblDate.Text = dt.Rows[0]["insertDate1"].ToString();
                        lblDetails.Text = dt.Rows[0]["otherDescription"].ToString();

                        string script = "<script type=\"text/javascript\">openpopup('popup1')</script>";
                        ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
                    }

                }
                if (e.CommandName == "change")
                {
                    Session["id"] = e.CommandArgument;
                    Response.Redirect("/admin/AddOffersPromotion.aspx");
                }
                if (e.CommandName == "remove")
                {
                    messages1.Visible = false;
                    string i = _ob.DeleteOfferPromotion(e.CommandArgument.ToString());
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "Entry deleted successfully !");
                        messages1.Visible = true;
                        DataTable dtGridData = _obInfo.GetOffersPromotions();
                        gridData.DataSource = dtGridData;
                        gridData.DataBind();
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Text == "Pending")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\pending.png";
                }
                if (e.Row.Cells[3].Text == "Approve")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\approved.png";
                } 
                if (e.Row.Cells[3].Text == "Reject")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\rejected.png";
                } 
                if (e.Row.Cells[3].Text == "Edit and Approve")
                {
                    ((Image)e.Row.Cells[13].FindControl("Image1")).ImageUrl = "~\\images\\edit_approve.png";
                }

                if (e.Row.Cells[4].Text == "Pending")
                {
                    ((Image)e.Row.Cells[14].FindControl("Image2")).ImageUrl = "~\\images\\pending.png";
                }
                if (e.Row.Cells[4].Text == "Approve")
                {
                    ((Image)e.Row.Cells[14].FindControl("Image2")).ImageUrl = "~\\images\\approved.png";
                } 
                if (e.Row.Cells[4].Text == "Reject")
                {
                    ((Image)e.Row.Cells[14].FindControl("Image2")).ImageUrl = "~\\images\\rejected.png";
                } 
                if (e.Row.Cells[4].Text == "Edit and Approve")
                {
                    ((Image)e.Row.Cells[14].FindControl("Image2")).ImageUrl = "~\\images\\edit_approve.png";
                }
            }
        }

        protected void gridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["dtbl"] as DataTable;
            string sortingDirection;
            if (Direction == SortDirection.Ascending)
            {
                Direction = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                Direction = SortDirection.Ascending;
                sortingDirection = "Asc";
            }

            DataView sortedView = new DataView(dataTable);
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            gridData.DataSource = sortedView;
            gridData.DataBind();
        }

        public SortDirection Direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void btnApproveEntry_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gridData.Rows.Count; i++)
                {
                    CheckBox cb = (CheckBox)gridData.Rows[i].Cells[1].FindControl("chkchild");

                    if (cb != null)
                    {
                        if (cb.Checked)
                        {
                            string res = _ob.ApproveRejectOffer(gridData.Rows[i].Cells[2].Text, uDetails[0], uDetails[0], "Approve", "Approve");
                        }
                    }

                }
                messages1.SetMessage(1, "Entry approved Successfully !!");
                messages1.Visible = true;
                DataTable dtGridData = _obInfo.GetOffersPromotions();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); 

            }
            catch (Exception)
            {

            }
        }

        protected void btnDeleteEntry_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gridData.Rows.Count; i++)
                {
                    CheckBox cb = (CheckBox)gridData.Rows[i].Cells[1].FindControl("chkchild");

                    if (cb != null)
                    {
                        if (cb.Checked)
                        {
                            string res = _ob.DeleteOfferPromotion(gridData.Rows[i].Cells[2].Text);
                        }
                    }

                }
                messages1.SetMessage(1, "Deleted Successfully !!");
                messages1.Visible = true;
                DataTable dtGridData = _obInfo.GetOffersPromotions();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); 

            }
            catch (Exception)
            {

            }
        }

        protected void btnRejectEntry_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < gridData.Rows.Count; i++)
                {
                    CheckBox cb = (CheckBox)gridData.Rows[i].Cells[1].FindControl("chkchild");

                    if (cb != null)
                    {
                        if (cb.Checked)
                        {
                            string res = _ob.ApproveRejectOffer(gridData.Rows[i].Cells[2].Text, uDetails[0], uDetails[0], "Reject", "Reject");
                        }
                    }

                }
                messages1.SetMessage(1, "Entry rejected Successfully !!");
                messages1.Visible = true;
                DataTable dtGridData = _obInfo.GetOffersPromotions();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); 

            }
            catch (Exception)
            {

            }
        }
    }
}