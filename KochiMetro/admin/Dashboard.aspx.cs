﻿using System;
using System.Web;

namespace KochiMetro.admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {

                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }
        }
    }
}