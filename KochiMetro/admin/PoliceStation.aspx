﻿<%@ Page Title="Police Station" Language="C#" MasterPageFile="~/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="PoliceStation.aspx.cs" Inherits="KochiMetro.admin.PoliceStation" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.7.1005, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <h2 class="page-title">
                Police Station</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
                <asp:Label ID="lblExploreKochiId" runat="server" Text="" Visible="False"></asp:Label>
                <asp:Label ID="lblPoliceId" runat="server" Text="" Visible="False"></asp:Label>
            </div>
            <div class="content-box">
                <ul class="dashboard-form">
                    <li>
                        <label>
                            Name:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtName" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Address:</label>
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtAddress" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Area:</label>
                        <asp:TextBox ID="txtArea" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtArea" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Type:</label>
                        <asp:DropDownList ID="ddType" runat="server" OnDataBinding="ddType_DataBinding" DataTextField="filterType"
                            DataValueField="id" OnDataBound="ddType_DataBound">
                            <%-- <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                            <asp:ListItem Value="General">General</asp:ListItem>
                            <asp:ListItem Value="Women">Women</asp:ListItem>
                            <asp:ListItem Value="RPF">RPF</asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="ddType" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Phone No.:</label>
                        <asp:TextBox ID="txtContactNo" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtContactNo" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                            ValidChars="0123456789" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                    </li>
                    <li>
                        <label>
                            Other Description:</label>
                        <div style="float: left; width: 66%; height: 93%;">
                            <CKEditor:CKEditorControl ID="CKEditorDesc" BasePath="/ckeditor/" runat="server">
                            </CKEditor:CKEditorControl></div>
                    </li>
                    <li>
                        <div class="add-button">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                                Visible="False" />
                            <asp:Button ID="btnAdd" runat="server" Text="Save Details" CssClass="add-btn" OnClick="btnAdd_Click" /></div>
                    </li>
                </ul>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
