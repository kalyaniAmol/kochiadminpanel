﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;

namespace KochiMetro.admin
{
    public partial class FuelPoint : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        static string filename = "";
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    prevPage = Request.UrlReferrer.ToString();
                    try
                    {
                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        if (dtFilter.Rows.Count > 0)
                        {
                            ddType.DataSource = dtFilter;
                            ddType.DataBind();
                        }
                        Session.Remove("exploreId");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    try
                    {
                        lblFuelPointId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetFuelPointById(lblFuelPointId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                            DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                            if (dtFilter.Rows.Count > 0)
                            {
                                ddType.DataSource = dtFilter;
                                ddType.DataBind();
                            }
                            txtName.Text = dt.Rows[0]["fuelPointName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            ddType.SelectedValue = dt.Rows[0]["fuelComapny"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();

                            btnBack.Visible = true;
                            btnAdd.Text = "Update Details";
                        }
                        Session.Remove("id");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddType.SelectedValue != "--Select--")
                {
                    string res;
                    if (btnAdd.Text == "Save Details")
                    {
                        if (uploadImg.HasFile)
                        {
                            string ranId = _ob.getRandomID();
                            filename = uploadImg.FileName.Replace(' ', '_');
                            string Ext = System.IO.Path.GetExtension(this.uploadImg.PostedFile.FileName);
                            if (Ext.ToLower() == ".bmp" || Ext.ToLower() == ".gif" || Ext.ToLower() == ".jpg" || Ext.ToLower() == ".jpeg" || Ext.ToLower() == ".png")
                            {
                                string[] splitname = filename.Split('.');
                                filename = splitname[0].ToString() + "_" + ranId + Ext;
                                uploadImg.SaveAs(Server.MapPath("~\\images\\fuel\\" + filename));

                            }
                            res = _ob.AddFuelPoint(lblExploreKochiId.Text, txtName.Text, txtAddress.Text, filename, txtArea.Text,
                                ddType.SelectedValue, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                            if (res.Contains("Success"))
                            {
                                messages1.SetMessage(1, "Fuel point saved successfully!! ");
                                messages1.Visible = true;
                                ClearTextBoxes();
                            }
                        }
                    }
                    if (btnAdd.Text == "Update Details")
                    {
                        res = _ob.UpdateFuelPoint(lblFuelPointId.Text, txtName.Text, txtAddress.Text, txtArea.Text,
                            ddType.SelectedValue, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                        if (res.Contains("Success"))
                        {
                            ClearTextBoxes();
                            Session["status"] = "Success";
                            Response.Redirect(prevPage);
                        }
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please Select fue company first !! ");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void ddType_DataBinding(object sender, EventArgs e)
        {
            ddType.Items.Clear();
        }

        protected void ddType_DataBound(object sender, EventArgs e)
        {
            ddType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Select--", ""));
        }
    }
}