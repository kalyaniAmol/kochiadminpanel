﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro.admin
{
    public partial class UserMgmt : Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    try
                    {
                        DataTable dtGridData = _ob.GetAllUsers();
                        gridData.DataSource = dtGridData;
                        gridData.DataBind();
                        ViewState["dtbl"] = dtGridData;
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchUserByNameMobile(txtSearch.Text);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
            else
            {
                DataTable dtGridData = _ob.GetAllUsers();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
        }

        protected void ddUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddUserType.SelectedItem.Text != "-Select-")
            {
                DataTable dtGridData = _ob.GetAllUserByType(ddUserType.SelectedValue);
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
            else
            {
                DataTable dtGridData = _ob.GetAllUsers();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
        }



        protected void gridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
                e.Row.Cells[2].Visible = false;
            }
        }

        protected void gridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //int row = 0;
                if (e.CommandName == "details")
                {
                    DataTable dt = _ob.GetUserById(e.CommandArgument.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        lblName.Text = dt.Rows[0]["pname"].ToString();
                        lblEmail.Text = dt.Rows[0]["emailID"].ToString();
                        lblContact.Text = dt.Rows[0]["mobileNo"].ToString();
                        lblDob.Text = dt.Rows[0]["udob1"].ToString();
                        lblSignUpDate.Text = dt.Rows[0]["registrationDate1"].ToString();

                        string script = "<script type=\"text/javascript\">openpopup('popup1')</script>";
                        ScriptManager.RegisterStartupScript(Page, GetType(), "key", script, false);
                    }
                }
                if (e.CommandName == "remove")
                {
                    messages1.Visible = false;
                    string i = _ob.DeleteUser(e.CommandArgument.ToString());
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "User deleted successfully !");
                        messages1.Visible = true;
                        if (ddUserType.SelectedItem.Text != "-Select-")
                        {
                            DataTable dtGridData = _ob.GetAllUserByType(ddUserType.SelectedValue);
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                        else
                        {
                            DataTable dtGridData = _ob.GetAllUsers();
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
                if (e.CommandName == "active")
                {
                    messages1.Visible = false;
                    string i = _ob.ChangeUserStatus(e.CommandArgument.ToString(), "0");
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "User deactivated successfully !");
                        messages1.Visible = true;
                        if (ddUserType.SelectedItem.Text != "-Select-")
                        {
                            DataTable dtGridData = _ob.GetAllUserByType(ddUserType.SelectedValue);
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                        else
                        {
                            DataTable dtGridData = _ob.GetAllUsers();
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
                if (e.CommandName == "deactive")
                {
                    messages1.Visible = false;
                    string i = _ob.ChangeUserStatus(e.CommandArgument.ToString(), "1");
                    if (i == "Success")
                    {
                        messages1.SetMessage(1, "User activated successfully !");
                        messages1.Visible = true;
                        if (ddUserType.SelectedItem.Text != "-Select-")
                        {
                            DataTable dtGridData = _ob.GetAllUserByType(ddUserType.SelectedValue);
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                        else
                        {
                            DataTable dtGridData = _ob.GetAllUsers();
                            gridData.DataSource = dtGridData;
                            gridData.DataBind(); ViewState["dtbl"] = dtGridData;
                        }
                    }
                    else
                    {
                        messages1.SetMessage(0, i);
                        messages1.Visible = true;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridData.PageIndex = e.NewPageIndex;
            if (txtSearch.Text.Length > 0)
            {
                DataTable dtGridData = _ob.SearchUserByNameMobile(txtSearch.Text);
                gridData.DataSource = dtGridData;
                gridData.DataBind();
            }
            else
            {
                DataTable dtGridData = _ob.GetAllUsers();
                gridData.DataSource = dtGridData;
                gridData.DataBind(); 
            }
        }

        protected void gridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dataTable = ViewState["dtbl"] as DataTable;
            string sortingDirection;
            if (Direction == SortDirection.Ascending)
            {
                Direction = SortDirection.Descending;
                sortingDirection = "Desc";
            }
            else
            {
                Direction = SortDirection.Ascending;
                sortingDirection = "Asc";
            }

            DataView sortedView = new DataView(dataTable);
            sortedView.Sort = e.SortExpression + " " + sortingDirection;
            gridData.DataSource = sortedView;
            gridData.DataBind();

        }

        public SortDirection Direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }


        protected void gridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Text == "True")
                {
                    ((Button)e.Row.Cells[10].FindControl("btnActive")).Visible = true;
                    ((Button)e.Row.Cells[10].FindControl("btnDeactive")).Visible = false;
                }
                if (e.Row.Cells[2].Text == "False")
                {
                    ((Button)e.Row.Cells[10].FindControl("btnActive")).Visible = false;
                    ((Button)e.Row.Cells[10].FindControl("btnDeactive")).Visible = true;
                }
            }
        }

    }
}