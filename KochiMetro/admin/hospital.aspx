﻿<%@ Page Title="Hospital" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="hospital.aspx.cs" Inherits="KochiMetro.admin.Hospital" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.7.1005, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        function Validate_Checkbox() {
            var chks = $("#<%= chkSpeciality.ClientID %> input:checkbox");

            var hasChecked = false;
            for (var i = 0; i < chks.length; i++) {
                if (chks[i].checked) {
                    hasChecked = true;
                    break;
                }
            }
            if (hasChecked == false) {
                alert("Please select at least one checkbox..!");

                return false;
            }

        }     
    </script>
    <style>
        table#ContentPlaceHolder1_chkSpeciality
        {
            width:80%;margin-bottom: 10px;
        }
        table#ContentPlaceHolder1_chkSpeciality label
        {
            width: 80%;
            font-size: 12px;
            float: right;
            margin: 0;
            padding: 0;
        }input#ContentPlaceHolder1_txtContactNo {
    width: 105px;
}
        table#ContentPlaceHolder1_chkSpeciality td
        {
            border: 0;
        }
        table#ContentPlaceHolder1_chkSpeciality input[type="checkbox"]
        {
            width: 20px;
            float: left;
            margin: 0;
        }.uvsnvs{width:250px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblExploreKochiId" runat="server" Text="" Visible="False"></asp:Label>
            <asp:Label ID="lblHospitalId" runat="server" Text="" Visible="False"></asp:Label>
            <h2 class="page-title">
                Hospital</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <ul class="dashboard-form">
                    <li>
                        <label>
                            Name:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtName" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Address:</label>
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtAddress" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Area:</label>
                        <asp:TextBox ID="txtArea" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtArea" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Speciality:</label>
                        <asp:CheckBoxList ID="chkSpeciality" runat="server" DataTextField="filterType" DataValueField="id"
                            RepeatColumns="4" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                        <%--   <asp:TextBox ID="txtSpeciality" runat="server" CssClass="text"></asp:TextBox>--%>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="RequiredFieldValidator"
                    ControlToValidate="chkSpeciality" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                    </li>
                    <li>
                        <label>
                            Phone No.:</label>
                        <asp:TextBox ID="txtContactNo" runat="server" CssClass="text uvsnvs"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtContactNo" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                            ValidChars="0123456789" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                    </li>
                    <li>
                        <label>
                            Other Description:</label>
                        <div style="float: left; width: 66%; height: 93%;">
                            <CKEditor:CKEditorControl ID="CKEditorDesc" BasePath="/ckeditor/" runat="server">
                            </CKEditor:CKEditorControl></div>
                    </li>
                    <li>
                        <div class="add-button">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                                Visible="False" />
                            <asp:Button ID="btnAdd" runat="server" Text="Save Details" CssClass="add-btn" OnClick="btnAdd_Click"
                                OnClientClick="javascript:Validate_Checkbox();" />
                        </div>
                    </li>
                </ul>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
