﻿<%@ Page Title="Add Offers & Pomotions" Language="C#" MasterPageFile="~/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="AddOffersPromotion.aspx.cs" Inherits="KochiMetro.admin.AddOffersPromotion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding: 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
        }
        
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 40px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        
        #search a.button:hover
        {
            background-color: #000;
        }
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding: 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
            margin-left: 10px;
        } 
    </style>
    <script type="text/javascript">
        function UploadFile(fileUpload) {
            if (fileUpload.value != '') {
                //alert('hi');
                document.getElementById("<%=Button1.ClientID %>").click();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblImage" runat="server" Text="" Visible="False"></asp:Label>
            <asp:Label ID="lblID" runat="server" Text="" Visible="False"></asp:Label>
            <h2 class="page-title">
                Explore Kochi</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <ul class="dashboard-form">
                    <li>
                        <label>
                            Title:</label>
                        <asp:TextBox ID="txtName" runat="server" CssClass="text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtName" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Upload Image:</label>
                        <asp:FileUpload ID="uploadCover" runat="server" />
                        <asp:Button ID="Button1" Text="Upload" runat="server" OnClick="Upload" Style="display: none"
                            CausesValidation="false" />
                        <%-- <asp:LinkButton ID="btnUpload" runat="server" CausesValidation="false" OnClick="btnUpload_Click">Upload</asp:LinkButton>--%>
                        <p class="upload-txt">
                            Image should be 1680x1050 in size (.jpeg, .png format only)</p>
                        <p class="upload-txt">
                            Image should be 200x400 in size (.jpeg, .png format only)</p>
                    </li>
                    <li>
                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                            <label>
                                &nbsp;</label><div class="vsboxim">
                                    <div class="vsboximg">
                                        <img id="ImgCover" runat="server" class="vsboximg1" width="100" height="100"/>
                                        <asp:LinkButton ID="btnDelete" OnClick="btnDelete_Click" runat="server" CausesValidation="false"
                                            CssClass="vsboximga">Delete</asp:LinkButton>
                                    </div>
                                </div>
                        </asp:Panel>
                    </li>
                    <li>
                        <label>
                            Address:</label>
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="text"></asp:TextBox>
                    </li>
                    <li>
                        <label>
                            Valid Till:</label>
                        <asp:TextBox ID="txtValidity" runat="server" CssClass="text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtValidity"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtValidity" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                    </li>
                    <li>
                        <label>
                            Other Description:</label>
                        <div style="float: left; width: 66%; height: 93%;">
                            <CKEditor:CKEditorControl ID="CKEditorDesc" BasePath="/ckeditor/" runat="server">
                            </CKEditor:CKEditorControl></div>
                    </li>
                    <li>
                        <div class="add-button">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="add-btn back" OnClick="btnBack_Click"
                                Visible="False" />
                            <asp:Button ID="btnAdd" runat="server" Text="Save Details" CssClass="add-btn" OnClick="btnAdd_Click" />
                        </div>
                    </li>
                </ul>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
