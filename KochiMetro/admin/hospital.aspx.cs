﻿using System;
using System.Data;
using System.Web;
using KochiMetro.classes;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace KochiMetro.admin
{
    public partial class Hospital : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        static string prevPage = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                {
                    try
                    {
                        prevPage = Request.UrlReferrer.ToString();

                        lblExploreKochiId.Text = Session["exploreId"].ToString();
                        DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                        if (dtFilter.Rows.Count > 0)
                        {
                            chkSpeciality.DataSource = dtFilter;
                            chkSpeciality.DataBind();
                        }
                        Session.Remove("exploreId");
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    try
                    {
                        lblHospitalId.Text = Session["id"].ToString();
                        DataTable dt = _ob.GetHospitalById(lblHospitalId.Text);
                        if (dt.Rows.Count > 0)
                        {
                            lblExploreKochiId.Text = dt.Rows[0]["exploreCatId"].ToString();
                            DataTable dtFilter = _ob.GetFilterTypeById(lblExploreKochiId.Text);
                            if (dtFilter.Rows.Count > 0)
                            {
                                chkSpeciality.DataSource = dtFilter;
                                chkSpeciality.DataBind();
                            }
                            txtName.Text = dt.Rows[0]["hospitalName"].ToString();
                            txtAddress.Text = dt.Rows[0]["address"].ToString();
                            txtArea.Text = dt.Rows[0]["area"].ToString();
                            txtContactNo.Text = dt.Rows[0]["contactNo"].ToString();
                            CKEditorDesc.Text = dt.Rows[0]["otherDescription"].ToString();
                            string[] splittedSpecialityId = dt.Rows[0]["specialityId"].ToString().Split(',');

                            for (int j = 0; j < chkSpeciality.Items.Count; j++)
                            {
                                for (int i = 0; i < splittedSpecialityId.Length; i++)
                                {
                                    if (splittedSpecialityId[i].ToString() == chkSpeciality.Items[j].Value)
                                    {
                                        chkSpeciality.Items[j].Selected = true;
                                    }
                                }
                            }

                            btnBack.Visible = true;
                            btnAdd.Text = "Update Details";
                            Session.Remove("id");
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }

            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string speciality = "";
                string specialityId = "";
                string res;

                for (int i = 0; i < chkSpeciality.Items.Count; i++)
                {
                    if (chkSpeciality.Items[i].Selected)
                    {

                        speciality = speciality + "," + chkSpeciality.Items[i].Text;
                        specialityId = specialityId + "," + chkSpeciality.Items[i].Value;
                    }
                }
                speciality = speciality.TrimStart(',');
                specialityId = specialityId.TrimStart(',');

                if (btnAdd.Text == "Save Details")
                {
                    res = _ob.AddHospital(lblExploreKochiId.Text, txtName.Text, txtAddress.Text, txtArea.Text, specialityId,
                        speciality, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        messages1.SetMessage(1, "Hospital details saved successfully!! ");
                        messages1.Visible = true;
                        ClearTextBoxes();
                    }
                }
                if (btnAdd.Text == "Update Details")
                {
                    res = _ob.UpdateHospital(lblHospitalId.Text, txtName.Text, txtAddress.Text, txtArea.Text, specialityId,
                        speciality, txtContactNo.Text, CKEditorDesc.Text, uDetails[0]);
                    if (res.Contains("Success"))
                    {
                        //messages1.SetMessage(1, "Hospital Details updated successfully!! ");
                        //messages1.Visible = true;
                        ClearTextBoxes();
                        Session["status"] = "Success";
                        Response.Redirect(prevPage);
                    }
                }
            }
            catch (Exception ee)
            {
                messages1.SetMessage(0, ee.Message);
                messages1.Visible = true;
            }
        }

        private void ClearTextBoxes()
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtArea.Text = "";
            chkSpeciality.ClearSelection();
            txtContactNo.Text = "";
            CKEditorDesc.Text = "";
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}