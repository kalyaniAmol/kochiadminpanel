﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Linq;

namespace KochiMetro.admin
{
    public partial class BulkUpload : System.Web.UI.Page
    {
        private readonly SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["kochiMetroConnectionString"].ConnectionString);

        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                try
                {
                    Panel2.Visible = false;
                    Panel3.Visible = false;
                    var dtCat = _obInfo.GetExploreKochiCategories();
                    if (dtCat == null) throw new ArgumentNullException();
                    ddCategory.DataSource = dtCat;
                    ddCategory.DataBind();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        protected void btnUploadData_Click(object sender, EventArgs e)
        {
            if (ddCategory.SelectedValue != "")
            {
                messages1.Visible = false;
                Panel2.Visible = true;
                Panel1.Visible = false;
                lblBatchId.Text = Convert.ToString(_ob.GetBatch(ddCategory.SelectedValue));
            }
            else
            {
                messages1.SetMessage(2, "Please select category to continue !!");
                messages1.Visible = true;
            }
        }

        protected void ddCategory_DataBound(object sender, EventArgs e)
        {
            ddCategory.Items.Insert(0, new ListItem("-Select Category-", ""));
        }

        protected void ddCategory_DataBinding(object sender, EventArgs e)
        {
            ddCategory.Items.Clear();
        }

        protected void ddCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddCategory.SelectedItem.Text == "")
            {
                messages1.SetMessage(2, "Please select category to continue !!");
                messages1.Visible = true;
            }
            else
            {
                lblExploreKochiId.Text = ddCategory.SelectedValue;
            }
        }

        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridData.PageIndex = e.NewPageIndex;
            gridData.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                messages1.Visible = false;
                if (ddCategory.SelectedItem.Text != "")
                {
                    string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
                    if (uploadExcel.HasFile)
                    {
                        string ext = System.IO.Path.GetExtension(this.uploadExcel.PostedFile.FileName);
                        if (ext.ToUpper() == ".XLS" || ext.ToUpper() == ".XLSX")
                        {
                            string curdate = DateTime.Now.ToString("ddMMyyyy_HHmmss");
                            string path = uploadExcel.PostedFile.FileName;
                            string fileName = "excel" + curdate + ext;
                            uploadExcel.SaveAs(Server.MapPath("~\\UploadedDocs\\" + fileName));
                            string xConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Server.MapPath("~\\UploadedDocs\\" + fileName) + ";" + "Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";
                            OleDbConnection oledbcon = new OleDbConnection(xConnStr);
                            try
                            {
                                oledbcon.Open();
                                OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", oledbcon);
                                OleDbDataAdapter adptr = new OleDbDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                adptr.Fill(dt);

                                oledbcon.Close();
                                System.IO.File.Delete(Server.MapPath("~\\UploadedDocs\\excel" + curdate + ext));

                                if (ddCategory.SelectedValue == "1")
                                {
                                    insertHospitals(dt);
                                }
                                if (ddCategory.SelectedValue == "2")
                                {
                                    insertGovtOffices(dt);
                                }
                                if (ddCategory.SelectedValue == "3")
                                {
                                    insertEmissionTesting(dt);
                                }
                                if (ddCategory.SelectedValue == "4")
                                {
                                    insertParkings(dt);
                                }
                                if (ddCategory.SelectedValue == "5")
                                {
                                    insertPoliceStations(dt);
                                }
                                if (ddCategory.SelectedValue == "6")
                                {
                                    insertTourism(dt);
                                }
                                if (ddCategory.SelectedValue == "7")
                                {
                                    insertCinemas(dt);
                                }
                                if (ddCategory.SelectedValue == "8")
                                {
                                    insertRestaurant(dt);
                                }
                                if (ddCategory.SelectedValue == "9")
                                {
                                    insertFuelStations(dt);
                                }


                            }
                            catch (Exception ee)
                            {
                                messages1.Visible = true;
                                messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                            }
                        }
                        else
                        {
                            messages1.Visible = true;
                            messages1.SetMessage(0, "Select proper Excel sheet !");
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Select proper Excel sheet !");
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please select category to continue !!");
                    messages1.Visible = true;
                }
            }
            catch (Exception ee)
            {
                messages1.Visible = true;
                messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel1.Visible = true;
        }

        public void Openconn()
        {
            try { conn.Open(); }
            catch { conn.Close(); conn.Open(); }
        }

        protected void btnDownloadBulkData_Click(object sender, EventArgs e)
        {
            if (ddCategory.SelectedValue != "")
            {
                Panel3.Visible = true;
                Panel1.Visible = false;
            }
            else
            {
                messages1.SetMessage(2, "Please select category to continue !!");
                messages1.Visible = true;
            }
        }

        protected void btnGetBulk_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            messages1.Visible = false;
            if (txtFrom.Text.Length > 0 && txtTo.Text.Length > 0)
            {
                DateTime fromDate = Convert.ToDateTime(txtFrom.Text, CultureInfo.GetCultureInfo("hi-IN"));
                DateTime toDate = Convert.ToDateTime(txtTo.Text, CultureInfo.GetCultureInfo("hi-IN"));
                if (fromDate > toDate)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(2, "Please Enter Proper Dates !");
                }
                else
                {
                    dt = _ob.GetAllEntriesByIdBetDatesForExport(ddCategory.SelectedValue, fromDate, toDate);
                    GridView2.DataSource = dt;
                    GridView2.DataBind();
                    ExporttoExcel(dt);
                }
            }
            else
            {
                dt = _ob.GetAllEntriesByIdForExport(ddCategory.SelectedValue);
                GridView2.DataSource = dt;
                GridView2.DataBind();
                ExporttoExcel(dt);
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected void btnBackBulk_Click(object sender, EventArgs e)
        {
            Panel3.Visible = false;
            Panel1.Visible = true;
        }

        protected void btnDownloadSample_Click(object sender, EventArgs e)
        {
            messages1.Visible = false;
            if (ddCategory.SelectedValue != "")
            {
                try
                {
                    string PathToExcelFile = "~\\sampledocuments\\" + ddCategory.SelectedItem.Text + ".xlsx";
                    Response.Redirect("/download.aspx?file=sampledocuments//" + ddCategory.SelectedItem.Text + ".xlsx");
                }
                catch (Exception)
                {
                    messages1.SetMessage(2, "File not found. Please contact admin");
                    messages1.Visible = true;
                }
            }
            else
            {
                messages1.SetMessage(2, "Please select category to continue !!");
                messages1.Visible = true;
            }
        }


        private void ExporttoExcel(DataTable table)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public void insertHospitals(DataTable dt)
        {
            //string returnResult = "";
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string hospitalName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string specialityId = dt.Columns[3].ColumnName.ToLower();
                string speciality = dt.Columns[4].ColumnName.ToLower();
                string contactNo = dt.Columns[5].ColumnName.ToLower();
                string otherDescription = dt.Columns[6].ColumnName.ToLower();
                string longitude = dt.Columns[7].ColumnName.ToLower();
                string latitude = dt.Columns[8].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_hospital where ";
                    if (hospitalName.ToLower().Trim() == "hospitalname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && specialityId.ToLower().Trim() == "specialityid" && speciality.ToLower().Trim() == "speciality" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string hospitalNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "hospitalName" + k.ToString();
                                hospitalNames = "hospitalName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["hospitalName"].ToString());

                            }
                            else
                            {
                                para = "hospitalName" + k.ToString();
                                hospitalNames = hospitalNames + " or hospitalName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["hospitalName"].ToString());
                            }
                        }
                        query = query + hospitalNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();
                        // dtAlreadyExists = ob.checkDuplicateEmailAndContactNo(emailIDs, contactnos);

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("hospitalName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("speciality", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["hospitalName"].ToString() == dtAlreadyExists.Rows[j]["hospitalName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["hospitalName"] = dt.Rows[i]["hospitalName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["Speciality"] = dt.Rows[i]["speciality"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add Hospitals. Because following names already registered.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("hospitalName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("specialityId", typeof(string));
                            dtToAdd.Columns.Add("speciality", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["hospitalName"] = dt.Rows[i]["hospitalName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["specialityId"] = dt.Rows[i]["specialityId"].ToString();
                                dtRow["speciality"] = dt.Rows[i]["speciality"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("hospitals");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkHospital(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Hospital details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Hospital not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Hospitals not added !!");
                            }
                        }



                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertGovtOffices(DataTable dt)
        {
            //string returnResult = "";
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string gcatId = dt.Columns[0].ColumnName.ToLower();
                string gcatName = dt.Columns[1].ColumnName.ToLower();
                string address = dt.Columns[2].ColumnName.ToLower();
                string area = dt.Columns[3].ColumnName.ToLower();
                string serviceId = dt.Columns[4].ColumnName.ToLower();
                string services = dt.Columns[5].ColumnName.ToLower();
                string contactNo = dt.Columns[6].ColumnName.ToLower();
                string otherDescription = dt.Columns[7].ColumnName.ToLower();
                string longitude = dt.Columns[8].ColumnName.ToLower();
                string latitude = dt.Columns[9].ColumnName.ToLower();
                try
                {
                    if (gcatId.ToLower().Trim() == "gcatid" && gcatName.ToLower().Trim() == "gcatname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && serviceId.ToLower().Trim() == "serviceid" && services.ToLower().Trim() == "services" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {

                        DataTable dtToAdd = new DataTable();
                        dtToAdd.Columns.Add("exploreCatId", typeof(string));
                        dtToAdd.Columns.Add("gcatId", typeof(string));
                        dtToAdd.Columns.Add("gcatName", typeof(string));
                        dtToAdd.Columns.Add("address", typeof(string));
                        dtToAdd.Columns.Add("area", typeof(string));
                        dtToAdd.Columns.Add("serviceId", typeof(string));
                        dtToAdd.Columns.Add("services", typeof(string));
                        dtToAdd.Columns.Add("contactNo", typeof(string));
                        dtToAdd.Columns.Add("otherDescription", typeof(string));
                        dtToAdd.Columns.Add("insertedBy", typeof(string));
                        dtToAdd.Columns.Add("longitude", typeof(string));
                        dtToAdd.Columns.Add("latitude", typeof(string));
                        dtToAdd.Columns.Add("batch", typeof(string));
                        dtToAdd.Columns.Add("status", typeof(string));

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DataRow dtRow = dtToAdd.NewRow();
                            dtRow["exploreCatId"] = lblExploreKochiId.Text;
                            dtRow["gcatId"] = dt.Rows[i]["gcatId"].ToString();
                            dtRow["gcatName"] = dt.Rows[i]["gcatName"].ToString();
                            dtRow["address"] = dt.Rows[i]["address"].ToString();
                            dtRow["area"] = dt.Rows[i]["area"].ToString();
                            dtRow["serviceId"] = dt.Rows[i]["serviceId"].ToString();
                            dtRow["services"] = dt.Rows[i]["services"].ToString();
                            dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                            dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                            dtRow["insertedBy"] = uDetails[0].ToString();
                            dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                            dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                            dtRow["batch"] = lblBatchId.Text;
                            dtRow["status"] = "Pending";

                            dtToAdd.Rows.Add(dtRow);
                        }

                        DataSet ds = new DataSet("govtOffices");
                        int xyz = dtToAdd.Rows.Count;
                        ds.Tables.Add(dtToAdd);
                        string strXml = ds.GetXml();
                        int insRows = _ob.inserBulkGovtOffices(strXml);
                        if (insRows > 0)
                        {
                            messages1.Visible = true;
                            if (insRows == xyz)
                            {
                                messages1.SetMessage(1, "Government offices details added successfully !!");
                                gridData.Visible = true;
                                DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                gridData.DataSource = dtFinal;
                                gridData.DataBind();
                                GridView1.Visible = false;
                            }
                            else
                            {
                                messages1.SetMessage(1, "Govt offices not added !!");
                            }
                        }
                        else
                        {
                            messages1.Visible = true; messages1.SetMessage(1, "Govt offices not added !!");
                        }
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertEmissionTesting(DataTable dt)
        {
            //string returnResult = "";
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string emissionName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string contactNo = dt.Columns[3].ColumnName.ToLower();
                string otherDescription = dt.Columns[4].ColumnName.ToLower();
                string longitude = dt.Columns[5].ColumnName.ToLower();
                string latitude = dt.Columns[6].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_EmissionTesting where ";
                    if (emissionName.ToLower().Trim() == "emissionname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string emissionNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "emissionName" + k.ToString();
                                emissionNames = "emissionName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["emissionName"].ToString());

                            }
                            else
                            {
                                para = "emissionName" + k.ToString();
                                emissionNames = emissionNames + " or emissionName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["emissionName"].ToString());
                            }
                        }
                        query = query + emissionNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();
                        // dtAlreadyExists = ob.checkDuplicateEmailAndContactNo(emailIDs, contactnos);

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("emissionName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["emissionName"].ToString() == dtAlreadyExists.Rows[j]["emissionName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["emissionName"] = dt.Rows[i]["emissionName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add emission testing centres. Because following names already registered.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("emissionName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["emissionName"] = dt.Rows[i]["emissionName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("emission");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkEmissionTest(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Emission details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Emission not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Emission not added !!");
                            }
                        }



                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertParkings(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string parkingName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string pTypeId = dt.Columns[3].ColumnName.ToLower();
                string pType = dt.Columns[4].ColumnName.ToLower();
                string contactNo = dt.Columns[5].ColumnName.ToLower();
                string otherDescription = dt.Columns[6].ColumnName.ToLower();
                string longitude = dt.Columns[7].ColumnName.ToLower();
                string latitude = dt.Columns[8].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_Parking where ";
                    if (parkingName.ToLower().Trim() == "parkingname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && pTypeId.ToLower().Trim() == "ptypeid" && pType.ToLower().Trim() == "ptype" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string parkingNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "parkingName" + k.ToString();
                                parkingNames = "parkingName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["parkingName"].ToString());

                            }
                            else
                            {
                                para = "parkingName" + k.ToString();
                                parkingNames = parkingNames + " or parkingName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["parkingName"].ToString());
                            }
                        }
                        query = query + parkingNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();
                        // dtAlreadyExists = ob.checkDuplicateEmailAndContactNo(emailIDs, contactnos);

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("parkingName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("pType", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["parkingName"].ToString() == dtAlreadyExists.Rows[j]["parkingName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["parkingName"] = dt.Rows[i]["parkingName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["pType"] = dt.Rows[i]["pType"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add parking areas. Because following areas already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("parkingName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("pTypeId", typeof(string));
                            dtToAdd.Columns.Add("pType", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["parkingName"] = dt.Rows[i]["parkingName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["pTypeId"] = dt.Rows[i]["pTypeId"].ToString();
                                dtRow["pType"] = dt.Rows[i]["pType"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("parking");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkParking(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Parking details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Parking details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Parking details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertPoliceStations(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string policeStationName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string policeType = dt.Columns[3].ColumnName.ToLower();
                string contactNo = dt.Columns[4].ColumnName.ToLower();
                string otherDescription = dt.Columns[5].ColumnName.ToLower();
                string longitude = dt.Columns[6].ColumnName.ToLower();
                string latitude = dt.Columns[7].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_PoliceStation where ";
                    if (policeStationName.ToLower().Trim() == "policestationname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && policeType.ToLower().Trim() == "policetype" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string policeStationNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "policeStationName" + k.ToString();
                                policeStationNames = "policeStationName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["policeStationName"].ToString());

                            }
                            else
                            {
                                para = "parkingName" + k.ToString();
                                policeStationNames = policeStationNames + " or policeStationName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["policeStationName"].ToString());
                            }
                        }
                        query = query + policeStationNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();
                        // dtAlreadyExists = ob.checkDuplicateEmailAndContactNo(emailIDs, contactnos);

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("policeStationName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("contactNo", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["policeStationName"].ToString() == dtAlreadyExists.Rows[j]["policeStationName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["policeStationName"] = dt.Rows[i]["policeStationName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add police station. Because following stations already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("policeStationName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("policeType", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["policeStationName"] = dt.Rows[i]["policeStationName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["policeType"] = dt.Rows[i]["policeType"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("policestation");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkPoliceStation(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Police Station details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertTourism(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string touristSpotName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string contactNo = dt.Columns[3].ColumnName.ToLower();
                string coverImage = dt.Columns[4].ColumnName.ToLower();
                string activitiesId = dt.Columns[5].ColumnName.ToLower();
                string activities = dt.Columns[6].ColumnName.ToLower();
                string distanceFrom = dt.Columns[7].ColumnName.ToLower();
                string otherDescription = dt.Columns[8].ColumnName.ToLower();
                string longitude = dt.Columns[9].ColumnName.ToLower();
                string latitude = dt.Columns[10].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_tourism where ";
                    if (touristSpotName.ToLower().Trim() == "touristspotname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && contactNo.ToLower().Trim() == "contactno" && coverImage.ToLower().Trim() == "coverimage" && activitiesId.ToLower().Trim() == "activitiesid" && activities.ToLower().Trim() == "activities" && distanceFrom.ToLower().Trim() == "distancefrom" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string touristSpotNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "touristSpotName" + k.ToString();
                                touristSpotNames = "touristSpotName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["touristSpotName"].ToString());

                            }
                            else
                            {
                                para = "touristSpotName" + k.ToString();
                                touristSpotNames = touristSpotNames + " or touristSpotName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["touristSpotName"].ToString());
                            }
                        }
                        query = query + touristSpotNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("touristSpotName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("contactNo", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["touristSpotName"].ToString() == dtAlreadyExists.Rows[j]["touristSpotName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["touristSpotName"] = dt.Rows[i]["touristSpotName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add tourist places. Because following places already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("touristSpotName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("coverImage", typeof(string));
                            dtToAdd.Columns.Add("activitiesId", typeof(string));
                            dtToAdd.Columns.Add("activities", typeof(string));
                            dtToAdd.Columns.Add("distanceFrom", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["touristSpotName"] = dt.Rows[i]["touristSpotName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                dtRow["coverImage"] = dt.Rows[i]["coverImage"].ToString();
                                dtRow["activitiesId"] = dt.Rows[i]["activitiesId"].ToString();
                                dtRow["activities"] = dt.Rows[i]["activities"].ToString();
                                dtRow["distanceFrom"] = dt.Rows[i]["distanceFrom"].ToString();
                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("tourism");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkTourism(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Tourism details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(0, "Tourism details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(0, "Tourism details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertCinemas(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string theatreName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string displayImage = dt.Columns[3].ColumnName.ToLower();
                string noOfScreens = dt.Columns[4].ColumnName.ToLower();
                string contactNo = dt.Columns[5].ColumnName.ToLower();
                string otherDescription = dt.Columns[6].ColumnName.ToLower();
                string longitude = dt.Columns[7].ColumnName.ToLower();
                string latitude = dt.Columns[8].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_CinemaHall where ";
                    if (theatreName.ToLower().Trim() == "theatrename" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && displayImage.ToLower().Trim() == "displayimage" && noOfScreens.ToLower().Trim() == "noofscreens" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string theatreNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "theatreName" + k.ToString();
                                theatreNames = "theatreName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["theatreName"].ToString());

                            }
                            else
                            {
                                para = "theatreName" + k.ToString();
                                theatreNames = theatreNames + " or theatreName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["theatreName"].ToString());
                            }
                        }
                        query = query + theatreNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("theatreName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("contactNo", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["theatreName"].ToString() == dtAlreadyExists.Rows[j]["theatreName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["theatreName"] = dt.Rows[i]["theatreName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add cinema halls. Because following names already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("theatreName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("displayImage", typeof(string));
                            dtToAdd.Columns.Add("noOfScreens", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["theatreName"] = dt.Rows[i]["theatreName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["displayImage"] = dt.Rows[i]["displayImage"].ToString();
                                dtRow["noOfScreens"] = dt.Rows[i]["noOfScreens"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("cinemas");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkCinemas(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Cinema hall details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Cinema hall details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Cinema hall details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertRestaurant(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string hotelName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string starRating = dt.Columns[3].ColumnName.ToLower();
                string contactNo = dt.Columns[4].ColumnName.ToLower();
                string otherDescription = dt.Columns[5].ColumnName.ToLower();
                string longitude = dt.Columns[6].ColumnName.ToLower();
                string latitude = dt.Columns[7].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_Hotel where ";
                    if (hotelName.ToLower().Trim() == "hotelname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && starRating.ToLower().Trim() == "starrating" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string hotelNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "hotelName" + k.ToString();
                                hotelNames = "hotelName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["hotelName"].ToString());

                            }
                            else
                            {
                                para = "hotelName" + k.ToString();
                                hotelNames = hotelNames + " or hotelName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["hotelName"].ToString());
                            }
                        }
                        query = query + hotelNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("hotelName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("contactNo", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["hotelName"].ToString() == dtAlreadyExists.Rows[j]["hotelName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["hotelName"] = dt.Rows[i]["hotelName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add restaurants. Because following names already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("hotelName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("starRating", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["hotelName"] = dt.Rows[i]["hotelName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["starRating"] = dt.Rows[i]["starRating"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();

                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("restaurant");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkRestaurant(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Restaurant details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Restaurant details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Restaurant details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }

        public void insertFuelStations(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                dt = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
               
                string fuelPointName = dt.Columns[0].ColumnName.ToLower();
                string address = dt.Columns[1].ColumnName.ToLower();
                string area = dt.Columns[2].ColumnName.ToLower();
                string fuelComapny = dt.Columns[3].ColumnName.ToLower();
                string displayImage = dt.Columns[4].ColumnName.ToLower();
                string contactNo = dt.Columns[5].ColumnName.ToLower();
                string otherDescription = dt.Columns[6].ColumnName.ToLower();
                string longitude = dt.Columns[7].ColumnName.ToLower();
                string latitude = dt.Columns[8].ColumnName.ToLower();
                try
                {
                    string query = "select * from tbl_FuelPoint where ";
                    if (fuelPointName.ToLower().Trim() == "fuelpointname" && address.ToLower().Trim() == "address" && area.ToLower().Trim() == "area" && fuelComapny.ToLower().Trim() == "fuelcomapny" && displayImage.ToLower().Trim() == "displayimage" && contactNo.ToLower().Trim() == "contactno" && otherDescription.ToLower().Trim() == "otherdescription" && longitude.ToLower().Trim() == "longitude" && latitude.ToLower().Trim() == "latitude")
                    {
                        string para = "";
                        SqlCommand cmd1 = new SqlCommand();
                        DataTable dtAlreadyExists = new DataTable();
                        string fuelPointNames = "";
                        int k = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            k = k + 1;
                            if (i == 0)
                            {

                                para = "fuelPointName" + k.ToString();
                                fuelPointNames = "fuelPointName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["fuelPointName"].ToString());

                            }
                            else
                            {
                                para = "fuelPointName" + k.ToString();
                                fuelPointNames = fuelPointNames + " or fuelPointName=@" + para + "";
                                cmd1.Parameters.AddWithValue(para, dt.Rows[i]["fuelPointName"].ToString());
                            }
                        }
                        query = query + fuelPointNames;
                        cmd1.Connection = conn;
                        cmd1.CommandText = query;
                        Openconn();
                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        da.Fill(dtAlreadyExists);
                        conn.Close();

                        int x = dtAlreadyExists.Rows.Count;

                        if (dtAlreadyExists.Rows.Count > 0)
                        {
                            DataTable dtExistsList = new DataTable();
                            dtExistsList.Columns.Add("fuelPointName", typeof(string));
                            dtExistsList.Columns.Add("address", typeof(string));
                            dtExistsList.Columns.Add("area", typeof(string));
                            dtExistsList.Columns.Add("contactNo", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {

                                for (int j = 0; j < dtAlreadyExists.Rows.Count; j++)
                                {
                                    if (dt.Rows[i]["fuelPointName"].ToString() == dtAlreadyExists.Rows[j]["fuelPointName"].ToString())
                                    {
                                        DataRow dtRow = dtExistsList.NewRow();
                                        dtRow["fuelPointName"] = dt.Rows[i]["fuelPointName"].ToString();
                                        dtRow["address"] = dt.Rows[i]["address"].ToString();
                                        dtRow["area"] = dt.Rows[i]["area"].ToString();
                                        dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                        dtExistsList.Rows.Add(dtRow);
                                    }
                                }

                            }
                            GridView1.DataSource = dtExistsList;
                            GridView1.DataBind();
                            GridView1.Visible = true;
                            messages1.Visible = true;
                            gridData.Visible = false;
                            messages1.SetMessage(0, "Can not add fuel point. Because following names already exists.");
                        }
                        else
                        {
                            DataTable dtToAdd = new DataTable();
                            dtToAdd.Columns.Add("exploreCatId", typeof(string));
                            dtToAdd.Columns.Add("fuelPointName", typeof(string));
                            dtToAdd.Columns.Add("address", typeof(string));
                            dtToAdd.Columns.Add("area", typeof(string));
                            dtToAdd.Columns.Add("fuelComapny", typeof(string));
                            dtToAdd.Columns.Add("displayImage", typeof(string));
                            dtToAdd.Columns.Add("contactNo", typeof(string));
                            dtToAdd.Columns.Add("otherDescription", typeof(string));
                            dtToAdd.Columns.Add("insertedBy", typeof(string));
                            dtToAdd.Columns.Add("longitude", typeof(string));
                            dtToAdd.Columns.Add("latitude", typeof(string));
                            dtToAdd.Columns.Add("batch", typeof(string));
                            dtToAdd.Columns.Add("status", typeof(string));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                DataRow dtRow = dtToAdd.NewRow();
                                dtRow["exploreCatId"] = lblExploreKochiId.Text;
                                dtRow["fuelPointName"] = dt.Rows[i]["fuelPointName"].ToString();
                                dtRow["address"] = dt.Rows[i]["address"].ToString();
                                dtRow["area"] = dt.Rows[i]["area"].ToString();
                                dtRow["fuelComapny"] = dt.Rows[i]["fuelComapny"].ToString();
                                dtRow["displayImage"] = dt.Rows[i]["displayImage"].ToString();
                                dtRow["contactNo"] = dt.Rows[i]["contactNo"].ToString();
                                dtRow["otherDescription"] = dt.Rows[i]["otherDescription"].ToString();
                                dtRow["insertedBy"] = uDetails[0].ToString();
                                dtRow["longitude"] = dt.Rows[i]["longitude"].ToString();
                                dtRow["latitude"] = dt.Rows[i]["latitude"].ToString();
                                dtRow["batch"] = lblBatchId.Text;
                                dtRow["status"] = "Pending";

                                dtToAdd.Rows.Add(dtRow);
                            }

                            DataSet ds = new DataSet("fuelpoint");
                            int xyz = dtToAdd.Rows.Count;
                            ds.Tables.Add(dtToAdd);
                            string strXml = ds.GetXml();
                            int insRows = _ob.inserBulkFuelPoint(strXml);
                            if (insRows > 0)
                            {
                                messages1.Visible = true;
                                if (insRows == xyz)
                                {
                                    messages1.SetMessage(1, "Fuel point details added successfully !!");
                                    gridData.Visible = true;
                                    DataTable dtFinal = _ob.GetEntryByIdBatch(lblExploreKochiId.Text, lblBatchId.Text);
                                    gridData.DataSource = dtFinal;
                                    gridData.DataBind();
                                    GridView1.Visible = false;
                                }
                                else
                                {
                                    messages1.SetMessage(1, "Fuel point details not added !!");
                                }
                            }
                            else
                            {
                                messages1.Visible = true; messages1.SetMessage(1, "Fuel point details not added !!");
                            }
                        }
                    }
                    else
                    {
                        messages1.Visible = true;
                        messages1.SetMessage(0, "Please check excel sheet headings and data.");
                    }
                }
                catch (Exception ee)
                {
                    messages1.Visible = true;
                    messages1.SetMessage(0, "Uploaded Excel Sheet is not in proper format !" + ee.Message + "");
                }

            }
        }



    }
}