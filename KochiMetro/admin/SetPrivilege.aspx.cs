﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro.admin
{
    public partial class SetPrivilege : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
                    {
                        btnEdit.Visible = false;
                        ddType.DataBind();
                        ddType.SelectedValue = "Maker";
                        DataTable dtGridData = new DataTable();
                        dtGridData = _ob.GetActiveAdminsByType("Maker");
                        if (dtGridData.Rows.Count > 0)
                        {
                            CheckBoxList1.DataSource = dtGridData;
                            CheckBoxList1.DataBind();
                        }
                        else
                        {
                            CheckBoxList1.ClearSelection();
                            lblUsers.Text = "No Users !!";
                            lblUsers.Visible = true;
                        }

                        if (ddType.SelectedItem.Text == "-Set privileges for-")
                        {
                            Panel1.Enabled = false;
                        }

                        try
                        {
                            if (Session["id"].ToString().Length > 0)
                            {
                                string[] splittedID = Session["id"].ToString().Split(';');
                                if (splittedID[1].ToString() == "view")
                                {
                                    btnEdit.Visible = true;
                                    ddType.DataBind();
                                    ddType.SelectedValue = splittedID[1];
                                    ddType.Enabled = false;
                                    dtGridData = _ob.GetActiveAdminsByType(splittedID[2]);
                                    if (dtGridData.Rows.Count > 0)
                                    {
                                        CheckBoxList1.DataSource = dtGridData;
                                        CheckBoxList1.DataBind();
                                    }
                                    else
                                    {
                                        CheckBoxList1.ClearSelection();
                                        lblUsers.Text = "No Users !!";
                                        lblUsers.Visible = true;
                                    }



                                    Panel1.Enabled = false;
                                    btnSet.Visible = false;
                                    btnEdit.Visible = true;

                                }
                                if (splittedID[1].ToString() == "edit")
                                {
                                    ddType.DataBind();
                                    ddType.SelectedValue = splittedID[1].ToString();
                                    ddType.Enabled = false;
                                    dtGridData = _ob.GetActiveAdminsByType(splittedID[2]);
                                    if (dtGridData.Rows.Count > 0)
                                    {
                                        CheckBoxList1.DataSource = dtGridData;
                                        CheckBoxList1.DataBind();

                                    }
                                    else
                                    {
                                        CheckBoxList1.ClearSelection();
                                        lblUsers.Text = "No Users !!";
                                        lblUsers.Visible = true;
                                    }
                                    btnSet.Text = "Update";
                                }

                                for (int j = 0; j < CheckBoxList1.Items.Count; j++)
                                {
                                    if (splittedID[0].ToString() == CheckBoxList1.Items[j].Value)
                                    {
                                        CheckBoxList1.Items[j].Selected = true; break;
                                    }
                                }

                                DataTable dtPrivilege = new DataTable();
                                dtPrivilege = _ob.GetAdminPrivilegeByAdminId(splittedID[0]);
                                if (dtPrivilege.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtPrivilege.Rows.Count; i++)
                                    {
                                        if (dtPrivilege.Rows[i]["pId"].ToString() == "1" && dtPrivilege.Rows[i]["isChecked"].ToString() == "True")
                                        {
                                            chkExploreKochi.Checked = true;
                                            continue;
                                        }
                                        if (dtPrivilege.Rows[i]["pId"].ToString() == "2" && dtPrivilege.Rows[i]["isChecked"].ToString() == "True")
                                        {
                                            chkOffers.Checked = true;
                                            continue;
                                        }
                                        if (dtPrivilege.Rows[i]["pId"].ToString() == "3" && dtPrivilege.Rows[i]["isChecked"].ToString() == "True")
                                        {
                                            chkUser.Checked = true;
                                            continue;
                                        }

                                    }
                                }
                                Session.Remove("id");
                            }
                        }
                        catch (Exception)
                        {
                            //ignored
                        }
                    }
                    else
                    {
                        Response.Redirect("/Default.aspx");
                    }
                }

            }
            catch (Exception)
            {
                //ignored
            }

        }

        protected void ddType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddType.SelectedItem.Text != "-Set privileges for-")
            {
                DataTable dtGridData = _ob.GetActiveAdminsByType(ddType.SelectedValue);
                if (dtGridData.Rows.Count > 0)
                {
                    CheckBoxList1.DataSource = dtGridData;
                    CheckBoxList1.DataBind();
                }
                else
                {
                    CheckBoxList1.ClearSelection();
                    lblUsers.Text = "No Users !!";
                    lblUsers.Visible = true;
                }
                if (ddType.SelectedItem.Text == "-Set privileges for-")
                {
                    Panel1.Enabled = false;
                }
            }
            else
            {
                //DataTable dtGridData = _ob.GetAllEntries();
                //gridData.DataSource = dtGridData;
                //gridData.DataBind(); ViewState["dtbl"] = dtGridData;
            }
        }

        protected void ddTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddTeam.SelectedValue != "--Select Team--" && ddType.SelectedValue != "--Set privileges for--")
            {
                DataTable dtGridData = _ob.GetAdminsByTypeTeam(ddType.SelectedValue, ddTeam.SelectedValue);
                if (dtGridData.Rows.Count > 0)
                {
                    CheckBoxList1.DataSource = dtGridData;
                    CheckBoxList1.DataBind();
                }
                else
                {
                    CheckBoxList1.ClearSelection();
                    lblUsers.Text = "No Users !!";
                    lblUsers.Visible = true;
                }
            }
            else
            {
                DataTable dtGridData = _ob.GetActiveAdminsByType("Maker");
                if (dtGridData.Rows.Count > 0)
                {
                    CheckBoxList1.DataSource = dtGridData;
                    CheckBoxList1.DataBind();
                }
                else
                {
                    CheckBoxList1.ClearSelection();
                    lblUsers.Text = "No Users !!";
                    lblUsers.Visible = true;
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnSet.Text = "Update";
            btnEdit.Visible = false;
            Panel1.Enabled = true;
            btnSet.Visible = true;
        }

        protected void btnSet_Click(object sender, EventArgs e)
        {

            string Admins = ""; string[] splittedAdmins;

            foreach (ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                {
                    Admins += item.Value + ";";
                }
            }


            Admins = Admins.TrimStart(';');
            Admins = Admins.TrimEnd(';');
            splittedAdmins = Admins.Split(';');
            if (btnSet.Text == "Set")
            {
                if (Admins.Length > 0)
                {
                    foreach (string j in splittedAdmins)
                    {
                        if (chkExploreKochi.Checked)
                        {
                            //1 is pid of explore kochi
                            string res = _ob.AddAdminPrivilege("1", j, chkExploreKochi.Checked.ToString());
                        }
                        else
                        {
                            //1 is pid of explore kochi
                            string res = _ob.AddAdminPrivilege("1", j, "False");
                        }

                        if (chkOffers.Checked)
                        {
                            //2 is pid of offers and promotion
                            string res1 = _ob.AddAdminPrivilege("2", j, chkOffers.Checked.ToString());
                        }
                        else
                        {
                            //2 is pid of offers and promotion
                            string res1 = _ob.AddAdminPrivilege("2", j, "False");
                        }

                        if (chkUser.Checked)
                        {
                            //3 is pid of user management
                            string res2 = _ob.AddAdminPrivilege("3", j, chkUser.Checked.ToString());
                        }
                        else
                        {
                            //3 is pid of user management
                            string res2 = _ob.AddAdminPrivilege("3", j, "False");
                        }
                        messages1.SetMessage(1, "Privileges added successfully !!");
                        messages1.Visible = true;
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please select users !!");
                    messages1.Visible = true;
                }

            }
            if (btnSet.Text == "Update")
            {
                if (Admins.Length > 0)
                {
                    foreach (string j in splittedAdmins)
                    {
                        if (chkExploreKochi.Checked)
                        {
                            //1 is pid of explore kochi
                            string res = _ob.AddAdminPrivilege("1", j, chkExploreKochi.Checked.ToString());
                        }
                        else
                        {
                            //1 is pid of explore kochi
                            string res = _ob.AddAdminPrivilege("1", j, "False");
                        }

                        if (chkOffers.Checked)
                        {
                            //2 is pid of offers and promotion
                            string res1 = _ob.AddAdminPrivilege("2", j, chkOffers.Checked.ToString());
                        }
                        else
                        {
                            //2 is pid of offers and promotion
                            string res1 = _ob.AddAdminPrivilege("2", j, "False");
                        }

                        if (chkUser.Checked)
                        {
                            //3 is pid of user management
                            string res2 = _ob.AddAdminPrivilege("3", j, chkUser.Checked.ToString());
                        }
                        else
                        {
                            //3 is pid of user management
                            string res2 = _ob.AddAdminPrivilege("3", j, "False");
                        }

                        messages1.SetMessage(1, "Privileges updated successfully !!");
                        messages1.Visible = true;
                    }
                }
                else
                {
                    messages1.SetMessage(2, "Please select users !!");
                    messages1.Visible = true;
                }

            }
        }

        protected void chkAdmin_DataBound(object sender, EventArgs e)
        {

        }

    }
}