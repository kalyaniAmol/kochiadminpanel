﻿<%@ Page Title="Manage Checker" Language="C#" MasterPageFile="~/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="ManageChecker.aspx.cs" Inherits="KochiMetro.admin.ManageChecker" %>

<%@ Register Src="../messages.ascx" TagName="messages" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #search input
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            height: 20px;
            margin: 0;
            padding:5px 9px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
        }
        
        #search a.button
        {
            background: url("/images/search.png") no-repeat scroll center center #7eac10;
            cursor: pointer;
            float: left;
            height: 32px;
            text-indent: -99999em;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 40px;
            border: 0 solid #fff;
        }
        
        #search a.button:hover
        {
            background-color: #000;
        }.dbtnvs { cursor: pointer; background-color: #4CAF50; border: none; color: white; padding: 6px 7px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight: bold; width: 80px; } .actbtnvs { cursor: pointer; background-color: #f44336; border: none; color: white; padding: 6px 8px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px; font-weight: bold; width:80px; }
       
        div.select-box select
        {
            background: none repeat scroll 0 0 #fff;
            border: 1px solid #ccc;
            color: #7F7F7F;
            float: left;
            font: 12px 'Helvetica' , 'Lucida Sans Unicode' , 'Lucida Grande' ,sans-serif;
            margin: 0;
            padding:5px 10px;
            -webkit-transition: background 0.3s ease-in-out 0;
            -moz-transition: background 0.3s ease-in-out 0;
            -ms-transition: background 0.3s ease-in-out 0;
            -o-transition: background 0.3s ease-in-out 0;
            transition: background 0.3s ease-in-out 0;
            width: 270px;
            margin-left: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="true">
        <ProgressTemplate>
            <div style="text-align: center;">
                <div style="height: 200%; position: absolute; top: 0; left: 0; visibility: visible;
                    display: block; width: 100%; height: 100%; background-color: #9fddea; background-color: rgba(159, 221, 234, 0.94);
                    z-index: 999; -webkit-filter: alpha(opacity=75); -moz-filter: alpha(opacity=75);
                    -o-filter: alpha(opacity=75); filter: alpha(opacity=75); -ms-opacity: 0.75; opacity: 0.75;
                    padding-top: 20%;">
                    <div style="z-index: 1000; position: absolute; top: 0; left: 0; padding-top: 25%;
                        visibility: visible; display: block; text-align: center; -ms-border-radius: 10px;
                        border-radius: 10px; width: 100%;">
                        <div>
                            Please Wait...
                            <br />
                            <img src="/images/loader.gif" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblAdminId" runat="server" Text="" Visible="False"></asp:Label>
            <h2 class="page-title" style="    margin-left: 3%;">
                Manage Checkers</h2>
            <div id="DivToHide">
                <uc1:messages ID="messages1" runat="server" />
            </div>
            <div class="content-box">
                <div class="search-select" style="margin-bottom:0px;">
                    <div class="select-box">
                        <asp:DropDownList ID="ddType" runat="server" DataTextField="privilegeType" DataValueField="pId"
                            OnSelectedIndexChanged="ddType_SelectedIndexChanged" OnDataBinding="ddType_DataBinding"
                            OnDataBound="ddType_DataBound" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="select-box">
                        <asp:DropDownList ID="ddTeam" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddTeam_SelectedIndexChanged">
                            <asp:ListItem Value="--Select Team--">--Select Team--</asp:ListItem>
                            <asp:ListItem Value="KMRL">KMRL</asp:ListItem>
                            <asp:ListItem Value="Checker">Axis</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <a class="large-btn" href="/admin/AddAdmin.aspx">Add Checker</a>
                </div>
                <asp:GridView ID="gridData" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    EmptyDataText="No results to show !!" AllowSorting="True" OnSorting="gridData_Sorting"
                    PageSize="15" CssClass="explore-listing vstabless" OnRowDataBound="gridData_RowDataBound"
                    OnRowCreated="gridData_RowCreated" OnRowCommand="gridData_RowCommand" DataKeyNames="adminId"
                    OnPageIndexChanging="gridData_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="S.No" ItemStyle-Height="20">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                            <ItemStyle Height="20px" HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="adminId" HeaderText="adminId" ReadOnly="True" />
                        <asp:BoundField DataField="isActive" HeaderText="isActive" ReadOnly="True" />
                        <asp:TemplateField HeaderText="Name" SortExpression="name">
                            <ItemTemplate>
                                <%#Eval("name")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="mobileNo" HeaderText="Mobile No."   ItemStyle-Width="100px"/>
                        <asp:BoundField DataField="emailId" HeaderText="Email Id" />
                        <asp:BoundField DataField="createdDate1" HeaderText="Rgistration Date"   ItemStyle-Width="100px"/>
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeny" runat="server" CausesValidation="false" CommandName="change"
                                    Text="Edit" CommandArgument='<%# Eval("adminId") %>' />  &nbsp; &Iota; &nbsp;
                                <asp:LinkButton ID="btnReturn" runat="server" CausesValidation="false" CommandName="remove"
                                    Text="Delete" CommandArgument='<%# Eval("adminId") %>' OnClientClick="return confirm('Are you sure you want to delete this Record?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:Button ID="btnActive" runat="server" CausesValidation="false" CommandName="active"
                                  CssClass="actbtnvs"   Text="Deactive" CommandArgument='<%# Eval("adminId") %>' />
                                <asp:Button ID="btnDeactive" runat="server" CausesValidation="false" CommandName="deactive"
                                    CssClass="dbtnvs"   Text="Active" CommandArgument='<%# Eval("adminId") %>' OnClientClick="return confirm('Are you sure you want to deactivate this Record?');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Privileges" ItemStyle-Width="80px">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnView" runat="server" CausesValidation="false" CommandName="view"
                                    Text="View" CommandArgument='<%# Eval("adminId") %>' />
                                &nbsp; &Iota; &nbsp;
                                <asp:LinkButton ID="btnEdit" runat="server" CausesValidation="false" CommandName="pedit"
                                    Text="Edit" CommandArgument='<%# Eval("adminId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
