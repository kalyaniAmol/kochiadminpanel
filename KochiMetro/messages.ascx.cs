﻿using System;
using System.Web.UI;

namespace KochiMetro
{
    public partial class Messages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Display()
        {
        }
        public void SetMessage(int type, string msg)
        {
            message.Text = msg;
            switch (type)
            {
                case 1:
                    mypanel.CssClass = "notification n-success";
                    Label1.Text = "Success - ";
                    break;
                case 2:
                    mypanel.CssClass = "notification n-information";
                    Label1.Text = "Info - ";
                    break;
                case 3:
                    mypanel.CssClass = "notification n-attention";
                    Label1.Text = "Notice - ";
                    break;
                default:
                    mypanel.CssClass = "notification n-error";
                    Label1.Text = "Error - ";
                    break;
            }

            string script = "<script type=\"text/javascript\"> var seconds = 10;  setTimeout(function () { document.getElementById('DivToHide').style.display = 'none';  }, seconds * 1000); </script>";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "key", script, false);
        }
    }
}