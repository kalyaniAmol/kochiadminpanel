﻿using System;
using System.Web;
using System.Web.Security;

namespace KochiMetro
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {  
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            //Label1.Text = System.DateTime.Now.ToString();

            if (HttpContext.Current.User.Identity.IsAuthenticated && uDetails[2] == "Admin")
            {
                if(!IsPostBack)
                {
                    lblName.Text = "Welcome  " +uDetails[1];
                    lblDate.Text = uDetails[3];
                }
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }

        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/Default.aspx");
        }
    }
}