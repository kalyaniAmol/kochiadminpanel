﻿using System;
using System.Web;
using System.Web.Security;
using System.Data;
using KochiMetro.classes;

namespace KochiMetro
{
    public partial class SuperCheckerMaster : System.Web.UI.MasterPage
    {
        private readonly AdminClass _ob = new AdminClass();
        private readonly InfoClass _obInfo = new InfoClass();
        string[] uDetails = HttpContext.Current.User.Identity.Name.Split(',');
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && (uDetails[2] == "Admin" || uDetails[2] == "Super Checker"))
            {
                if (!IsPostBack)
                {
                }
            }
            else
            {
                Response.Redirect("/SuperCheckerLogin.aspx");
            }
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("/SuperCheckerLogin.aspx");
        }
    }
}