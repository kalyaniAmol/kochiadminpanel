﻿using System;
using System.Data;
using System.Web.Security;
using KochiMetro.classes;

namespace KochiMetro
{
    public partial class SuperCheckerLogin : System.Web.UI.Page
    {
        private readonly AdminClass _ob = new AdminClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt;
                dt = _ob.GetAdminLoginDetails(txtMobileNo.Text, txtPassword.Text);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["adminType"].ToString() == "Admin" || dt.Rows[0]["adminType"].ToString() == "Super Checker")
                    {
                        FormsAuthentication.RedirectFromLoginPage(dt.Rows[0]["adminId"] + "," + dt.Rows[0]["name"] + "," + dt.Rows[0]["adminType"], false);

                        Response.Redirect("/superchecker/Dashboard.aspx", false);
                    }

                }
                else
                {
                    messages2.Visible = true;
                    messages2.SetMessage(0, "Invalid Login Details !");
                }
            }
            catch (Exception ee)
            {
                messages2.Visible = true;
                messages2.SetMessage(0, ee.Message);
            }
        }
    }
}