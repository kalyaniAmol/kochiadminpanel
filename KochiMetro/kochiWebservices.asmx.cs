﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using KochiMetro.classes;
using System.Web;

namespace KochiMetro
{
    /// <summary>
    /// Summary description for kochiWebservices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class KochiWebservices : WebService
    {
        public readonly SqlConnection Cn = new SqlConnection(ConfigurationManager.ConnectionStrings["kochiMetroConnectionString"].ToString());
        readonly InfoClass _ob = new InfoClass();
        readonly AdminClass _obAdmin = new AdminClass();
        readonly CryptLib _crypt = new CryptLib();
        #region IMPORTANT
        [WebMethod]
        public string Encrypt(string plainText)  // Method to encrypt string to AES encrytped text 
        {
            string iv = CryptLib.GenerateRandomIV(16); //16 bytes = 128 bits
            string key = CryptLib.getHashSha256("my secret key", 32); //32 bytes = 256 bits
            String cypherText = _crypt.encrypt(plainText, key, iv);
            return cypherText + "~" + iv;
        }


        [WebMethod]
        public string Decrypt(string cypherText)  // Method to decrypt AES encrytped text to string
        {
            cypherText = cypherText.Replace(" ", "+");
            string[] decoded = cypherText.Split('~');
            CryptLib.GenerateRandomIV(16);
            string key = CryptLib.getHashSha256("my secret key", 32); //32 bytes = 256 bits
            string plainText = _crypt.decrypt(decoded[0], key, decoded[1]);
            return plainText;
        }

        [WebMethod]
        public string Sha2EncryptionMethodKey(string password)
        {
            System.Security.Cryptography.SHA256 sha256 = new System.Security.Cryptography.SHA256Managed();
            byte[] sha256Bytes = System.Text.Encoding.Default.GetBytes(password);
            byte[] cryString = sha256.ComputeHash(sha256Bytes);
            string sha256Str = string.Empty;
            for (int i = 0; i < cryString.Length; i++)
            {
                sha256Str += i.ToString("X");
            }
            return sha256Str;
        }

        #endregion

        #region sign up

        /// <summary>
        /// Web-service is used for first time registration of user.
        /// If user is already registered the details of user get updated.
        /// </summary>
        /// <param name="mobileNo"></param>
        /// <param name="udob"></param>
        /// <param name="referredUserCode"></param>
        /// <param name="signUpType"></param>
        /// <param name="imeiNo"></param>
        /// <param name="deviceType"></param>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SignUp(string mobileNo, string udob, string referredUserCode, string signUpType, string imeiNo, string deviceType)
        {
            var serializer = new JavaScriptSerializer();
            LogSuccess("Webservice called", "SignUp", "mobileNo=" + mobileNo + ",udob=" + udob + ",referredUserCode=" + referredUserCode + ",signUpType=" + signUpType + ",imeiNo=" + imeiNo + ",deviceType=" + deviceType);


            LogSteps("Serializer object created", "SignUp");
            var dt1 = new DataTable();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            string status = "";
            try
            {
                string veriPin = GenerateVerificationCode();
                LogSteps("Verification pin generated:" + veriPin, "SignUp");
                string referralCode = GenerateReferralCode(mobileNo);
                LogSteps("Referral code generated:" + referralCode, "SignUp");
                dt1 = _ob.SignUp(mobileNo, udob, referralCode, referredUserCode, Encrypt(veriPin), signUpType, imeiNo, deviceType);
                if (dt1.Rows.Count > 0)
                {
                    LogSteps("Response from SignUp stored procedure:" + dt1.Rows.Count, "SignUp");
                    if (dt1.Rows[0][0].ToString().Contains("Mobile number and date of birth mismatched") || dt1.Rows[0][0].ToString().Contains("You are already registered as app only user") || dt1.Rows[0][0].ToString().Contains("Deactivated User"))
                    {
                        DataTable dt = new DataTable();
                        status = "Fail";
                        dt.Columns.Add("result", typeof(string));

                        DataRow dtRow = dt.NewRow();
                        dtRow["result"] = dt1.Rows[0][0].ToString();
                        dt.Rows.Add(dtRow);

                        foreach (DataRow rs in dt.Rows)
                        {
                            var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                            rows.Add(row);
                        }
                    }
                    else
                    {
                        status = "Success";
                        try
                        {
                            string strUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=" + ConfigurationManager.AppSettings["username"] + ":" + ConfigurationManager.AppSettings["password"] + "&senderID=" + ConfigurationManager.AppSettings["sender"] + "&receipientno=" + mobileNo + "&dcs=0&msgtxt=" + veriPin + "&state=1";
                            WebRequest request = WebRequest.Create(strUrl);
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            Stream s = response.GetResponseStream();
                            if (s != null)
                            {
                                LogSteps("Sending Message:", "SignUp");
                                StreamReader readStream = new StreamReader(s);
                                readStream.ReadToEnd();
                                response.Close();
                                s.Close();
                                readStream.Close();
                                LogSteps("Message sent: ", "SignUp");
                            }
                        }
                        catch (Exception ee)
                        {
                            LogSteps("Error in sending mail. Error:" + ee.Message, "SignUp");
                        }

                        foreach (DataRow rs in dt1.Rows)
                        {
                            var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                            rows.Add(row);
                        }
                        LogSuccess("Success", "SignUp", serializer.Serialize(rows));
                    }
                }
            }
            catch (Exception ee)
            {
                LogError(ee, "mobileNo=" + mobileNo + ",udob=" + udob + ",referredUserCode=" + referredUserCode + ",signUpType=" + signUpType + ",imeiNo=" + imeiNo + ",deviceType=" + deviceType, "SignUp");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ManualSignUp(string pname, string emailId, string mobileNo, string udob, string referredUserCode, string signUpType, string imeiNo, string deviceType)
        {
            var serializer = new JavaScriptSerializer();
            LogSuccess("Webservice called", "ManualSignUp", "pname=" + pname + "mobileNo=" + mobileNo + ",udob=" + udob + ",referredUserCode=" + referredUserCode + ",signUpType=" + signUpType + ",imeiNo=" + imeiNo + ",deviceType=" + deviceType);


            LogSteps("Serializer object created", "ManualSignUp");
            var dt1 = new DataTable();
            string status = "";
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            try
            {
                string veriPin = GenerateVerificationCode();
                LogSteps("Verification pin generated:" + veriPin, "ManualSignUp");
                string referralCode = GenerateReferralCode(pname);
                LogSteps("Referral code generated:" + referralCode, "ManualSignUp");
                dt1 = _ob.ManualSignUp(pname, emailId, mobileNo, udob, referralCode, referredUserCode, Encrypt(veriPin), signUpType, imeiNo, deviceType);
                if (dt1.Rows.Count > 0)
                {
                    LogSteps("Response from ManualSignUp stored procedure:" + dt1.Rows.Count, "ManualSignUp");
                    if (dt1.Rows[0][0].ToString().Contains("Mobile number and date of birth mismatched") || dt1.Rows[0][0].ToString().Contains("You are already registered as kochi card user") || dt1.Rows[0][0].ToString().Contains("Deactivated User"))
                    {
                        DataTable dt = new DataTable();
                        status = "Fail";
                        dt.Columns.Add("result", typeof(string));

                        DataRow dtRow = dt.NewRow();
                        dtRow["result"] = dt1.Rows[0][0].ToString();
                        dt.Rows.Add(dtRow);

                        foreach (DataRow rs in dt.Rows)
                        {
                            var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                            rows.Add(row);
                        }
                    }
                    else
                    {
                        status = "Success";
                        try
                        {
                            string strUrl = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=" + ConfigurationManager.AppSettings["username"] + ":" + ConfigurationManager.AppSettings["password"] + "&senderID=" + ConfigurationManager.AppSettings["sender"] + "&receipientno=" + mobileNo + "&dcs=0&msgtxt=" + veriPin + "&state=1";
                            WebRequest request = WebRequest.Create(strUrl);
                            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                            Stream s = response.GetResponseStream();
                            if (s != null)
                            {
                                LogSteps("Sending Message:", "ManualSignUp");
                                StreamReader readStream = new StreamReader(s);
                                readStream.ReadToEnd();
                                response.Close();
                                s.Close();
                                readStream.Close();
                                LogSteps("Message sent: ", "ManualSignUp");
                            }
                        }
                        catch (Exception ee)
                        {
                            LogSteps("Error in sending mail. Error:" + ee.Message, "ManualSignUp");
                        }

                        foreach (DataRow rs in dt1.Rows)
                        {
                            var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                            rows.Add(row);
                        }

                        LogSuccess("Success", "ManualSignUp", serializer.Serialize(rows));
                    }
                }
            }
            catch (Exception ee)
            {
                LogError(ee, "pname=" + pname + "mobileNo=" + mobileNo + ",udob=" + udob + ",referredUserCode=" + referredUserCode + ",signUpType=" + signUpType + ",imeiNo=" + imeiNo + ",deviceType=" + deviceType, "ManualSignUp");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void VerifyOtp(string userId, string veriPin)
        {
            LogSteps("Web-service called", "VerifyOtp");
            veriPin = Decrypt(veriPin);
            var serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "VerifyOtp");

            LogSteps("Parametres for Web-service: userId:" + userId + " veriPin:" + veriPin, "VerifyOtp");

            var dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            string status;
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            try
            {
                string dbVeriPin = Decrypt(_ob.GetMyVerificationPin(userId));
                LogSteps("Decrypted Input parameters :function : GetMyVerificationPin", "VerifyOtp");

                LogSteps("Parametres after decrypt for Web-service: dbVeriPin:" + dbVeriPin, "VerifyOtp");

                if (veriPin == dbVeriPin)
                {
                    string i = _ob.VerifyOtp(userId);
                    if (i.Contains("Success"))
                    {
                        status = "Success";

                        DataRow dt1Row = dt1.NewRow();
                        dt1Row["result"] = i;

                        dt1.Rows.Add(dt1Row);
                    }
                    else
                    {
                        status = "Fail";
                        DataRow dt1Row = dt1.NewRow();
                        dt1Row["result"] = i;

                        dt1.Rows.Add(dt1Row);
                    }
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = "You have entered wrong OTP";

                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "VerifyOtp", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "userId=" + userId + "veriPin=" + Decrypt(veriPin), "VerifyOtp");
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        #endregion

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void UpdateProfile(string pname, string emailId, string mobileNo, string udob, string userId)
        {
            LogSteps("Web-service called", "UpdateProfile");
            var serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "UpdateProfile");

            LogSteps("Parametres for Web-service: pname:" + pname + " emailId:" + emailId + " mobileNo:" + mobileNo + " udob:" + udob + " userId:" + userId, "VerifyOtp");

            var dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            string status;
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            try
            {
                string res = _ob.UpdateProfile(pname, emailId, mobileNo, udob, userId);
                LogSteps("Response from UpdateProfile stored procedure:" + res, "UpdateProfile");
                status = "Success";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = res;
                dt1.Rows.Add(dt1Row);
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                LogSuccess("Success", "UpdateProfile", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "pname=" + pname + " emailId:" + emailId + "mobileNo=" + mobileNo + ",udob=" + udob + " userId:" + userId, "UpdateProfile");

                status = "Fail";

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SetMpin(string userId, string mPin)
        {
            LogSteps("Web-service called", "SetMpin");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "VerifyOtp");

            LogSteps("Parametres for Web-service: userId:" + userId + " mPin:" + mPin, "VerifyOtp");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            var rows = new List<Dictionary<string, object>>();
            string status;
            try
            {
                string i = _ob.SetMpin(userId, mPin);
                LogSteps("Response from SetMpin stored procedure:" + i, "SetMpin");

                if (i.Contains("Success"))
                {
                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "SetMpin", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "userId=" + userId + " mPin:" + mPin, "SetMpin");

                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ResetMpin(string userId, string oldmPin, string newmPin)
        {
            LogSteps("Web-service called", "ResetMpin");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "VerifyOtp");

            LogSteps("Parametres for Web-service: userId:" + userId + " oldmPin:" + oldmPin + " newmPin:" + newmPin, "ResetMpin");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            var rows = new List<Dictionary<string, object>>();
            string status;
            try
            {
                string encodedPin = Decrypt(_ob.GetMyMPinByuserId(userId));
                LogSteps("Response from GetMyMPinByuserId stored procedure:" + encodedPin, "ResetMpin");

                if (encodedPin == Decrypt(oldmPin))
                {
                    string i = _ob.SetMpin(userId, newmPin);
                    LogSteps("Response from SetMpin stored procedure:" + i, "ResetMpin");

                    if (i.Contains("Success"))
                    {
                        status = "Success";
                        DataRow dt1Row = dt1.NewRow();
                        dt1Row["result"] = i;

                        dt1.Rows.Add(dt1Row);
                    }
                    else
                    {
                        status = "Fail";
                        DataRow dt1Row = dt1.NewRow();
                        dt1Row["result"] = i;

                        dt1.Rows.Add(dt1Row);
                    }
                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    LogSuccess("Success", "ResetMpin", serializer.Serialize(rows));
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = "Please enter correct old mPin!! ";

                    dt1.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    LogSuccess("Success", "ResetMpin", serializer.Serialize(rows));
                }
            }
            catch (Exception ee)
            {
                LogError(ee, "userId:" + userId + " oldmPin:" + oldmPin + " newmPin:" + newmPin, "ResetMpin");
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ResendVerficationPin(string userId, string mobileNo)
        {
            LogSteps("Web-service called", "ResendVerficationPin");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "VerifyOtp");

            LogSteps("Parametres for Web-service: userId:" + userId + " mobileNo:" + mobileNo, "ResendVerficationPin");

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            var rows = new List<Dictionary<string, object>>();
            string status;
            try
            {
                string veriPin = GenerateVerificationCode();
                LogSteps("Response from GenerateVerificationCode stored procedure:" + veriPin, "ResendVerficationPin");
                string i = _ob.ResendVerficationPin(userId, Encrypt(veriPin));
                LogSteps("Response from ResendVerficationPin stored procedure:" + i, "ResendVerficationPin");
                if (i.Contains("Success"))
                {
                    try
                    {
                        string strUrl = string.Format("http://api.mVaayoo.com/mvaayooapi/MessageCompose?user={0}:{1}&senderID={2}&receipientno={3}&dcs=0&msgtxt={4}&state=1", ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"], ConfigurationManager.AppSettings["sender"], mobileNo, veriPin);
                        WebRequest request = WebRequest.Create(strUrl);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream s = response.GetResponseStream();
                        if (s != null)
                        {
                            StreamReader readStream = new StreamReader(s);
                            readStream.ReadToEnd();
                            response.Close();
                            s.Close();
                            readStream.Close();
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "SetMpin", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "userId:" + userId + " mobileNo:" + mobileNo, "ResendVerficationPin");
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Login(string mobileNo, string mPin)
        {
            var serializer = new JavaScriptSerializer();
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                mPin = Decrypt(mPin);
                string dbMPin = Decrypt(_ob.GetMyMPin(mobileNo));
                if (mPin == dbMPin)
                {
                    dt1 = _ob.GetUserDetailsBymobileNo(mobileNo);
                    if (dt1.Rows.Count <= 0) return;
                    status = "Success";
                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Fail";
                    dt1.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = "Please check your mpin";
                    dt1.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "Login", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                //LogError(ee);
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ForgetMpin(string mobileNo)
        {
            LogSteps("Web-service called", "ForgetMpin");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "ForgetMpin");

            LogSteps("Parametres for Web-service: mobileNo:" + mobileNo, "ForgetMpin");
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            var rows = new List<Dictionary<string, object>>();
            string status;
            try
            {
                string veriPin = GenerateVerificationCode();
                LogSteps("Response from GenerateVerificationCode stored procedure:" + veriPin, "ForgetMpin");
                string i = _ob.ForgetMpin(mobileNo, veriPin);
                LogSteps("Response from ForgetMpin stored procedure:" + i, "ForgetMpin");

                if (i.Contains("Success"))
                {
                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "ForgetMpin", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "mobileNo:" + mobileNo, "ForgetMpin");
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }



        //SOS Details
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void AddSosContact(string userId, string contactName, string contactNumber)
        {
            LogSteps("Web-service called", "AddSosContact");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "AddSosContact");

            LogSteps("Parametres for Web-service: userId:" + userId + " contactName:" + contactName + " contactNumber:" + contactNumber, "AddSosContact");
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                string i = _ob.AddSosContact(userId, contactName, contactNumber);
                LogSteps("Response from AddSosContact stored procedure:" + i, "AddSosContact");

                if (i.Contains("Success"))
                {
                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    if (i.Contains(("Cannot insert duplicate key row in object")))
                    {
                        dt1Row["result"] = "Contact Number Already Added";
                    }
                    else
                    {
                        dt1Row["result"] = i;
                    }
                    dt1.Rows.Add(dt1Row);
                }

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "AddSosContact", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "userId:" + userId + " contactName:" + contactName + " contactNumber:" + contactNumber, "AddSosContact");
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSosDetails(string userId)
        {
            LogSteps("Web-service called", "GetSosDetails");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetSosDetails");

            LogSteps("Parametres for Web-service: userId:" + userId, "GetSosDetails");
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _ob.GetSosDetails(userId);
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "Details Not Added Yet !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";
                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "GetSosDetails", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                //LogError(ee);
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void EditSosContact(string userId, string sosId, string contactName, string contactNumber)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                string i = _ob.EditSosContact(userId, sosId, contactName, contactNumber);
                if (i.Contains("Success"))
                {
                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;
                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;
                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "EditSosContact", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                //LogError(ee);
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }
            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void DeleteSosContact(string sosId, string userId)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("result", typeof(string));
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                string i = _ob.DeleteSosContact(sosId, userId);
                if (i.Contains("Success"))
                {
                    status = "Success";
                    DataRow dt1Row = dt1.NewRow();
                    dt1Row["result"] = i;

                    dt1.Rows.Add(dt1Row);
                }
                else
                {
                    status = "Fail";
                    DataRow dt1Row = dt1.NewRow();
                    if (i.Contains(("Cannot insert duplicate key row in object")))
                    {
                        dt1Row["result"] = "Contact Number Already Added";
                    }
                    else
                    {
                        dt1Row["result"] = i;
                    }
                    dt1.Rows.Add(dt1Row);
                }
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
                LogSuccess("Success", "DeleteSosContact", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                //LogError(ee);
                status = "Fail";
                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);
                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }
            }

            Context.Response.ContentType = "application/json; charset=utf-8";
            Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllExploreKochiCategoryList()
        {
            LogSteps("Web-service called", "GetAllExploreKochiCategoryList");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetAllExploreKochiCategoryList");
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _ob.GetExploreKochiCategories();
                LogSteps("Response from GetExploreKochiCategories stored procedure:" + dt1.Rows.Count, "GetExploreKochiCategories");

                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No categories !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    //var ser = serializer.Serialize(new { Status = status, data = rows });
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                    Object data1 = new
                    {
                        Status = status,
                        data = rows
                    };
                    LogSuccess("Success", "GetAllExploreKochiCategoryList", serializer.Serialize(data1));
                    JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                    string json = javaScriptSerializer.Serialize(new { req = data1 });

                }
                else
                {
                    status = "Success";
                    DataTable dt = new DataTable();
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("category", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {

                        DataRow dtrow = dt.NewRow();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["category"] = dt1.Rows[i]["category"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dt.Rows.Add(dtrow);
                    }
                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "GetAllExploreKochiCategoryList", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "", "GetAllExploreKochiCategoryList");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllGovtEntriesById(string exploreCatId, string govtCatId, string type, string Page, string RecsPerPage)
        {
            LogSteps("Web-service called", "GetAllGovtEntriesById");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetAllGovtEntriesById");

            LogSteps("Parametres for Web-service: exploreCatId:" + exploreCatId + " govtCatId:" + govtCatId + " type:" + type + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GetAllGovtEntriesById");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            var filterRows = new List<Dictionary<string, object>>();
            try
            {
                if (type == "Govt Category")
                {
                    dt1 = _obAdmin.GetAllGovtEntriesById(exploreCatId, govtCatId, Page, RecsPerPage);
                    LogSteps("Response from GetAllGovtEntriesById stored procedure:" + dt1.Rows.Count, "GetAllGovtEntriesById");

                }
                if (type == "Govt Service")
                {
                    dt1 = _obAdmin.GetAllServiceGovtEntriesById(exploreCatId, govtCatId, Page, RecsPerPage);
                    LogSteps("Response from GetAllServiceGovtEntriesById stored procedure:" + dt1.Rows.Count, "GetAllGovtEntriesById");

                }
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No data !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Success";
                    // filters for that category
                    DataTable dtFilter = new DataTable();
                    dtFilter = _obAdmin.GetFilterTypeById1(exploreCatId);
                    foreach (DataRow rs in dtFilter.Rows)
                    {
                        var row1 = dtFilter.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        filterRows.Add(row1);
                    }


                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();

                        dt.Rows.Add(dtrow);
                    }
                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows, filter = filterRows }));
                }

                LogSuccess("Success", "GetAllGovtEntriesById", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "exploreCatId:" + exploreCatId + " govtCatId:" + govtCatId + " type:" + type + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GetAllGovtEntriesById");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllEntriesById(string exploreCatId, string Page, string RecsPerPage)
        {
            LogSteps("Web-service called", "GetAllEntriesById");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetAllEntriesById");

            LogSteps("Parametres for Web-service: exploreCatId:" + exploreCatId + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GetAllEntriesById");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            var filterRows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _obAdmin.GetAllEntriesByIdWebservice(exploreCatId, Page, RecsPerPage);
                LogSteps("Response from GetAllEntriesByIdWebservice stored procedure:" + dt1.Rows.Count, "GetAllEntriesById");
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No data !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Success";
                    // filters for that category
                    DataTable dtFilter = new DataTable();
                    dtFilter = _obAdmin.GetFilterTypeById1(exploreCatId);
                    foreach (DataRow rs in dtFilter.Rows)
                    {
                        var row1 = dtFilter.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        filterRows.Add(row1);
                    }


                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {

                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();

                        dt.Rows.Add(dtrow);
                    }
                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows, filter = filterRows }));
                }

                LogSuccess("Success", "GetAllEntriesById", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "exploreCatId:" + exploreCatId + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GetAllEntriesById");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetEntryById(string exploreCatId, string id)
        {
            LogSteps("Web-service called", "GetEntryById");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetEntryById");

            LogSteps("Parametres for Web-service: exploreCatId:" + exploreCatId + " id:" + id, "GetEntryById");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _obAdmin.GetEntryByEntryId(exploreCatId, id);
                LogSteps("Response from GetEntryByEntryId stored procedure:" + dt1.Rows.Count, "GetEntryByEntryId");
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No data !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {

                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();

                        dt.Rows.Add(dtrow);
                    }
                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }

                LogSuccess("Success", "GetEntryById", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "exploreCatId:" + exploreCatId + " id:" + id, "GetEntryById");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ContextualSearchResult(string keyword, string exploreCatId, string Page, string RecsPerPage)
        {
            LogSteps("Web-service called", "ContextualSearchResult");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "ContextualSearchResult");
            LogSteps("Parametres for Web-service: keyword: " + keyword + "exploreCatId:" + exploreCatId + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "ContextualSearchResult");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _obAdmin.ContextualSearchResult(keyword, exploreCatId, Page, RecsPerPage);
                LogSteps("Response from ContextualSearchResult stored procedure:" + dt1.Rows.Count, "ContextualSearchResult");
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No search results to show !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();

                        dt.Rows.Add(dtrow);
                    }

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "ContextualSearchResult", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "keyword: " + keyword + "exploreCatId:" + exploreCatId + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "ContextualSearchResult");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GlobalSearchResult(string keyword, string Page, string RecsPerPage)
        {
            LogSteps("Web-service called", "GlobalSearchResult");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GlobalSearchResult");
            LogSteps("Parametres for Web-service: keyword: " + keyword + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GlobalSearchResult");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _obAdmin.GlobalSearchResult(keyword, Page, RecsPerPage);
                LogSteps("Response from GlobalSearchResult stored procedure:" + dt1.Rows.Count, "GlobalSearchResult");
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No search results to show !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));
                    dt.Columns.Add("category", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();
                        dtrow["category"] = dt1.Rows[i]["category"].ToString();

                        dt.Rows.Add(dtrow);
                    }

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "GlobalSearchResult", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "keyword: " + keyword + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GlobalSearchResult");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getFilterSearch(string keyword, string exploreCatId, string distanceFrom, string Page, string RecsPerPage)
        {
            LogSteps("Web-service called", "GlobalSearchResult");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GlobalSearchResult");
            LogSteps("Parametres for Web-service: keyword: " + keyword + " exploreCatId:" + exploreCatId + " distanceFrom:" + distanceFrom + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "GlobalSearchResult");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                keyword = "#" + keyword.Replace(",", "##") + "#";
                dt1 = _obAdmin.getFilterSearch(keyword, exploreCatId, distanceFrom, Page, RecsPerPage);
                LogSteps("Response from getFilterSearch stored procedure:" + dt1.Rows.Count, "getFilterSearch");
               
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No search results to show !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("exploreCatId", typeof(string));
                    dt.Columns.Add("entryName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("area", typeof(string));
                    dt.Columns.Add("contactNo", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));
                    dt.Columns.Add("speciality", typeof(string));
                    dt.Columns.Add("fuelComapny", typeof(string));
                    dt.Columns.Add("pType", typeof(string));
                    dt.Columns.Add("policeType", typeof(string));
                    dt.Columns.Add("noOfScreens", typeof(string));
                    dt.Columns.Add("starRating", typeof(string));
                    dt.Columns.Add("imageName", typeof(string));
                    dt.Columns.Add("displayImage", typeof(string));
                    dt.Columns.Add("govtService", typeof(string));
                    dt.Columns.Add("activityId", typeof(string));
                    dt.Columns.Add("distanceFrom", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["exploreCatId"] = dt1.Rows[i]["exploreCatId"].ToString();
                        dtrow["entryName"] = dt1.Rows[i]["entryName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["area"] = dt1.Rows[i]["area"].ToString();
                        dtrow["contactNo"] = dt1.Rows[i]["contactNo"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();
                        dtrow["speciality"] = dt1.Rows[i]["speciality"].ToString();
                        dtrow["fuelComapny"] = dt1.Rows[i]["fuelComapny"].ToString();
                        dtrow["pType"] = dt1.Rows[i]["pType"].ToString();
                        dtrow["policeType"] = dt1.Rows[i]["policeType"].ToString();
                        dtrow["noOfScreens"] = dt1.Rows[i]["noOfScreens"].ToString();
                        dtrow["starRating"] = dt1.Rows[i]["starRating"].ToString();
                        dtrow["imageName"] = dt1.Rows[i]["imageName"].ToString();
                        dtrow["displayImage"] = dt1.Rows[i]["displayImage"].ToString();
                        dtrow["govtService"] = dt1.Rows[i]["govtService"].ToString();
                        dtrow["activityId"] = dt1.Rows[i]["activitiesId"].ToString();
                        dtrow["distanceFrom"] = dt1.Rows[i]["distanceFrom"].ToString();

                        dt.Rows.Add(dtrow);
                    }

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "getFilterSearch", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "keyword: " + keyword + " exploreCatId:" + exploreCatId + " distanceFrom:" + distanceFrom + " Page:" + Page + " RecsPerPage:" + RecsPerPage, "getFilterSearch");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetGovtCategoryServices(string exploreCatId)
        {
            LogSteps("Web-service called", "GetGovtCategoryServices");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetGovtCategoryServices");
            LogSteps("Parametres for Web-service: exploreCatId:" + exploreCatId, "GetGovtCategoryServices");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _obAdmin.GetGovtCategoryServices(exploreCatId);
                LogSteps("Response from GetGovtCategoryServices stored procedure:" + dt1.Rows.Count, "GetGovtCategoryServices");
               
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No data !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Success";


                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }

                LogSuccess("Success", "GetGovtCategoryServices", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, " exploreCatId:" + exploreCatId, "GetGovtCategoryServices");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetOffersPromotions()
        {
            LogSteps("Web-service called", "GetOffersPromotions");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetOffersPromotions");
         
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _ob.GetOffersPromotions();
                LogSteps("Response from GetOffersPromotions stored procedure:" + dt1.Rows.Count, "GetOffersPromotions");
               
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No search results to show !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(string));
                    dt.Columns.Add("offerName", typeof(string));
                    dt.Columns.Add("bannerImage", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("otherDescription", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["id"] = dt1.Rows[i]["id"].ToString();
                        dtrow["offerName"] = dt1.Rows[i]["offerName"].ToString();
                        dtrow["bannerImage"] = dt1.Rows[i]["bannerImage"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["otherDescription"] = dt1.Rows[i]["otherDescription"].ToString();

                        dt.Rows.Add(dtrow);
                    }

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "GetOffersPromotions", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "", "GetOffersPromotions");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIssuanceCentre()
        {
            LogSteps("Web-service called", "GetIssuanceCentre");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetIssuanceCentre");

            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _ob.GetIssuanceCentre();
                LogSteps("Response from GetIssuanceCentre stored procedure:" + dt1.Rows.Count, "GetIssuanceCentre");

                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No search results to show !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));

                }
                else
                {
                    status = "Success";

                    DataTable dt = new DataTable();
                    dt.Columns.Add("centreName", typeof(string));
                    dt.Columns.Add("address", typeof(string));
                    dt.Columns.Add("workingHours", typeof(string));
                    dt.Columns.Add("workingDays", typeof(string));

                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        DataRow dtrow = dt.NewRow();
                        dtrow["centreName"] = dt1.Rows[i]["centreName"].ToString();
                        dtrow["address"] = dt1.Rows[i]["address"].ToString();
                        dtrow["workingHours"] = dt1.Rows[i]["workingHours"].ToString();
                        dtrow["workingDays"] = dt1.Rows[i]["workingDays"].ToString();

                        dt.Rows.Add(dtrow);
                    }

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                LogSuccess("Success", "GetIssuanceCentre", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "", "GetIssuanceCentre");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;
                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }


        //Query Web services

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetServiceSubCategories()
        {
            LogSteps("Web-service called", "GetServiceSubCategories");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "GetServiceSubCategories");
         
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            try
            {
                dt1 = _ob.GetServiceSubCategories();
                LogSteps("Response from GetServiceSubCategories stored procedure:" + dt1.Rows.Count, "GetServiceSubCategories");
               
                if (dt1.Rows.Count <= 0)
                {
                    DataTable dt = new DataTable();
                    status = "Fail";
                    dt.Columns.Add("result", typeof(string));

                    DataRow dt1Row = dt.NewRow();
                    dt1Row["result"] = "No categories found !!";

                    dt.Rows.Add(dt1Row);

                    foreach (DataRow rs in dt.Rows)
                    {
                        var row = dt.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }

                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }
                else
                {
                    status = "Success";
                    foreach (DataRow rs in dt1.Rows)
                    {
                        var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                        rows.Add(row);
                    }
                    Context.Response.ContentType = "application/json; charset=utf-8";
                    Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
                }

                LogSuccess("Success", "GetServiceSubCategories", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, "", "GetServiceSubCategories");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFiltersById(string exploreCatId)
        {
            LogSteps("Web-service called", "exploreCatId");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            LogSteps("Serializar object created", "exploreCatId");
            LogSteps("Parametres for Web-service: exploreCatId:" + exploreCatId, "exploreCatId");
            DataTable dt1 = new DataTable();
            string status;
            var rows = new List<Dictionary<string, object>>();
            var filterRows = new List<Dictionary<string, object>>();
            try
            {

                status = "Success";
                // filters for that category
                DataTable dtFilter = new DataTable();
                dtFilter = _obAdmin.GetFilterTypeById1(exploreCatId);
                LogSteps("Response from GetFilterTypeById1 stored procedure:" + dt1.Rows.Count, "GetFilterTypeById1");
               
                foreach (DataRow rs in dtFilter.Rows)
                {
                    var row1 = dtFilter.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    filterRows.Add(row1);
                }
                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = filterRows }));


                LogSuccess("Success", "GetFiltersById", serializer.Serialize(rows));
            }
            catch (Exception ee)
            {
                LogError(ee, " exploreCatId:" + exploreCatId, "GetFiltersById");
                status = "Fail";
                dt1.Columns.Add("result", typeof(string));

                DataRow dt1Row = dt1.NewRow();
                dt1Row["result"] = ee.Message;

                dt1.Rows.Add(dt1Row);

                foreach (DataRow rs in dt1.Rows)
                {
                    var row = dt1.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => rs[col]);
                    rows.Add(row);
                }

                Context.Response.ContentType = "application/json; charset=utf-8";
                Context.Response.Write(serializer.Serialize(new { Status = status, data = rows }));
            }
        }



        //Methods supporting web services

        public void Openconn()
        {
            try { Cn.Open(); }
            catch { Cn.Close(); Cn.Open(); }
        }

        [WebMethod]
        public string GenerateVerificationCode()
        {
            Random random = new Random();
            string[] array = new string[] { "0", "2", "3", "4", "5", "6", "8", "9" };
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int ij = 0; ij < 6; ij++)
            {
                int abc = random.Next(0, 8);
                sb.Append(array[abc]);
            }
            return sb.ToString();
        }

        public string GenerateReferralCode(string uName)
        {
            Random random = new Random();
            string[] array = new string[] { "0", "2", "3", "4", "5", "6", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "Y", "Z" };
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int ij = 0; ij < 6; ij++)
            {
                int abc = random.Next(0, 51);
                sb.Append(array[abc]);
            }
            return uName.Substring(0, 4) + sb;
        }

        #region Logger Services

        private void LogSuccess(string result, string webServiceName, string parameters)
        {
            try
            {
                string callingIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Request From IP: {0}", callingIP);
                message += Environment.NewLine;
                message += string.Format("Web Service Name: {0}", webServiceName);
                message += Environment.NewLine;
                message += string.Format("Result: {0}", result);
                message += Environment.NewLine;
                message += string.Format("Parameters: {0}", parameters);
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                string filename = "SuccessLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                string path = Server.MapPath("~/Log/" + filename);
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }
            }
            catch { }


        }
        private void LogError(Exception ex, string parameters, string webServiceName)
        {
            try
            {
                string callingIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Request From IP: {0}", callingIP);
                message += Environment.NewLine;
                message += string.Format("webServiceName: {0}", webServiceName);
                message += Environment.NewLine;
                message += string.Format("Message: {0}", ex.Message);
                message += Environment.NewLine;
                message += string.Format("StackTrace: {0}", ex.StackTrace);
                message += Environment.NewLine;
                message += string.Format("Source: {0}", ex.Source);
                message += Environment.NewLine;
                message += string.Format("TargetSite: {0}", ex.TargetSite);
                message += Environment.NewLine;
                message += string.Format("Parameters: {0}", parameters);
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                string filename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                string path = Server.MapPath("~/Log/" + filename);
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }
            }
            catch { }
        }
        private void LogSteps(string result, string webServiceName)
        {
            try
            {
                string callingIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Request From IP: {0}", callingIP);
                message += Environment.NewLine;
                message += string.Format("Web Service Name: {0}", webServiceName);
                message += Environment.NewLine;
                message += string.Format("Result: {0}", result);
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                string filename = "SuccessLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                string path = Server.MapPath("~/Log/" + filename);
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(message);
                    writer.Close();
                }

            }
            catch { }

        }

        #endregion
    }


}
